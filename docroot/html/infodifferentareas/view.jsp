<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.GregorianCalendar" %>
<%@ page import="ru.admomsk.server.beans.ViewBean" %>
<%@page import="javax.portlet.PortletURL"%>
<portlet:defineObjects />
<jsp:useBean id="viewBean" class="ru.admomsk.server.beans.ViewBean" scope="request"/>

<c:if test="${not viewBean.editBean.validAll}">
	<div class="journal-content-article">
		<h2>Портлет не сконфигурирован!</h2>
		<div class="form-error-message">В настройках портлета имеются ошибки, исправьте их, пожалуйста!</div>
	</div>
</c:if>

<c:if test='<%=viewBean.getSent().equals(ViewBean.SENT_SUCCESSFULLY)%>'>
	<div class="journal-content-article">
		<h2>Данные успешно отправлены</h2>
		<p>После регистрации электронного обращения вы получите первое уведомление&nbsp;— с регистрационным номером и датой.</p>
		<p>После подготовки запрашиваемого документа вам будет направлено второе уведомление. Для получения подготовленного документа необходимо лично обратиться по адресу, указанному во втором уведомлении.</p>
		<p>Запрошенный документ может получить представитель заявителя, предъявив оригинал доверенности, оформленной в установленном законом порядке.</p>
		<p>Телефон для справок: 78-79-01.</p>
		<p>Вы можете вернуться к заполнению <a id="btn_back_notsucc" href="<c:out value='<%=viewBean.getViewJSPURL()%>'></c:out>">формы</a>.</p>
	</div>
</c:if>

<c:if test='<%=viewBean.getSent().equals(ViewBean.SENT_FAILS)%>'>
	<div class="journal-content-article">
		<h2 class="form-error-message">Данные не отправлены!</h2>
		<div class="form-error-message">Возможно, произошла ошибка!</div>
		<p>Попробуйте повторить попытку позже.</p>
		<p>Телефон для справок: 78-79-01.</p>
		<p>Отправить обращение <a id="btn_back_notsucc" href="<c:out value='<%=viewBean.getViewJSPURL()%>'></c:out>">ещё раз</a>.</p>
	</div>
</c:if>

<c:if test="${viewBean.editBean.validAll}">
	<c:if test='<%=viewBean.getSent().equals(ViewBean.SENT_EXCEPT)%>'>
	<div id="kindergarden" >
	
		<form accept-charset="utf-8" id="ida_form" name="ida_form" method="POST" enctype="multipart/form-data" action="<portlet:actionURL />" class="jotform-form">
			<div class="form-all">
		    	<ul class="form-section">
		      		
				<li id="id_3" class="form-line ">
					<div class="form-header-group">
					<h3>Выберите наименование запрашиваемого документа<span class="form-required">*</span></h3>
			        	</div>
						
					<h4>Справочная информация в сфере жилищной политики</h4>
					<div class="form-input-wide" id="cid_78">
						<div class="form-html" id="text_78">
						<p class="moreinfo">Внимание! Документ этой группы может быть выдан заявителю только в том случае, если он является лицом, имеющим непосредственное отношение к объекту, указанному в запрашиваемом документе.</p>
						</div>
					</div>
						<div class="form-input-wide">
							<div id="radio_val_info" class="form-single-column <c:if test='<%=viewBean.getValidReqDocGroup().equals(viewBean.getIdle()) || viewBean.getValidYearHousingProgram().equals(viewBean.getIdle())%>'>form-validation-error</c:if>">
								<span class="form-radio-item">
									<input type="hidden" name="req_doc_group_val1" value="Справка об использовании (неиспользовании) гражданами права на приватизацию жилых помещений муниципального жилищного фонда города Омска за период с 14.05.2007">
									<jsp:setProperty name="viewBean" property="reqDocGroupVal1" value="Справка об использовании (неиспользовании) гражданами права на приватизацию жилых помещений муниципального жилищного фонда города Омска за период с 14.05.2007"/>
									<input id="req_doc_group1" name="req_doc_group" type="radio" value='<jsp:getProperty name="viewBean" property="reqDocGroupVal1"/>' class="form-radio" <c:if test='<%=viewBean.getReqDocGroup().equals(viewBean.getReqDocGroupVal1())%>'>checked</c:if>>
									<label><jsp:getProperty name="viewBean" property="reqDocGroupVal1"/> (<a href="/web/guest/services/center/housing/no-privating">описание услуги</a>)</label>
								</span>
								<!--
								<div class="clear">&nbsp;</div>
								<span class="form-radio-item">
									<input type="hidden" name="req_doc_group_val2" value="Справка об участии граждан в подпрограммах, предусмотренных федеральной целевой программой «Жилище»">
									<jsp:setProperty name="viewBean" property="reqDocGroupVal2" value="Справка об участии граждан в подпрограммах, предусмотренных федеральной целевой программой «Жилище»"/>
									<input id="req_doc_group2" name="req_doc_group" type="radio" value='<jsp:getProperty name="viewBean" property="reqDocGroupVal2"/>' class="form-radio" <c:if test='<%=viewBean.getReqDocGroup().equals(viewBean.getReqDocGroupVal2())%>'>checked</c:if>>
									<label><jsp:getProperty name="viewBean" property="reqDocGroupVal2"/> (<a href="/web/guest/services/center/housing/target-program">описание услуги</a>)</label>
								</span>
								<div class="clear">&nbsp;</div>
								<div id="year_housing_program_div" class="form-single-column <c:if test='<%=viewBean.getValidReqDocGroup().equals(viewBean.getIdle()) || viewBean.getValidYearHousingProgram().equals(viewBean.getIdle())%>'>form-validation-error</c:if>" style="display: none;">
									<span class="form-radio-item">
										<input type="hidden" name="year_housing_program_val1" value="2002–2010 годы">
										<jsp:setProperty name="viewBean" property="yearHousingProgramVal1" value="2002–2010 годы"/>
										<input id="year_housing_program_val1" name="year_housing_program" type="radio" value='<jsp:getProperty name="viewBean" property="yearHousingProgramVal1"/>' class="form-radio" <c:if test='<%=viewBean.getYearHousingProgram().equals(viewBean.getYearHousingProgramVal1())%>'>checked</c:if>>
										<label><jsp:getProperty name="viewBean" property="yearHousingProgramVal1"/></label>
									</span>
									<div class="clear">&nbsp;</div>
									<span class="form-radio-item">
										<input type="hidden" name="year_housing_program_val2" value="2011–2015 годы">
										<jsp:setProperty name="viewBean" property="yearHousingProgramVal2" value="2011–2015 годы"/>
										<input id="year_housing_program_val2" name="year_housing_program" type="radio" value='<jsp:getProperty name="viewBean" property="yearHousingProgramVal2"/>' class="form-radio" <c:if test='<%=viewBean.getYearHousingProgram().equals(viewBean.getYearHousingProgramVal2())%>'>checked</c:if>>
										<label><jsp:getProperty name="viewBean" property="yearHousingProgramVal2"/></label>
									</span>
									<div class="clear">&nbsp;</div>
									<span class="form-radio-item">
										<input type="hidden" name="year_housing_program_val3" value="2015–2020 годы">
										<jsp:setProperty name="viewBean" property="yearHousingProgramVal3" value="2015–2020 годы"/>
										<input id="year_housing_program_val3" name="year_housing_program" type="radio" value='<jsp:getProperty name="viewBean" property="yearHousingProgramVal3"/>' class="form-radio" <c:if test='<%=viewBean.getYearHousingProgram().equals(viewBean.getYearHousingProgramVal3())%>'>checked</c:if>>
										<label><jsp:getProperty name="viewBean" property="yearHousingProgramVal3"/></label>
									</span>
									<div class="clear">&nbsp;</div>
								</div>
								<span class="form-radio-item">
									<input type="hidden" name="req_doc_group_val3" value="Справка о принятии на учет граждан в качестве нуждающихся в жилых помещениях, предоставляемых по договорам социального найма">
									<jsp:setProperty name="viewBean" property="reqDocGroupVal3" value="Справка о принятии на учет граждан в качестве нуждающихся в жилых помещениях, предоставляемых по договорам социального найма"/>
									<input id="req_doc_group3" name="req_doc_group" type="radio" value='<jsp:getProperty name="viewBean" property="reqDocGroupVal3"/>' class="form-radio" <c:if test='<%=viewBean.getReqDocGroup().equals(viewBean.getReqDocGroupVal3())%>'>checked</c:if>>
									<label><jsp:getProperty name="viewBean" property="reqDocGroupVal3"/> (<a href="/web/guest/services/center/housing/queue">описание услуги</a>)</label>
								</span>
								-->
								<div class="clear">&nbsp;</div>
								<span class="form-radio-item">
									<input type="hidden" name="req_doc_group_val4" value="Справка об отсутствии задолженности по плате за пользование жилым помещением муниципального жилищного фонда города Омска">
									<jsp:setProperty name="viewBean" property="reqDocGroupVal4" value="Справка об отсутствии задолженности по плате за пользование жилым помещением муниципального жилищного фонда города Омска"/>
									<input id="req_doc_group4" name="req_doc_group" type="radio" value='<jsp:getProperty name="viewBean" property="reqDocGroupVal4"/>' class="form-radio" <c:if test='<%=viewBean.getReqDocGroup().equals(viewBean.getReqDocGroupVal4())%>'>checked</c:if>>
									<label><jsp:getProperty name="viewBean" property="reqDocGroupVal4"/> (<a href="/web/guest/services/center/housing/no-debts">описание услуги</a>)</label>
								</span>
								<div class="clear">&nbsp;</div>
								<span class="form-radio-item">
									<input type="hidden" name="req_doc_group_val5" value="Выписка из лицевого счета нанимателей жилых помещений о плате за пользование жилым помещением муниципального жилищного фонда города Омска">
									<jsp:setProperty name="viewBean" property="reqDocGroupVal5" value="Выписка из лицевого счета нанимателей жилых помещений о плате за пользование жилым помещением муниципального жилищного фонда города Омска"/>
									<input id="req_doc_group5" name="req_doc_group" type="radio" value='<jsp:getProperty name="viewBean" property="reqDocGroupVal5"/>' class="form-radio" <c:if test='<%=viewBean.getReqDocGroup().equals(viewBean.getReqDocGroupVal5())%>'>checked</c:if>>
									<label><jsp:getProperty name="viewBean" property="reqDocGroupVal5"/> (<a href="/web/guest/services/center/housing/personal-account">описание услуги</a>)</label>
								</span>
								<div class="clear">&nbsp;</div>
								<span class="form-radio-item">
									<input type="hidden" name="req_doc_group_val6" value="Дубликат договора безвозмездной передачи жилого помещения муниципального жилищного фонда города Омска в собственность граждан">
									<jsp:setProperty name="viewBean" property="reqDocGroupVal6" value="Дубликат договора безвозмездной передачи жилого помещения муниципального жилищного фонда города Омска в собственность граждан"/>
									<input id="req_doc_group6" name="req_doc_group" type="radio" value='<jsp:getProperty name="viewBean" property="reqDocGroupVal6"/>' class="form-radio" <c:if test='<%=viewBean.getReqDocGroup().equals(viewBean.getReqDocGroupVal6())%>'>checked</c:if>>
									<label><jsp:getProperty name="viewBean" property="reqDocGroupVal6"/> (<a href="/web/guest/services/center/housing/privating-copy">описание услуги</a>)</label>
								</span>
								<div class="clear">&nbsp;</div>
								<span class="form-radio-item">
									<input type="hidden" name="req_doc_group_val7" value="Дубликат договора социального найма жилого помещения муниципального жилищного фонда города Омска">
									<jsp:setProperty name="viewBean" property="reqDocGroupVal7" value="Дубликат договора социального найма жилого помещения муниципального жилищного фонда города Омска"/>
									<input id="req_doc_group7" name="req_doc_group" type="radio" value='<jsp:getProperty name="viewBean" property="reqDocGroupVal7"/>' class="form-radio" <c:if test='<%=viewBean.getReqDocGroup().equals(viewBean.getReqDocGroupVal7())%>'>checked</c:if>>
									<label><jsp:getProperty name="viewBean" property="reqDocGroupVal7"/> (<a href="/web/guest/services/center/housing/social-rent-copy">описание услуги</a>)</label>
								</span>
								<div class="clear">&nbsp;</div>
							<%-- 	
							<h4>Справочная информация в сфере архитектуры и градостроительства</h4>	
								<span class="form-radio-item">
									<jsp:setProperty name="viewBean" property="reqDocGroupVal8" value="Справка о перспективах застройки города Омска"/>
									<input id="req_doc_group8" name="req_doc_group" type="radio" value='<jsp:getProperty name="viewBean" property="reqDocGroupVal8"/>' class="form-radio" <c:if test='<%=viewBean.getReqDocGroup().equals(viewBean.getReqDocGroupVal8())%>'>checked</c:if>>
									<label><jsp:getProperty name="viewBean" property="reqDocGroupVal8"/></label>
								</span>
								<div class="clear">&nbsp;</div>
							 --%>
							<h4>Справочная информация в сфере имущественных отношений</h4>
								<%-- 
								<span class="form-radio-item">
									<jsp:setProperty name="viewBean" property="reqDocGroupVal9" value="Справка о разрешенном использовании земельных участков в соответствии с Правилами землепользования и застройки муниципального образования городской округ город Омск Омской области"/>
									<input id="req_doc_group9" name="req_doc_group" type="radio" value='<jsp:getProperty name="viewBean" property="reqDocGroupVal9"/>' class="form-radio" <c:if test='<%=viewBean.getReqDocGroup().equals(viewBean.getReqDocGroupVal9())%>'>checked</c:if>>
									<label><jsp:getProperty name="viewBean" property="reqDocGroupVal9"/></label>
								</span>
								<div class="clear">&nbsp;</div>
								 --%>
								<span class="form-radio-item">
									<input type="hidden" name="req_doc_group_val10" value="Выписка из Реестра муниципального имущества города Омска">
									<jsp:setProperty name="viewBean" property="reqDocGroupVal10" value="Выписка из Реестра муниципального имущества города Омска"/>
									<input id="req_doc_group10" name="req_doc_group" type="radio" value='<jsp:getProperty name="viewBean" property="reqDocGroupVal10"/>' class="form-radio" <c:if test='<%=viewBean.getReqDocGroup().equals(viewBean.getReqDocGroupVal10())%>'>checked</c:if>>
									<label><jsp:getProperty name="viewBean" property="reqDocGroupVal10"/> (<a href="/web/guest/services/center/property/register-extract">описание услуги</a>)</label>
								</span>
								<div class="clear">&nbsp;</div>
								<span class="form-radio-item">
									<input type="hidden" name="req_doc_group_val11" value="Сведения из Реестра муниципального имущества города Омска">
									<jsp:setProperty name="viewBean" property="reqDocGroupVal11" value="Сведения из Реестра муниципального имущества города Омска"/>
									<input id="req_doc_group11" name="req_doc_group" type="radio" value='<jsp:getProperty name="viewBean" property="reqDocGroupVal11"/>' class="form-radio" <c:if test='<%=viewBean.getReqDocGroup().equals(viewBean.getReqDocGroupVal11())%>'>checked</c:if>>
									<label><jsp:getProperty name="viewBean" property="reqDocGroupVal11"/> (<a href="/web/guest/services/center/property/register-info">описание услуги</a>)</label>
								</span>
								<!-- 
								<div class="clear">&nbsp;</div>
								<span class="form-radio-item">
									<input type="hidden" name="req_doc_group_val12" value="Справка о почтово-адресной нумерации объектов капитального строительства, расположенных на территории города Омска (за период с августа 2007 года по настоящее время)">
									<jsp:setProperty name="viewBean" property="reqDocGroupVal12" value="Справка о почтово-адресной нумерации объектов капитального строительства, расположенных на территории города Омска (за период с августа 2007 года по настоящее время)"/>
									<input id="req_doc_group12" name="req_doc_group" type="radio" value='<jsp:getProperty name="viewBean" property="reqDocGroupVal12"/>' class="form-radio" <c:if test='<%=viewBean.getReqDocGroup().equals(viewBean.getReqDocGroupVal12())%>'>checked</c:if>>
									<label><jsp:getProperty name="viewBean" property="reqDocGroupVal12"/> (<a href="/web/guest/services/center/housing/addresses">описание услуги</a>)</label>
								</span>
								-->
								<div class="clear">&nbsp;</div>
								
							</div>
							
							<c:if test='<%=viewBean.getValidReqDocGroup().equals(viewBean.getIdle())%>'>
						  		<div class="form-error-message for_id_radio_val_info">
								Необходимо выбрать наименование запрашиваемого документа.
								</div>
							</c:if>
							
							<c:if test='<%=viewBean.getValidYearHousingProgram().equals(viewBean.getIdle())%>'>
						  		<div class="form-error-message for_id_radio_val_info">
								Необходимо выбрать годы участия граждан в подпрограммах, предусмотренных федеральной целевой программой «Жилище».
								</div>
							</c:if>
						</div>
						
					</li>
				</ul>
				
				<ul class="form-section">
					<li id="id_6" class="form-line">
						<div class="form-header-group">
							<h3 class="form-header" id="header_1">Укажите цель (суть) запроса</h3>
			        	</div>
			        		<label class="form-label-left">Цель (суть) запроса<span class="form-required">*</span></label>
						<div class="form-input-wide">
							<input id="purpose_request" name="purpose_request" type="text" value="<c:out value='<%=viewBean.getPurposeRequest()%>'></c:out>" class="form-textbox tfPosition <c:if test='<%=viewBean.getValidPurposeRequest().equals(viewBean.getIdle())%>'>form-validation-error</c:if>">
						</div>
						<c:if test='<%=viewBean.getValidPurposeRequest().equals(viewBean.getIdle())%>'>
						  		<div class="form-error-message for_id_purpose_request">
								Указание цели (сути) запроса ускорит подготовку документа.
								</div>
						</c:if>
					</li>
					<li class="form-line" id="cid_7">
						<div class="form-header-group">
							<h3 class="form-header">Введите сведения о заявителе</h3>
						</div>
						
						<label for="input_8" id="label_8" class="form-label-left">
		          			Заявитель<span class="form-required">*</span>
		        		</label>		
						<div class="form-input-wide" id="cid_8">
							<div id="type_applicant_div" class="form-single-column <c:if test='<%=viewBean.getValidTypeApplicant().equals(viewBean.getIdle())%>'>form-validation-error</c:if>">
								<span class="form-radio-item">
									<input type="hidden" name="type_applicant_val1" value="Физическое лицо">
									<jsp:setProperty name="viewBean" property="typeApplicantVal1" value="Физическое лицо"/>
									<input id="individual" name="type_applicant" type="radio" value='<jsp:getProperty name="viewBean" property="typeApplicantVal1"/>' class="form-radio" class="form-radio" <c:if test='<%=viewBean.getTypeApplicant().equals(viewBean.getTypeApplicantVal1())%>'>checked</c:if>>
									<label><jsp:getProperty name="viewBean" property="typeApplicantVal1"/></label>
								</span>
								<div class="clear">&nbsp;</div>
								<span class="form-radio-item">
									<input type="hidden" name="type_applicant_val2" value="Юридическое лицо">
									<jsp:setProperty name="viewBean" property="typeApplicantVal2" value="Юридическое лицо"/>
									<input id="legal" name="type_applicant" type="radio" value='<jsp:getProperty name="viewBean" property="typeApplicantVal2"/>' class="form-radio" <c:if test='<%=viewBean.getTypeApplicant().equals(viewBean.getTypeApplicantVal2())%>'>checked</c:if>>
									<label><jsp:getProperty name="viewBean" property="typeApplicantVal2"/></label>
								</span>
								<div class="clear">&nbsp;</div>
							</div>
							<c:if test='<%=viewBean.getValidTypeApplicant().equals(viewBean.getIdle())%>'>
								<div class="form-error-message for_id_type_applicant_div">
								Необходимо выбрать заявителя.
								</div>
							</c:if>
						</div>
					</li>
				</ul>
				
				<ul id="individual_ul" class="form-section" style="display: none;">
					
					<li id="id_9" class="form-line">
						<label class="form-label-left">Заявитель &mdash; физическое лицо<span class="form-required">*</span></label>
						<div id="cid_9" class="form-input-wide">
							<span class="form-sub-label-container">
								<input id="individual_lastname" name="individual_lastname" type="text"  value="<c:out value='<%=viewBean.getIndividualLastname()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidIndividualLastname().equals(viewBean.getIdle()) %>'>form-validation-error</c:if>">
								<label class="form-sub-label">Фамилия</label>
							</span>
							<span class="form-sub-label-container">
								<input id="individual_firstname" name="individual_firstname" type="text"  value="<c:out value='<%=viewBean.getIndividualFirstname()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidIndividualFirstname().equals(viewBean.getIdle()) %>'>form-validation-error</c:if>">
								<label for="input_10" class="form-sub-label">Имя</label>
							</span>
							<span class="form-sub-label-container">
								<input id="individual_middlename" name="individual_middlename" type="text"  value="<c:out value='<%=viewBean.getIndividualMiddlename()%>'></c:out>" class="form-textbox">
								<label class="form-sub-label">Отчество</label>
							</span>
						</div>
						<c:if test='<%=viewBean.getValidIndividualFirstname().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_individual_firstname">
							Необходимо ввести имя.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidIndividualLastname().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_individual_lastname">
							Необходимо ввести фамилию.
							</div>
						</c:if>
					</li>
					
					<li id="individual_birthday_li" class="form-line" style="display: none;">
						<label class="form-label-left">Дата рождения<span class="form-required">*</span></label>
						<div class="form-input-wide">
						
							<span class="form-sub-label-container">
								<select id="individual_day_birthday" name="individual_day_birthday" class="form-dropdown <c:if test='<%=viewBean.getValidIndividualDayBirthday().equals(viewBean.getIdle()) %>'>form-validation-error</c:if>">
									<option>  </option>
									<c:forEach var="i" begin="1" end="31" step="1" varStatus ="status">
										<option value="<c:out value='${i}'/>" <c:if test='${viewBean.individualDayBirthday eq i}'>selected</c:if>> <c:out value='${i}'/></option> 
									</c:forEach>
								</select>
								<label class="form-sub-label">День</label>
							</span>
							
							<span class="form-sub-label-container">
								<select id="individual_month_birthday" name="individual_month_birthday" class="form-dropdown <c:if test='<%=viewBean.getValidIndividualMonthBirthday().equals(viewBean.getIdle()) %>'>form-validation-error</c:if>">
									<option>  </option>
									<option value="1" <c:if test='${viewBean.individualMonthBirthday eq 1}'>selected</c:if>>Январь</option>
									<option value="2" <c:if test='${viewBean.individualMonthBirthday eq 2}'>selected</c:if>>Февраль</option>
									<option value="3" <c:if test='${viewBean.individualMonthBirthday eq 3}'>selected</c:if>>Март</option>
									<option value="4" <c:if test='${viewBean.individualMonthBirthday eq 4}'>selected</c:if>>Апрель</option>
									<option value="5" <c:if test='${viewBean.individualMonthBirthday eq 5}'>selected</c:if>>Май</option>
									<option value="6" <c:if test='${viewBean.individualMonthBirthday eq 6}'>selected</c:if>>Июнь</option>
									<option value="7" <c:if test='${viewBean.individualMonthBirthday eq 7}'>selected</c:if>>Июль</option>
									<option value="8" <c:if test='${viewBean.individualMonthBirthday eq 8}'>selected</c:if>>Август</option>
									<option value="9" <c:if test='${viewBean.individualMonthBirthday eq 9}'>selected</c:if>>Сентябрь</option>
									<option value="10" <c:if test='${viewBean.individualMonthBirthday eq 10}'>selected</c:if>>Октябрь</option>
									<option value="11" <c:if test='${viewBean.individualMonthBirthday eq 11}'>selected</c:if>>Ноябрь</option>
									<option value="12" <c:if test='${viewBean.individualMonthBirthday eq 12}'>selected</c:if>>Декабрь</option>
								</select>
								<label class="form-sub-label">Месяц</label>
							</span>
																		
							<span class="form-sub-label-container">
								<select id="individual_year_birthday" name="individual_year_birthday" class="form-dropdown <c:if test='<%=viewBean.getValidIndividualYearBirthday().equals(viewBean.getIdle()) %>'>form-validation-error</c:if>">
									<option> </option>
									<!--<c:forEach var="i" begin="1991" end="<%=new GregorianCalendar().get(Calendar.YEAR)%>" step="1" varStatus ="status">
										<option value="<c:out value='${i}'/>" <c:if test='${viewBean.individualYearBirthday eq i}'>selected</c:if>> <c:out value='${i}'/></option> 
									</c:forEach>
									-->
									<%
										for (int y = new GregorianCalendar().get(Calendar.YEAR); y >= 1900; y--) {
											if (viewBean.getIndividualYearBirthday().equals(String.valueOf(y))) {
												out.println("<option value='"+ y + "' selected>"+ y + "</option>");	
											} else {
												out.println("<option value='"+ y + "'>"+ y + "</option>");
											}
											
										}
									%>
								</select>
								<label class="form-sub-label">Год</label>
							</span>
						</div>
						<c:if test='<%=viewBean.getValidIndividualYearBirthday().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_individual_year_birthday">
							Необходимо выбрать год рождения.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidIndividualMonthBirthday().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_individual_month_birthday">
							Необходимо выбрать месяц рождения.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidIndividualDayBirthday().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_individual_day_birthday">
							Необходимо выбрать день рождения.
							</div>
						</c:if>
					</li>
					
					<li id="individual_change_ul" class="form-line" style="display: none;">
						<label class="form-label-left">Заявитель менял фамилию, имя или отчество</label>
						<div id="cid_9" class="form-input-wide">
							<span class="form-sub-label-container">
								<input id="individual_change_last_first_middle_name" name="individual_change_last_first_middle_name" type="text"  value="<c:out value='<%=viewBean.getIndividualChangeLastFirstMiddleName()%>'></c:out>" class="form-textbox">
								<label class="form-sub-label">Перечислите иные фамилии, имена и отчества через запятую</label>
							</span>
						</div>
					</li>
					
					<li id="id_12" class="form-line">
						<label class="form-label-left">Паспорт<span class="form-required">*</span></label>
						<div class="form-input-wide">
							<span class="form-sub-label-container">
								<input id="individual_series_passport" name="individual_series_passport" type="text"  value="<c:out value='<%=viewBean.getIndividualSeriesPassport()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidIndividualSeriesPassport().equals(viewBean.getIdle()) %>'>form-validation-error</c:if>">
								<label class="form-sub-label">Серия</label>
							</span>
							<span class="form-sub-label-container">
						        <input id="individual_number_passport" name="individual_number_passport" type="text"  value="<c:out value='<%=viewBean.getIndividualNumberPassport()%>'></c:out>" class=" form-textbox <c:if test='<%=viewBean.getValidIndividualNumberPassport().equals(viewBean.getIdle()) %>'>form-validation-error</c:if>">
						        <label class="form-sub-label">Номер</label>
					        </span>
						</div>
						<c:if test='<%=viewBean.getValidIndividualNumberPassport().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_individual_number_passport">
							Необходимо ввести номер паспорта.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidIndividualSeriesPassport().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_individual_series_passport">
							Необходимо ввести серию паспорта.
							</div>
						</c:if>
					</li>
					
					<li class="form-line">
						<label class="form-label-left">Кем выдан паспорт<span class="form-required">*</span></label>
						<div class="form-input-wide">
							<input id="individual_who_give_passport" name="individual_who_give_passport" type="text"  value="<c:out value='<%=viewBean.getIndividualWhoGivePassport()%>'></c:out>" class=" form-textbox <c:if test='<%=viewBean.getValidIndividualWhoGivePassport().equals(viewBean.getIdle()) %>'>form-validation-error</c:if>">
						</div>
						<c:if test='<%=viewBean.getValidIndividualWhoGivePassport().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_individual_who_give_passport">
							Необходимо ввести кем выдан паспорт.
							</div>
						</c:if>
					</li>
		      		
		      		<li id="id_15" class="form-line">
						<label class="form-label-left">Дата выдачи паспорта<span class="form-required">*</span></label>
						<div class="form-input-wide">
						
							<span class="form-sub-label-container">
								<select id="individual_day_issue_passport" name="individual_day_issue_passport" class="form-dropdown <c:if test='<%=viewBean.getValidIndividualDayIssuePassport().equals(viewBean.getIdle()) %>'>form-validation-error</c:if>">
									<option>  </option>
									<c:forEach var="i" begin="1" end="31" step="1" varStatus ="status">
										<option value="<c:out value='${i}'/>" <c:if test='${viewBean.individualDayIssuePassport eq i}'>selected</c:if>> <c:out value='${i}'/></option> 
									</c:forEach>
								</select>
								<label class="form-sub-label">День</label>
							</span>
							
							<span class="form-sub-label-container">
								<select id="individual_month_issue_passport" name="individual_month_issue_passport" class="form-dropdown <c:if test='<%=viewBean.getValidIndividualMonthIssuePassport().equals(viewBean.getIdle()) %>'>form-validation-error</c:if>">
									<option>  </option>
									<option value="1" <c:if test='${viewBean.individualMonthIssuePassport eq 1}'>selected</c:if>>Январь</option>
									<option value="2" <c:if test='${viewBean.individualMonthIssuePassport eq 2}'>selected</c:if>>Февраль</option>
									<option value="3" <c:if test='${viewBean.individualMonthIssuePassport eq 3}'>selected</c:if>>Март</option>
									<option value="4" <c:if test='${viewBean.individualMonthIssuePassport eq 4}'>selected</c:if>>Апрель</option>
									<option value="5" <c:if test='${viewBean.individualMonthIssuePassport eq 5}'>selected</c:if>>Май</option>
									<option value="6" <c:if test='${viewBean.individualMonthIssuePassport eq 6}'>selected</c:if>>Июнь</option>
									<option value="7" <c:if test='${viewBean.individualMonthIssuePassport eq 7}'>selected</c:if>>Июль</option>
									<option value="8" <c:if test='${viewBean.individualMonthIssuePassport eq 8}'>selected</c:if>>Август</option>
									<option value="9" <c:if test='${viewBean.individualMonthIssuePassport eq 9}'>selected</c:if>>Сентябрь</option>
									<option value="10" <c:if test='${viewBean.individualMonthIssuePassport eq 10}'>selected</c:if>>Октябрь</option>
									<option value="11" <c:if test='${viewBean.individualMonthIssuePassport eq 11}'>selected</c:if>>Ноябрь</option>
									<option value="12" <c:if test='${viewBean.individualMonthIssuePassport eq 12}'>selected</c:if>>Декабрь</option>
								</select>
								<label class="form-sub-label">Месяц</label>
							</span>
																		
							<span class="form-sub-label-container">
								<select id="individual_year_issue_passport" name="individual_year_issue_passport" class="form-dropdown <c:if test='<%=viewBean.getValidIndividualYearIssuePassport().equals(viewBean.getIdle()) %>'>form-validation-error</c:if>">
									<option> </option>
									<!-- 
									<c:forEach var="i" begin="1991" end="<%=new GregorianCalendar().get(Calendar.YEAR)%>" step="1" varStatus ="status">
										<option value="<c:out value='${i}'/>" <c:if test='${viewBean.individualYearIssuePassport eq i}'>selected</c:if>> <c:out value='${i}'/></option> 
									</c:forEach>
									 -->
									<%
										for (int y = new GregorianCalendar().get(Calendar.YEAR); y >= 1991; y--) {
											if (viewBean.getIndividualYearIssuePassport().equals(String.valueOf(y))) {
												out.println("<option value='"+ y + "' selected>"+ y + "</option>");
											} else {
												out.println("<option value='"+ y + "'>"+ y + "</option>");
											}
										}
									%>
								</select>
								<label class="form-sub-label">Год</label>
							</span>
						</div>
						<c:if test='<%=viewBean.getValidIndividualYearIssuePassport().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_individual_year_issue_passport">
							Необходимо выбрать год выдачи паспорта.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidIndividualMonthIssuePassport().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_individual_month_issue_passport">
							Необходимо выбрать месяц выдачи паспорта.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidIndividualDayIssuePassport().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_individual_day_issue_passport">
							Необходимо выбрать день выдачи паспорта.
							</div>
						</c:if>
					</li>
		      		
		      		<li class="form-line">
		      			<label class="form-label-left">Копия паспорта<span class="form-required">*</span></label>
						<div class="form-input-wide">
							<span class="form-sub-label-container">
								<input id="individual_attach_passport" name="individual_attach_passport" type="file" class="form-upload <c:if test='<%=viewBean.getValidIndividualAttachPassport().equals(viewBean.getBadFormatAttach()) || viewBean.getValidIndividualAttachPassport().equals(viewBean.getBadSizeAttach())%>'>form-validation-error</c:if>">
								<label class="form-sub-label">Файл <c:if test="${viewBean.editBean.formatAttach eq ''}">любого</c:if> формата <c:out value="${viewBean.editBean.formatAttach}"></c:out> размером не более <c:out value="${viewBean.editBean.maxSizeAttach}"></c:out> Мб</label>
							</span>
						</div>
						<c:if test='<%=viewBean.getValidIndividualAttachPassport().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_individual_attach_passport">
							Необходимо загрузить копию паспорта.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidIndividualAttachPassport().equals(viewBean.getBadFormatAttach())%>'>
							<div class="form-error-message for_individual_attach_passport">
								Недопустимый формат файла.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidIndividualAttachPassport().equals(viewBean.getBadSizeAttach())%>'>
							<div class="form-error-message for_individual_attach_passport">
								Файл превышает допустимый размер.
							</div>
						</c:if>
					</li>
					
					<li class="form-line">
		      			<label class="form-label-left">Доверенность (при необходимости)</label>
						<div class="form-input-wide">
							<span class="form-sub-label-container">
								<input id="individual_procuratory_attach_passport" name="individual_procuratory_attach_passport" type="file" class="form-upload <c:if test='<%=viewBean.getValidIndividualProcuratoryAttachPassport().equals(viewBean.getBadFormatAttach()) || viewBean.getValidIndividualProcuratoryAttachPassport().equals(viewBean.getBadSizeAttach())%>'>form-validation-error</c:if>">
								<label class="form-sub-label">Файл <c:if test="${viewBean.editBean.formatAttach eq ''}">любого</c:if> формата <c:out value="${viewBean.editBean.formatAttach}"></c:out> размером не более <c:out value="${viewBean.editBean.maxSizeAttach}"></c:out> Мб</label>
							</span>
						</div>
						<c:if test='<%=viewBean.getValidIndividualProcuratoryAttachPassport().equals(viewBean.getBadFormatAttach())%>'>
							<div class="form-error-message for_individual_procuratory_attach_passport">
								Недопустимый формат файла.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidIndividualProcuratoryAttachPassport().equals(viewBean.getBadSizeAttach())%>'>
							<div class="form-error-message for_individual_procuratory_attach_passport">
								Файл превышает допустимый размер.
							</div>
						</c:if>
					</li>
		      		
					<li id="id_16" class="form-line">
						<label class="form-label-left">Адрес регистрации по месту жительства<span class="form-required">*</span></label>
						<div class="form-input-wide" id="cid_16">
							<span class="form-sub-label-container">
								<input id="individual_city" name="individual_city" type="text"  value="<c:out value='<%=viewBean.getIndividualCity()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidIndividualCity().equals(viewBean.getIdle()) %>'>form-validation-error</c:if>">
								<label class="form-sub-label">Город</label>
							</span>
							<span class="form-sub-label-container">
								<input id="individual_street" name="individual_street" type="text"  value="<c:out value='<%=viewBean.getIndividualStreet()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidIndividualStreet().equals(viewBean.getIdle()) %>'>form-validation-error</c:if>">
								<label class="form-sub-label">Улица</label>
							</span>
							<span class="form-sub-label-container">
								<input id="individual_home" name="individual_home" type="text"  value="<c:out value='<%=viewBean.getIndividualHome()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidIndividualHome().equals(viewBean.getIdle()) %>'>form-validation-error</c:if>">
								<label class="form-sub-label">Дом</label>
							</span>
							<span class="form-sub-label-container">
								<input id="individual_housing" name="individual_housing" type="text"  value="<c:out value='<%=viewBean.getIndividualHousing()%>'></c:out>" class="form-textbox">
								<label class="form-sub-label">Корпус</label>
							</span>
							<span class="form-sub-label-container">
								<input id="individual_apartment" name="individual_apartment" type="text"  value="<c:out value='<%=viewBean.getIndividualApartment()%>'></c:out>" class="form-textbox">
								<label class="form-sub-label">Квартира</label>
							</span>
							<span class="form-sub-label-container">
					        	<input id="individual_section" name="individual_section" type="text"  value="<c:out value='<%=viewBean.getIndividualSection()%>'></c:out>" class="form-textbox">
					            <label class="form-sub-label">Секция</label>
		        			</span>
							<span class="form-sub-label-container">
								<input id="individual_room" name="individual_room" type="text"  value="<c:out value='<%=viewBean.getIndividualRoom()%>'></c:out>" class="form-textbox">
								<label class="form-sub-label">Комната</label>
							</span>
						</div>
						<c:if test='<%=viewBean.getValidIndividualHome().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_individual_home">
							Необходимо ввести дом.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidIndividualStreet().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_individual_street">
							Необходимо ввести улицу.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidIndividualCity().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_individual_city">
							Необходимо ввести город.
							</div>
						</c:if>
					</li>
		      		
					<li class="form-line">
						<label class="form-label-left">Телефон<span class="form-required">*</span></label>
						<div class="form-input-wide">
							<input id="individual_number_telephone" name="individual_number_telephone" type="text"  value="<c:out value='<%=viewBean.getIndividualNumberTelephone()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidIndividualNumberTelephone().equals(viewBean.getIdle()) %>'>form-validation-error</c:if>">
						</div>
						<c:if test='<%=viewBean.getValidIndividualNumberTelephone().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_individual_number_telephone">
							Необходимо ввести номер телефона.
							</div>
						</c:if>
					</li>
		      
					<li id="id_24" class="form-line">
						<label class="form-label-left">Адрес электронной почты<span class="form-required">*</span></label>
						<div class="form-input-wide">
							<span class="form-sub-label-container">
								<input id="individual_email" name="individual_email" type="text"  value="<c:out value='<%=viewBean.getIndividualEmail()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidIndividualEmail().equals(viewBean.getIdle()) || viewBean.getValidIndividualEmail().equals(viewBean.getBadEmail())%>'>form-validation-error</c:if>">
								<label class="form-sub-label">На указанный адрес будет направлено уведомление о готовности документа с указанием адреса службы одного окна, по которому будет выдаваться запрашиваемый документ.</label>
							</span>
						</div>
						<c:if test='<%=viewBean.getValidIndividualEmail().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_individual_email">
							Необходимо ввести адрес электронной почты.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidIndividualEmail().equals(viewBean.getBadEmail())%>'>
							<div class="form-error-message for_id_individual_email">
							Адрес электронной почты введен некорректно.
							</div>
						</c:if>
					</li>
		      </ul>
		      
		      <ul id="legal_ul" class="form-section" style="display: none;">
					<li id="id_25" class="form-line">
						<label class="form-label-left">Заявитель &mdash; юридическое лицо<span class="form-required">*</span></label>
						<div class="form-input-wide">
							<span class="form-sub-label-container">
								<input id="legal_name" name="legal_name" type="text"  value="<c:out value='<%=viewBean.getLegalName()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidLegalName().equals(viewBean.getIdle())%>'>form-validation-error</c:if>">
							    <label class="form-sub-label">Полное наименование</label>
							</span>
						</div>
						<c:if test='<%=viewBean.getValidLegalName().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_legal_name">
							Необходимо ввести полное наименование.
							</div>
						</c:if>
					</li>
		      
					<li id="id_26" class="form-line">
						<label class="form-label-left">Реквизиты<span class="form-required">*</span></label>
						<div class="form-input-wide">
							<span class="form-sub-label-container">
								<input id="legal_ogrn" name="legal_ogrn" type="text"  value="<c:out value='<%=viewBean.getLegalOgrn()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidLegalOgrn().equals(viewBean.getIdle())%>'>form-validation-error</c:if>">
								<label class="form-sub-label">ОГРН</label>
							</span>
							<span class="form-sub-label-container">
					        	<input id="legal_inn" name="legal_inn" type="text"  value="<c:out value='<%=viewBean.getLegalInn()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidLegalInn().equals(viewBean.getIdle())%>'>form-validation-error</c:if>">
					            <label class="form-sub-label">ИНН</label>
					        </span>
						</div>
						<c:if test='<%=viewBean.getValidLegalInn().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_legal_inn">
							Необходимо ввести ИНН.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidLegalOgrn().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_legal_ogrn">
							Необходимо ввести ОГРН.
							</div>
						</c:if>
					</li>
					
					<li id="id_28" class="form-line">
						<label class="form-label-left">Руководитель<span class="form-required">*</span></label>
						<div class="form-input-wide">
							<span class="form-sub-label-container">
								<input id="legal_head_lastname" name="legal_head_lastname" type="text"  value="<c:out value='<%=viewBean.getLegalHeadLastname()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidLegalHeadLastname().equals(viewBean.getIdle())%>'>form-validation-error</c:if>">
							    <label class="form-sub-label">Фамилия</label>
							</span>
							<span class="form-sub-label-container">
					        	<input id="legal_head_firstname" name="legal_head_firstname" type="text"  value="<c:out value='<%=viewBean.getLegalHeadFirstname()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidLegalHeadFirstname().equals(viewBean.getIdle())%>'>form-validation-error</c:if>">
					            <label class="form-sub-label">Имя</label>
					        </span>
					        <span class="form-sub-label-container">
					        	<input id="legal_head_middlename" name="legal_head_middlename" type="text"  value="<c:out value='<%=viewBean.getLegalHeadMiddlename()%>'></c:out>" class="form-textbox">
					            <label class="form-sub-label">Отчество</label>
					        </span>
						</div>
						<c:if test='<%=viewBean.getValidLegalHeadFirstname().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_legal_head_firstname">
							Необходимо ввести имя.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidLegalHeadLastname().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_legal_head_lastname">
							Необходимо ввести фамилию.
							</div>
						</c:if>
					</li>
					
					<li class="form-line">
						<label class="form-label-left">Документ, подтверждающий полномочия юридического лица<span class="form-required">*</span></label>
						<div class="form-input-wide">
							<span class="form-sub-label-container">
								<input id="legal_procuratory_attach" name="legal_procuratory_attach" type="file" class="form-upload <c:if test='<%=viewBean.getValidLegalProcuratoryAttach().equals(viewBean.getBadFormatAttach()) || viewBean.getValidLegalProcuratoryAttach().equals(viewBean.getBadSizeAttach())%>'>form-validation-error</c:if>">
								<label class="form-sub-label">Файл <c:if test="${viewBean.editBean.formatAttach eq ''}">любого</c:if> формата <c:out value="${viewBean.editBean.formatAttach}"></c:out> размером не более <c:out value="${viewBean.editBean.maxSizeAttach}"></c:out> Мб</label>
							</span>
						</div>
						<c:if test='<%=viewBean.getValidLegalProcuratoryAttach().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_legal_procuratory_attach">
							Необходимо загрузить документ, подтверждающий полномочия юридического лица.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidLegalProcuratoryAttach().equals(viewBean.getBadFormatAttach())%>'>
							<div class="form-error-message for_id_legal_procuratory_attach">
								Недопустимый формат файла.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidLegalProcuratoryAttach().equals(viewBean.getBadSizeAttach())%>'>
							<div class="form-error-message for_id_legal_procuratory_attach">
								Файл превышает допустимый размер.
							</div>
						</c:if>
					</li>
					
					<li id="id_31" class="form-line">
						<label class="form-label-left">Должность руководителя<span class="form-required">*</span></label>
						<div class="form-input-wide">
							<input id="legal_as_head" name="legal_as_head" type="text"  value="<c:out value='<%=viewBean.getLegalAsHead()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidLegalAsHead().equals(viewBean.getIdle())%>'>form-validation-error</c:if>">
						</div>
						<c:if test='<%=viewBean.getValidLegalAsHead().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_legal_as_head">
							Необходимо ввести должность.
							</div>
						</c:if>
					</li>
		      
					<li id="id_32" class="form-line">
						<label class="form-label-left">Адрес<span class="form-required">*</span></label>
						<div class="form-input-wide">
							<span class="form-sub-label-container">
								<input id="legal_index" name="legal_index" type="text"  value="<c:out value='<%=viewBean.getLegalIndex()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidLegalIndex().equals(viewBean.getIdle())%>'>form-validation-error</c:if>">
								<label class="form-sub-label">Индекс</label>
							</span>
							<span class="form-sub-label-container">
								<input id="legal_country" name="legal_country" type="text"  value="<c:out value='<%=viewBean.getLegalCountry()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidLegalCountry().equals(viewBean.getIdle())%>'>form-validation-error</c:if>">
								<label class="form-sub-label">Страна</label>
							</span>
							<span class="form-sub-label-container">
								<input id="legal_city" name="legal_city" type="text"  value="<c:out value='<%=viewBean.getLegalCity()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidLegalCity().equals(viewBean.getIdle())%>'>form-validation-error</c:if>">
								<label class="form-sub-label">Город</label>
							</span>
							<span class="form-sub-label-container">
								<input id="legal_street" name="legal_street" type="text"  value="<c:out value='<%=viewBean.getLegalStreet()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidLegalStreet().equals(viewBean.getIdle())%>'>form-validation-error</c:if>">
								<label class="form-sub-label">Улица</label>
							</span>
							<span class="form-sub-label-container">
								<input id="legal_home" name="legal_home" type="text"  value="<c:out value='<%=viewBean.getLegalHome()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidLegalHome().equals(viewBean.getIdle())%>'>form-validation-error</c:if>">
								<label class="form-sub-label">Дом</label>
							</span>
							<span class="form-sub-label-container">
								<input id="legal_housing" name="legal_housing" type="text"  value="<c:out value='<%=viewBean.getLegalHousing()%>'></c:out>" class="form-textbox">
								<label class="form-sub-label">Корпус</label>
							</span>
							<span class="form-sub-label-container">
						        <input id="legal_apartment" name="legal_apartment" type="text"  value="<c:out value='<%=viewBean.getLegalApartment()%>'></c:out>" class="form-textbox">
								<label class="form-sub-label">Квартира</label>
							</span>
							<span class="form-sub-label-container">
								<input id="legal_section" name="legal_section" type="text"  value="<c:out value='<%=viewBean.getLegalSection()%>'></c:out>" class=" form-textbox">
								<label class="form-sub-label">Секция</label>
							</span>
							<span class="form-sub-label-container">
					        	<input id="legal_room" name="legal_room" type="text"  value="<c:out value='<%=viewBean.getLegalRoom()%>'></c:out>" class="form-textbox">
					            <label class="form-sub-label">Комната</label>
				            </span>
						</div>
						<c:if test='<%=viewBean.getValidLegalHome().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_legal_home">
							Необходимо ввести дом.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidLegalStreet().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_legal_street">
							Необходимо ввести улицу.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidLegalCity().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_legal_city">
							Необходимо ввести город.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidLegalCountry().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_legal_country">
							Необходимо ввести страну.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidLegalIndex().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_legal_index">
							Необходимо ввести индекс.
							</div>
						</c:if>
					</li>
		     
					<li id="id_44" class="form-line">
						<label class="form-label-left">Телефон<span class="form-required">*</span></label>
						<div class="form-input-wide">
							<input id="legal_number_telephone" name="legal_number_telephone" type="text"  value="<c:out value='<%=viewBean.getLegalNumberTelephone()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidLegalNumberTelephone().equals(viewBean.getIdle())%>'>form-validation-error</c:if>">
						</div>
						<c:if test='<%=viewBean.getValidLegalNumberTelephone().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_legal_number_telephone">
							Необходимо ввести номер телефона.
							</div>
						</c:if>
					</li>
					
					<li id="id_42" class="form-line">
						<label class="form-label-left">Адрес электронной почты<span class="form-required">*</span></label>
						<div class="form-input-wide">
						<span class="form-sub-label-container">
							<input id="legal_email" name="legal_email" type="text"  value="<c:out value='<%=viewBean.getLegalEmail()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidLegalEmail().equals(viewBean.getIdle()) || viewBean.getValidLegalEmail().equals(viewBean.getBadEmail())%>'>form-validation-error</c:if>">
							<label class="form-sub-label"> На указанный адрес будет направлено уведомление о готовности документа с указанием адреса службы одного окна, по которому будет выдаваться запрашиваемый документ. </label></span>
						</div>
						<c:if test='<%=viewBean.getValidLegalEmail().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_legal_email">
							Необходимо ввести адрес электронной почты.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidLegalEmail().equals(viewBean.getBadEmail())%>'>
							<div class="form-error-message for_id_legal_email">
							Адрес электронной почты введен некорректно.
							</div>
						</c:if>
					</li>
		      </ul>
		      
		      <ul id="details_contract" class="form-section" style="display: none;">
		      
					<li id="cid_43" class="form-input-wide">
						<div class="form-header-group">
							<h3 class="form-header">Введите реквизиты договора</h3>
						</div>
					</li>
					
					<li id="id_41" class="form-line">
						<label class="form-label-left">Номер договора</label>
						<div class="form-input-wide">
							<input id="contract_number" name="contract_number" type="text"  value="<c:out value='<%=viewBean.getContractNumber()%>'></c:out>" class="form-textbox">
						</div>
					</li>
		
					<li id="id_45" class="form-line">
						<label class="form-label-left">Дата договора</label>
						<div class="form-input-wide">
							<div class="form-single-column">
								<span class="form-radio-item">
									<input type="hidden" name="date_contract_val1" value="Точная дата">
									<jsp:setProperty name="viewBean" property="dateContractVal1" value="Точная дата"/>							
									<input id="exact_date_contract" name="date_contract" type="radio" value='<jsp:getProperty name="viewBean" property="dateContractVal1"/>' class="form-radio" <c:if test='<%=viewBean.getDateContract().equals(viewBean.getDateContractVal1())%>'>checked</c:if>>
									<label><jsp:getProperty name="viewBean" property="dateContractVal1"/></label>
									
									<div id="exact_date_contract_val_div" class="form-input-wide" style="display: none;">
		      	
										<span class="form-sub-label-container">
											<select id="day_exact_date_contract" name="day_exact_date_contract" class="form-dropdown">
												<option>  </option>
												<c:forEach var="i" begin="1" end="31" step="1" varStatus ="status">
													<option value="<c:out value='${i}'/>" <c:if test='${viewBean.dayExactDateContract eq i}'>selected</c:if>> <c:out value='${i}'/></option> 
												</c:forEach>
											</select>
											<label class="form-sub-label">День</label>
										</span>
								      	
								      	<span class="form-sub-label-container">
											<select id="month_exact_date_contract" name="month_exact_date_contract" class="form-dropdown">
												<option>  </option>
												<option value="1" <c:if test='${viewBean.monthExactDateContract eq 1}'>selected</c:if>>Январь</option>
												<option value="2" <c:if test='${viewBean.monthExactDateContract eq 2}'>selected</c:if>>Февраль</option>
												<option value="3" <c:if test='${viewBean.monthExactDateContract eq 3}'>selected</c:if>>Март</option>
												<option value="4" <c:if test='${viewBean.monthExactDateContract eq 4}'>selected</c:if>>Апрель</option>
												<option value="5" <c:if test='${viewBean.monthExactDateContract eq 5}'>selected</c:if>>Май</option>
												<option value="6" <c:if test='${viewBean.monthExactDateContract eq 6}'>selected</c:if>>Июнь</option>
												<option value="7" <c:if test='${viewBean.monthExactDateContract eq 7}'>selected</c:if>>Июль</option>
												<option value="8" <c:if test='${viewBean.monthExactDateContract eq 8}'>selected</c:if>>Август</option>
												<option value="9" <c:if test='${viewBean.monthExactDateContract eq 9}'>selected</c:if>>Сентябрь</option>
												<option value="10" <c:if test='${viewBean.monthExactDateContract eq 10}'>selected</c:if>>Октябрь</option>
												<option value="11" <c:if test='${viewBean.monthExactDateContract eq 11}'>selected</c:if>>Ноябрь</option>
												<option value="12" <c:if test='${viewBean.monthExactDateContract eq 12}'>selected</c:if>>Декабрь</option>
											</select>
											<label class="form-sub-label">Месяц</label>
								      	</span>
								      	
										<span class="form-sub-label-container">
											<select id="year_exact_date_contract" name="year_exact_date_contract" class="form-dropdown">
												<option> </option>
												<!-- 
												<c:forEach var="i" begin="1900" end="<%=new GregorianCalendar().get(Calendar.YEAR)+100%>" step="1" varStatus ="status">
													<option value="<c:out value='${i}'/>" <c:if test='${viewBean.yearExactDateContract eq i}'>selected</c:if>> <c:out value='${i}'/></option> 
												</c:forEach>
												-->
												<%
													for (int y = new GregorianCalendar().get(Calendar.YEAR); y >= 1900; y--) {
														if (viewBean.getYearExactDateContract().equals(String.valueOf(y))) {
															out.println("<option value='"+ y + "' selected>"+ y + "</option>");	
														} else {
															out.println("<option value='"+ y + "'>"+ y + "</option>");
														}
													}
												%>
											</select>
											<label class="form-sub-label">Год</label>
										</span>
										
									</div>
									
								</span>
								<div class="clear">&nbsp;</div>
								<span class="form-radio-item">
									<input type="hidden" name="date_contract_val2" value="Диапазон дат">
									<jsp:setProperty name="viewBean" property="dateContractVal2" value="Диапазон дат"/>
									<input id="date_range_contract" name="date_contract" type="radio" value='<jsp:getProperty name="viewBean" property="dateContractVal2"/>' class="form-radio" <c:if test='<%=viewBean.getDateContract().equals(viewBean.getDateContractVal2())%>'>checked</c:if>>
									<label><jsp:getProperty name="viewBean" property="dateContractVal2"/></label>
									
									<div id="date_range_contract_val_div" class="form-input-wide" style="display: none;">
									
										<span class="form-sub-label-container">
											<select id="day_date_range_contract1" name="day_date_range_contract1" class="form-dropdown">
												<option>  </option>
												<c:forEach var="i" begin="1" end="31" step="1" varStatus ="status">
													<option value="<c:out value='${i}'/>" <c:if test='${viewBean.dayDateRangeContract1 eq i}'>selected</c:if>> <c:out value='${i}'/></option> 
												</c:forEach>
											</select>
											<label class="form-sub-label">День</label>
										</span>
										
										<span class="form-sub-label-container">
											<select id="month_date_range_contract1" name="month_date_range_contract1" class="form-dropdown">
												<option>  </option>
												<option value="1" <c:if test='${viewBean.monthDateRangeContract1 eq 1}'>selected</c:if>>Январь</option>
												<option value="2" <c:if test='${viewBean.monthDateRangeContract1 eq 2}'>selected</c:if>>Февраль</option>
												<option value="3" <c:if test='${viewBean.monthDateRangeContract1 eq 3}'>selected</c:if>>Март</option>
												<option value="4" <c:if test='${viewBean.monthDateRangeContract1 eq 4}'>selected</c:if>>Апрель</option>
												<option value="5" <c:if test='${viewBean.monthDateRangeContract1 eq 5}'>selected</c:if>>Май</option>
												<option value="6" <c:if test='${viewBean.monthDateRangeContract1 eq 6}'>selected</c:if>>Июнь</option>
												<option value="7" <c:if test='${viewBean.monthDateRangeContract1 eq 7}'>selected</c:if>>Июль</option>
												<option value="8" <c:if test='${viewBean.monthDateRangeContract1 eq 8}'>selected</c:if>>Август</option>
												<option value="9" <c:if test='${viewBean.monthDateRangeContract1 eq 9}'>selected</c:if>>Сентябрь</option>
												<option value="10" <c:if test='${viewBean.monthDateRangeContract1 eq 10}'>selected</c:if>>Октябрь</option>
												<option value="11" <c:if test='${viewBean.monthDateRangeContract1 eq 11}'>selected</c:if>>Ноябрь</option>
												<option value="12" <c:if test='${viewBean.monthDateRangeContract1 eq 12}'>selected</c:if>>Декабрь</option>
											</select>
											<label class="form-sub-label">Месяц</label>
										</span>
										
										<span class="form-sub-label-container">
											<select id="year_date_range_contract1" name="year_date_range_contract1" class="form-dropdown">
												<option> </option>
												<!-- 
												<c:forEach var="i" begin="1900" end="<%=new GregorianCalendar().get(Calendar.YEAR)+100%>" step="1" varStatus ="status">
													<option value="<c:out value='${i}'/>" <c:if test='${viewBean.yearDateRangeContract1 eq i}'>selected</c:if>> <c:out value='${i}'/></option> 
												</c:forEach>
												-->
												<%
													for (int y = new GregorianCalendar().get(Calendar.YEAR); y >= 1900; y--) {
														if (viewBean.getYearDateRangeContract1().equals(String.valueOf(y))) {
															out.println("<option value='"+ y + "' selected>"+ y + "</option>");	
														} else {
															out.println("<option value='"+ y + "'>"+ y + "</option>");
														}
													}
												%>
											</select>
											<label class="form-sub-label">Год</label>
										</span>
										
										<span class="form-sub-label-container">
											<select id="day_date_range_contract2" name="day_date_range_contract2" class="form-dropdown">
												<option>  </option>
												<c:forEach var="i" begin="1" end="31" step="1" varStatus ="status">
													<option value="<c:out value='${i}'/>" <c:if test='${viewBean.dayDateRangeContract2 eq i}'>selected</c:if>> <c:out value='${i}'/></option> 
												</c:forEach>
											</select>
											<label class="form-sub-label">День</label>
										</span>
										
										<span class="form-sub-label-container">
											<select id="month_date_range_contract2" name="month_date_range_contract2" class="form-dropdown">
												<option>  </option>
												<option value="1" <c:if test='${viewBean.monthDateRangeContract2 eq 1}'>selected</c:if>>Январь</option>
												<option value="2" <c:if test='${viewBean.monthDateRangeContract2 eq 2}'>selected</c:if>>Февраль</option>
												<option value="3" <c:if test='${viewBean.monthDateRangeContract2 eq 3}'>selected</c:if>>Март</option>
												<option value="4" <c:if test='${viewBean.monthDateRangeContract2 eq 4}'>selected</c:if>>Апрель</option>
												<option value="5" <c:if test='${viewBean.monthDateRangeContract2 eq 5}'>selected</c:if>>Май</option>
												<option value="6" <c:if test='${viewBean.monthDateRangeContract2 eq 6}'>selected</c:if>>Июнь</option>
												<option value="7" <c:if test='${viewBean.monthDateRangeContract2 eq 7}'>selected</c:if>>Июль</option>
												<option value="8" <c:if test='${viewBean.monthDateRangeContract2 eq 8}'>selected</c:if>>Август</option>
												<option value="9" <c:if test='${viewBean.monthDateRangeContract2 eq 9}'>selected</c:if>>Сентябрь</option>
												<option value="10" <c:if test='${viewBean.monthDateRangeContract2 eq 10}'>selected</c:if>>Октябрь</option>
												<option value="11" <c:if test='${viewBean.monthDateRangeContract2 eq 11}'>selected</c:if>>Ноябрь</option>
												<option value="12" <c:if test='${viewBean.monthDateRangeContract2 eq 12}'>selected</c:if>>Декабрь</option>
											</select>
											<label class="form-sub-label">Месяц</label>
										</span>
										
										<span class="form-sub-label-container">
											<select id="year_date_range_contract2" name="year_date_range_contract2" class="form-dropdown">
												<option> </option>
												<!--
												<c:forEach var="i" begin="1900" end="<%=new GregorianCalendar().get(Calendar.YEAR)+100%>" step="1" varStatus ="status">
													<option value="<c:out value='${i}'/>" <c:if test='${viewBean.yearDateRangeContract2 eq i}'>selected</c:if>> <c:out value='${i}'/></option> 
												</c:forEach>
												-->
												<%
													for (int y = new GregorianCalendar().get(Calendar.YEAR); y >= 1900; y--) {
														if (viewBean.getYearDateRangeContract2().equals(String.valueOf(y))) {
															out.println("<option value='"+ y + "' selected>"+ y + "</option>");	
														} else {
															out.println("<option value='"+ y + "'>"+ y + "</option>");
														}
													}
												%>
											</select>
											<label class="form-sub-label">Год</label>
										</span>
							
									</div>
									
									<div class="clear">&nbsp;</div>
								</span>
							</div>
						</div>
					</li>
		      </ul>
		      
		      <ul id="kind_object_ul" class="form-section" style="display: none;">
					<li id="id_49" class="form-line">
						<div class="form-header-group">
							<h3>Укажите вид объекта</h3>
						</div>
						<label class="form-label-left">Что представляет собой объект<span class="form-required">*</span></label>
						<div class="form-input-wide" id="cid_49">
							<span class="form-sub-label-container">
								<input id="kind_object" name="kind_object" type="text"  value="<c:out value='<%=viewBean.getKindObject()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidKindObject().equals(viewBean.getIdle())%>'>form-validation-error</c:if>">
								<label class="form-sub-label">Например, квартира, здание детского сада, овощехранилище, земельный участок и т.п.</label>
							</span>
						</div>
						<c:if test='<%=viewBean.getValidKindObject().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_kind_object">
							Необходимо указать вид объекта.
							</div>
						</c:if>
					</li>
		      </ul>
		      
		      <ul id="address_object_ul" class="form-section" style="display: none;">
					<li id="id_50" class="form-line">
						<div class="form-header-group">
							<h3 class="form-header">Укажите адрес объекта</h3>
						</div>
						<label class="form-label-left">Адрес объекта<span class="form-required">*</span></label>
						<div class="form-input-wide">
							<span class="form-sub-label-container">
								<input id="object_city" name="object_city" type="text"  value="<c:out value='<%=viewBean.getObjectCity()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidObjectCity().equals(viewBean.getIdle())%>'>form-validation-error</c:if>">
								<label for="input_50" class="form-sub-label">Город</label>
							</span>
							<span class="form-sub-label-container">
					        	<input id="object_street" name="object_street" type="text"  value="<c:out value='<%=viewBean.getObjectStreet()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidObjectStreet().equals(viewBean.getIdle())%>'>form-validation-error</c:if>">
					            <label class="form-sub-label">Улица</label>
				            </span>
				            <span class="form-sub-label-container">
								<input id="object_home" name="object_home" type="text"  value="<c:out value='<%=viewBean.getObjectHome()%>'></c:out>" class="form-textbox <c:if test='<%=viewBean.getValidObjectHome().equals(viewBean.getIdle())%>'>form-validation-error</c:if>">
								<label class="form-sub-label">Дом</label>
							</span>
							<span class="form-sub-label-container">
								<input id="object_housing" name="object_housing" type="text"  value="<c:out value='<%=viewBean.getObjectHousing()%>'></c:out>" class="form-textbox">
								<label class="form-sub-label">Корпус</label>
							</span>
							<span class="form-sub-label-container">
								<input id="object_apartment" name="object_apartment" type="text"  value="<c:out value='<%=viewBean.getObjectApartment()%>'></c:out>" class="form-textbox">
								<label class="form-sub-label">Квартира</label>
							</span>
							<span class="form-sub-label-container">
								<input id="object_section" name="object_section" type="text"  value="<c:out value='<%=viewBean.getObjectSection()%>'></c:out>" class="form-textbox">
								<label class="form-sub-label">Секция</label>
							</span>
							<span class="form-sub-label-container">
								<input id="object_room" name="object_room" type="text"  value="<c:out value='<%=viewBean.getObjectRoom()%>'></c:out>" class="form-textbox">
								<label class="form-sub-label">Комната</label>
							</span>
						</div>
						<c:if test='<%=viewBean.getValidObjectHome().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_object_home">
							Необходимо ввести дом.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidObjectStreet().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_object_street">
							Необходимо ввести улицу.
							</div>
						</c:if>
						<c:if test='<%=viewBean.getValidObjectCity().equals(viewBean.getIdle())%>'>
							<div class="form-error-message for_id_object_city">
							Необходимо ввести город.
							</div>
						</c:if>
					</li>
		      </ul>
		      
		      <ul id="object_more_info1_ul" class="form-section" style="display: none;">
					<li id="id_57" class="form-line">
						<div class="form-header-group">
							<h3 class="form-header">Предоставьте дополнительные сведения об объекте</h3>
						</div>
						<label class="form-label-left">Сведения</label>
						<div class="form-input-wide">
							<span class="form-sub-label-container">
								<input id="object_more_info1" name="object_more_info1" type="text"  value="<c:out value='<%=viewBean.getObjectMoreInfo1()%>'></c:out>" class="form-textbox">
								<label class="form-sub-label">Поскольку по одному адресу может быть несколько объектов учета, желательно указать дополнительные сведения, которые могут облегчить идентификацию объекта&nbsp;&mdash; например, кадастровый или условный номер, а также иные характеристики объекта.</label>
							</span>
						</div>
					</li>
		      </ul>
		      
		      <ul id="object_more_info2_ul" class="form-section" style="display: none;">
					<li id="id_62" class="form-line">
						<div class="form-header-group">
							<h3 class="form-header">Предоставьте дополнительные сведения об объекте</h3>
						</div>
						<div class="form-input-wide" id="cid_62">
							<div class="form-single-column">
								<span class="form-radio-item">
									<input type="hidden" name="object_more_info2_val1" value="Местоположение объекта на карте">
									<jsp:setProperty name="viewBean" property="objectMoreInfo2Val1" value="Местоположение объекта на карте"/>
									<input id="object_location" name="object_more_info2" type="radio" value='<jsp:getProperty name="viewBean" property="objectMoreInfo2Val1"/>' class="form-radio" <c:if test='<%=viewBean.getObjectMoreInfo2().equals(viewBean.getObjectMoreInfo2Val1())%>'>checked</c:if>>
									<label><jsp:getProperty name="viewBean" property="objectMoreInfo2Val1"/></label>
								</span>
								
								<span id="object_url_geo_span" class="form-sub-label-container" style="display: none;">
									<input id="object_url_geo" name="object_url_geo" type="text"  value="<c:out value='<%=viewBean.getObjectUrlGeo()%>'></c:out>" class="form-textbox">
									<label class="form-sub-label">Гиперссылка на фрагмент карты в одной из геоинформационных систем: «Яндекс.Карты», «2ГИС», Google Maps, OpenStreetMap</label>
								</span>
								
								<c:if test='<%=viewBean.getValidObjectUrlGeo().equals(viewBean.getBadLink())%>'>
									<div class="form-error-message for_id_object_url_geo">
										Гиперссылка введена на фрагмент карты введена некорректно.
									</div>
								</c:if>
								
								<span id="object_other_info_span" class="form-sub-label-container" style="display: none;">
									<input id="object_other_info" name="object_other_info" type="text"  value="<c:out value='<%=viewBean.getObjectOtherInfo()%>'></c:out>" class="form-textbox">
								    <label class="form-sub-label">Иные сведения</label>
								</span>
							</div>
						</div>
						
						<div class="form-input-wide" id="cid_622345">
							<div class="form-single-column">
								<span  class="form-radio-item">
									<input type="hidden" name="object_more_info2_val2" value="Кадастровый номер земельного участка, на котором расположен объект">
									<jsp:setProperty name="viewBean" property="objectMoreInfo2Val2" value="Кадастровый номер земельного участка, на котором расположен объект"/>
									<input id="object_inventory_number" name="object_more_info2" type="radio" value='<jsp:getProperty name="viewBean" property="objectMoreInfo2Val2"/>' class="form-radio" <c:if test='<%=viewBean.getObjectMoreInfo2().equals(viewBean.getObjectMoreInfo2Val2())%>'>checked</c:if>>
									<label><jsp:getProperty name="viewBean" property="objectMoreInfo2Val2"/></label>
									
									<span id="object_inventory_number_val_span" class="form-sub-label-container" style="display: none;">
										<input id="object_inventory_number_val" name="object_inventory_number_val" type="text" value="<c:out value='<%=viewBean.getObjectInventoryNumberVal()%>'></c:out>" class="form-textbox">
										<label class="form-sub-label">Кадастровый номер</label>
									</span>
									<div class="clear">&nbsp;</div>
									
									<label class="form-label-left">Также рекомендуется приложить копию ордера на квартиру, копию домовой книги (для индивидуального жилого дома) или иной документ, содержащий сведения о предыдущем почтовом адресе, копию свидетельства, копию технического или кадастрового паспорта объекта.</label>
									<div id="div_object_attach" class="form-input-wide">
										<span class="form-sub-label-container">
											<input id="object_attach" name="object_attach" type="file" class="form-upload <c:if test='<%=viewBean.getValidObjectAttach().equals(viewBean.getBadFormatAttach()) || viewBean.getValidObjectAttach().equals(viewBean.getBadSizeAttach())%>'>form-validation-error</c:if>">
											<label class="form-sub-label">Файл <c:if test="${viewBean.editBean.formatAttach eq ''}">любого</c:if> формата <c:out value="${viewBean.editBean.formatAttach}"></c:out> размером не более <c:out value="${viewBean.editBean.maxSizeAttach}"></c:out> Мб</label>
										</span>
									</div>
									<c:if test='<%=viewBean.getValidObjectAttach().equals(viewBean.getBadFormatAttach())%>'>
										<div class="form-error-message for_object_attach">
											Недопустимый формат файла.
										</div>
									</c:if>
									<c:if test='<%=viewBean.getValidObjectAttach().equals(viewBean.getBadSizeAttach())%>'>
										<div class="form-error-message for_object_attach">
											Файл превышает допустимый размер.
										</div>
									</c:if>
								</span>
								
								
							</div>	
						</div>						
						
					</li>
					
					<li class="form-input-wide" id="cid_66">
						<div class="form-input-wide">
						<span class="form-sub-label-container">
						<label class="form-label-left">Дополнительные сведения о документе</label>
						<input id="object_doc" name="object_doc" type="text"  value="<c:out value='<%=viewBean.getObjectDoc()%>'></c:out>" class="form-textbox">
						<label class="form-sub-label">Вид, название документа или опорные слова из содержательной части документа по объекту, которому присвоен адрес</label>
						</span>
						</div>
					</li>
		
					<li id="id_68" class="form-line">
						<label class="form-label-left">Номер документа</label>
						<div class="form-input-wide">
							<input id="number_doc" name="number_doc" type="text"  value="<c:out value='<%=viewBean.getNumberDoc()%>'></c:out>" class=" form-textbox">
						</div>
					</li>
		      
					<li id="id_451234" class="form-line">
						<label id="label_452345" class="form-label-left">Дата издания документа</label>
						<div class="form-input-wide">
							<div class="form-single-column">
								<span class="form-radio-item">
									<input type="hidden" name="public_date_doc_val1" value="Точная дата">
									<jsp:setProperty name="viewBean" property="publicDateDocVal1" value="Точная дата"/>
									<input id="public_exact_date_doc" name="public_date_doc" type="radio" value='<jsp:getProperty name="viewBean" property="publicDateDocVal1"/>' class="form-radio" <c:if test='<%=viewBean.getPublicDateDoc().equals(viewBean.getPublicDateDocVal1())%>'>checked</c:if>>
									<label><jsp:getProperty name="viewBean" property="publicDateDocVal1"/></label>
									
									<div id="public_exact_date_doc_val_div" class="form-input-wide" style="display: none;">
		      	
										<span class="form-sub-label-container">
											<select id="public_day_exact_date_doc" name="public_day_exact_date_doc" class="form-dropdown">
												<option>  </option>
												<c:forEach var="i" begin="1" end="31" step="1" varStatus ="status">
													<option value="<c:out value='${i}'/>" <c:if test='${viewBean.publicDayExactDateDoc eq i}'>selected</c:if>> <c:out value='${i}'/></option> 
												</c:forEach>
											</select>
											<label class="form-sub-label">День</label>
										</span>
								      	
								      	<span class="form-sub-label-container">
											<select id="public_month_exact_date_doc" name="public_month_exact_date_doc" class="form-dropdown">
												<option>  </option>
												<option value="1" <c:if test='${viewBean.publicMonthExactDateDoc eq 1}'>selected</c:if>>Январь</option>
												<option value="2" <c:if test='${viewBean.publicMonthExactDateDoc eq 2}'>selected</c:if>>Февраль</option>
												<option value="3" <c:if test='${viewBean.publicMonthExactDateDoc eq 3}'>selected</c:if>>Март</option>
												<option value="4" <c:if test='${viewBean.publicMonthExactDateDoc eq 4}'>selected</c:if>>Апрель</option>
												<option value="5" <c:if test='${viewBean.publicMonthExactDateDoc eq 5}'>selected</c:if>>Май</option>
												<option value="6" <c:if test='${viewBean.publicMonthExactDateDoc eq 6}'>selected</c:if>>Июнь</option>
												<option value="7" <c:if test='${viewBean.publicMonthExactDateDoc eq 7}'>selected</c:if>>Июль</option>
												<option value="8" <c:if test='${viewBean.publicMonthExactDateDoc eq 8}'>selected</c:if>>Август</option>
												<option value="9" <c:if test='${viewBean.publicMonthExactDateDoc eq 9}'>selected</c:if>>Сентябрь</option>
												<option value="10" <c:if test='${viewBean.publicMonthExactDateDoc eq 10}'>selected</c:if>>Октябрь</option>
												<option value="11" <c:if test='${viewBean.publicMonthExactDateDoc eq 11}'>selected</c:if>>Ноябрь</option>
												<option value="12" <c:if test='${viewBean.publicMonthExactDateDoc eq 12}'>selected</c:if>>Декабрь</option>
											</select>
											<label class="form-sub-label">Месяц</label>
								      	</span>
								      	
										<span class="form-sub-label-container">
											<select id="public_year_exact_date_doc" name="public_year_exact_date_doc" class="form-dropdown">
												<option> </option>
												<!--
												<c:forEach var="i" begin="1900" end="<%=new GregorianCalendar().get(Calendar.YEAR)+100%>" step="1" varStatus ="status">
													<option value="<c:out value='${i}'/>" <c:if test='${viewBean.publicYearExactDateDoc eq i}'>selected</c:if>> <c:out value='${i}'/></option> 
												</c:forEach>
												-->
												<%
													for (int y = new GregorianCalendar().get(Calendar.YEAR); y >= 1900; y--) {
														if (viewBean.getPublicYearExactDateDoc().equals(String.valueOf(y))) {
															out.println("<option value='"+ y + "' selected>"+ y + "</option>");	
														} else {
															out.println("<option value='"+ y + "'>"+ y + "</option>");
														}
													}
												%>
											</select>
											<label class="form-sub-label">Год</label>
										</span>
							      	
									</div>
									
								</span>
								<div class="clear">&nbsp;</div>
								<span class="form-radio-item">
									<input type="hidden" name="public_date_doc_val2" value="Диапазон дат">
									<jsp:setProperty name="viewBean" property="publicDateDocVal2" value="Диапазон дат"/>
									<input id="public_date_range_doc" name="public_date_doc" type="radio" value='<jsp:getProperty name="viewBean" property="publicDateDocVal2"/>' class="form-radio" <c:if test='<%=viewBean.getPublicDateDoc().equals(viewBean.getPublicDateDocVal2())%>'>checked</c:if>>
									<label><jsp:getProperty name="viewBean" property="publicDateDocVal2"/></label>
									
									<div id="public_date_range_doc_val_div" class="form-input-wide" style="display: none;">
										
										<span class="form-sub-label-container">
											<select id="day_public_date_range_doc1" name="day_public_date_range_doc1" class="form-dropdown">
												<option>  </option>
												<c:forEach var="i" begin="1" end="31" step="1" varStatus ="status">
													<option value="<c:out value='${i}'/>" <c:if test='${viewBean.dayPublicDateRangeDoc1 eq i}'>selected</c:if>> <c:out value='${i}'/></option> 
												</c:forEach>
											</select>
											<label class="form-sub-label">День</label>
										</span>
								        
								        <span class="form-sub-label-container">
											<select id="month_public_date_range_doc1" name="month_public_date_range_doc1" class="form-dropdown">
												<option>  </option>
												<option value="1" <c:if test='${viewBean.monthPublicDateRangeDoc1 eq 1}'>selected</c:if>>Январь</option>
												<option value="2" <c:if test='${viewBean.monthPublicDateRangeDoc1 eq 2}'>selected</c:if>>Февраль</option>
												<option value="3" <c:if test='${viewBean.monthPublicDateRangeDoc1 eq 3}'>selected</c:if>>Март</option>
												<option value="4" <c:if test='${viewBean.monthPublicDateRangeDoc1 eq 4}'>selected</c:if>>Апрель</option>
												<option value="5" <c:if test='${viewBean.monthPublicDateRangeDoc1 eq 5}'>selected</c:if>>Май</option>
												<option value="6" <c:if test='${viewBean.monthPublicDateRangeDoc1 eq 6}'>selected</c:if>>Июнь</option>
												<option value="7" <c:if test='${viewBean.monthPublicDateRangeDoc1 eq 7}'>selected</c:if>>Июль</option>
												<option value="8" <c:if test='${viewBean.monthPublicDateRangeDoc1 eq 8}'>selected</c:if>>Август</option>
												<option value="9" <c:if test='${viewBean.monthPublicDateRangeDoc1 eq 9}'>selected</c:if>>Сентябрь</option>
												<option value="10" <c:if test='${viewBean.monthPublicDateRangeDoc1 eq 10}'>selected</c:if>>Октябрь</option>
												<option value="11" <c:if test='${viewBean.monthPublicDateRangeDoc1 eq 11}'>selected</c:if>>Ноябрь</option>
												<option value="12" <c:if test='${viewBean.monthPublicDateRangeDoc1 eq 12}'>selected</c:if>>Декабрь</option>
											</select>
											<label id="sublabel_month" for="input_48_month" class="form-sub-label">Месяц</label>
										</span>
								        
										<span class="form-sub-label-container">
											<select id="year_public_date_range_doc1" name="year_public_date_range_doc1" class="form-dropdown">
										        <option> </option>
										        <!--
												<c:forEach var="i" begin="1900" end="<%=new GregorianCalendar().get(Calendar.YEAR)+100%>" step="1" varStatus ="status">
													<option value="<c:out value='${i}'/>" <c:if test='${viewBean.yearPublicDateRangeDoc1 eq i}'>selected</c:if>> <c:out value='${i}'/></option> 
												</c:forEach>
												-->
												<%
													for (int y = new GregorianCalendar().get(Calendar.YEAR); y >= 1900; y--) {
														if (viewBean.getYearPublicDateRangeDoc1().equals(String.valueOf(y))) {
															out.println("<option value='"+ y + "' selected>"+ y + "</option>");	
														} else {
															out.println("<option value='"+ y + "'>"+ y + "</option>");
														}
													}
												%>
											</select>
											<label id="sublabel_year" for="input_48_year" class="form-sub-label">Год</label>
										</span>
										
										<span class="form-sub-label-container">
											<select id="day_public_date_range_doc2" name="day_public_date_range_doc2" class="form-dropdown">
												<option>  </option>
												<c:forEach var="i" begin="1" end="31" step="1" varStatus ="status">
													<option value="<c:out value='${i}'/>" <c:if test='${viewBean.dayPublicDateRangeDoc2 eq i}'>selected</c:if>> <c:out value='${i}'/></option> 
												</c:forEach>
											</select>
											<label class="form-sub-label">День</label>
										</span>
										
										<span class="form-sub-label-container">
											<select id="month_public_date_range_doc2" name="month_public_date_range_doc2" class="form-dropdown">
												<option>  </option>
												<option value="1" <c:if test='${viewBean.monthPublicDateRangeDoc2 eq 1}'>selected</c:if>>Январь</option>
												<option value="2" <c:if test='${viewBean.monthPublicDateRangeDoc2 eq 2}'>selected</c:if>>Февраль</option>
												<option value="3" <c:if test='${viewBean.monthPublicDateRangeDoc2 eq 3}'>selected</c:if>>Март</option>
												<option value="4" <c:if test='${viewBean.monthPublicDateRangeDoc2 eq 4}'>selected</c:if>>Апрель</option>
												<option value="5" <c:if test='${viewBean.monthPublicDateRangeDoc2 eq 5}'>selected</c:if>>Май</option>
												<option value="6" <c:if test='${viewBean.monthPublicDateRangeDoc2 eq 6}'>selected</c:if>>Июнь</option>
												<option value="7" <c:if test='${viewBean.monthPublicDateRangeDoc2 eq 7}'>selected</c:if>>Июль</option>
												<option value="8" <c:if test='${viewBean.monthPublicDateRangeDoc2 eq 8}'>selected</c:if>>Август</option>
												<option value="9" <c:if test='${viewBean.monthPublicDateRangeDoc2 eq 9}'>selected</c:if>>Сентябрь</option>
												<option value="10" <c:if test='${viewBean.monthPublicDateRangeDoc2 eq 10}'>selected</c:if>>Октябрь</option>
												<option value="11" <c:if test='${viewBean.monthPublicDateRangeDoc2 eq 11}'>selected</c:if>>Ноябрь</option>
												<option value="12" <c:if test='${viewBean.monthPublicDateRangeDoc2 eq 12}'>selected</c:if>>Декабрь</option>
											</select>
											<label class="form-sub-label">Месяц</label>
										</span>
										
										<span class="form-sub-label-container">
											<select id="year_public_date_range_doc2" name="year_public_date_range_doc2" class="form-dropdown">
												<option> </option>
												<!--
												<c:forEach var="i" begin="1900" end="<%=new GregorianCalendar().get(Calendar.YEAR)+100%>" step="1" varStatus ="status">
													<option value="<c:out value='${i}'/>" <c:if test='${viewBean.yearPublicDateRangeDoc2 eq i}'>selected</c:if>> <c:out value='${i}'/></option> 
												</c:forEach>
												-->
												<%
													for (int y = new GregorianCalendar().get(Calendar.YEAR); y >= 1900; y--) {
														if (viewBean.getYearPublicDateRangeDoc2().equals(String.valueOf(y))) {
															out.println("<option value='"+ y + "' selected>"+ y + "</option>");	
														} else {
															out.println("<option value='"+ y + "'>"+ y + "</option>");
														}
													}
												%>
											</select>
											<label class="form-sub-label">Год</label>
										</span>
							
									</div>
									
									<div class="clear">&nbsp;</div>
								</span>
							</div>
						</div>
					</li>
		
					<li id="id_73" class="form-line">
						<label class="form-label-left">Орган, издавший документ</label>
						<div class="form-input-wide">
							<span class="form-sub-label-container">
								<input id="author_doc" name="author_doc" type="text"  value="<c:out value='<%=viewBean.getAuthorDoc()%>'></c:out>" class="form-textbox">
								<label for="input_73" class="form-sub-label">Администрация соответствующего административного округа города Омска</label>
							</span>
						</div>
					</li>
				</ul>
				
				<ul class="form-section">
					
					<li id="id_75" class="form-line">
						<div class="form-input-wide" id="cid_75">
							<div class="form-html" id="text_75">
								<p class="MsoNormal"><span style="font-size: 10.0pt;">Заполнив форму и нажав кнопку «Отправить», я даю свое согласие Администрации города Омска, находящейся по адресу: город Омск, улица Гагарина, дом 34, на передачу, обработку и хранение моих персональных данных, указанных выше, с использованием средств автоматизации в соответствии с Федеральным законом от 27.07.2006 №&nbsp;152-ФЗ «О персональных данных».</span>
								</p>
							</div>
						</div>
					</li>
					
					<li id="id_2" class="form-line">
						<div class="form-input-wide" id="cid_2">
							<div class="form-buttons-wrapper" style="text-align:left">
								<!-- <button class="form-submit-button" type="submit" id="infodiffrentareas_send">
								Отправить
								</button>  -->
								<input class="form-submit-button" type="submit" id="infodiffrentareas_send" value="Отправить">
								
							</div>
						</div>
					</li>
		      
				</ul>
			
			</div>
	
		</form>
		
	</div>
	</c:if>
</c:if>
<!-- Установить состояние ожидания, т.е вид - форма с полями -->
<jsp:scriptlet>viewBean.setSent(ViewBean.SENT_EXCEPT);</jsp:scriptlet>