<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<%@ page import="ru.admomsk.server.InfoDifferentAreas" %>

<portlet:defineObjects />
<jsp:useBean id="editBean" class="ru.admomsk.server.beans.EditBean" scope="request"/>

<div id="kindergarden">
	<form id="form_edit_info_different_areas" name="form_edit_info_different_areas" enctype="application/x-www-form-urlencoded" action="<portlet:actionURL />" method="post">
		<div class="form-all">
		
			<ul class="form-section">
				<li class="form-line">
					<label class="form-label-left">E-mail почтового ящика СЭД<span class="form-required">*</span></label>
					<div class="form-input-wide">
						<span class="form-sub-label-container">
							<input id="email_sed" name="email_sed" type="text" size="30" value="<c:out value="${editBean.to}"></c:out>" >
							<label class="form-sub-label">На данный адрес отсылается email.</label>
						</span>
					</div>
					<c:if test="${editBean.validTo eq editBean.idle}">
						<div class="form-error-message for_email_sed">
	  						Поле обязательно для заполнения.
	  					</div>
					</c:if>
					<c:if test="${editBean.validTo eq editBean.badEmail}">
						<div class="form-error-message for_email_sed">
	  						Адрес электронной почты введен некорректно.
	  					</div>
					</c:if>
				</li>
			</ul>
			
			<ul class="form-section">
				<li class="form-line">
					<label class="form-label-left">SMPT<span class="form-required">*</span></label>
					<div class="form-input-wide">
						<span class="form-sub-label-container">
							<input id="email_smpt" name="email_smpt" type="text" size="30" value="<c:out value="${editBean.host}"></c:out>" >
							<label class="form-sub-label">Введите SMPT в формате x.x.x.x</label>
						</span>
					</div>
					<c:if test="${editBean.validHost eq editBean.idle}">
		  				<div class="form-error-message for_email_smpt">
	  						Поле обязательно для заполнения.
	  					</div>
					</c:if>
				</li>
			</ul>
			
			<ul class="form-section">
				<li class="form-line">
					<label class="form-label-left">Порт<span class="form-required">*</span></label>
					<div class="form-input-wide">
						<span class="form-sub-label-container">
							<input id="email_port" name="email_port" type="text" size="30" value="<c:out value="${editBean.port}"></c:out>" >
							<label class="form-sub-label">Введите номер порта.</label>
						</span>
					</div>
					<c:if test="${editBean.validPort eq editBean.idle}">
						<div class="form-error-message for_email_port">
		  					Поле обязательно для заполнения.
		  				</div>
					</c:if>
					<c:if test="${editBean.validPort eq editBean.badPort}">
						<div class="form-error-message for_email_port">
	  						Порт должен содержать только цифры.
	  					</div>
					</c:if>
				</li>
			</ul>
			
			<ul class="form-section">
				<li class="form-line">
					<label class="form-label-left">Email портала<span class="form-required">*</span></label>
					<div class="form-input-wide">
						<span class="form-sub-label-container">
							<input id="email_from" name="email_from" type="text" size="30" value="<c:out value="${editBean.from}"></c:out>" >
							<label class="form-sub-label">С данного адреса отсылается email.</label>
						</span>
					</div>
					<c:if test="${editBean.validFrom eq editBean.idle}">
						<div class="form-error-message for_email_from">
	  						Поле обязательно для заполнения.
	  					</div>
					</c:if>
					<c:if test="${editBean.validFrom eq editBean.badEmail}">
						<div class="form-error-message for_email_from">
	  						Адрес электронной почты введен некорректно.
	  					</div>
					</c:if>
				</li>
			</ul>
			
			<ul class="form-section">
				<li class="form-line">
					<label class="form-label-left">Пароль email портала</label>
					<div class="form-input-wide">
						<span class="form-sub-label-container">
							<input id="pass_email_from" name="pass_email_from" type="password" size="30" value="<c:out value="${editBean.passFrom}"></c:out>" >
							<label class="form-sub-label">Пароль email портала, то есть адреса с которого отсылается почта.</label>
						</span>
					</div>
				</li>
			</ul>
			
			<ul class="form-section">
				<li class="form-line">
					<label class="form-label-left">Максимальный размер прикрепляемого файла<span class="form-required">*</span></label>
					<div class="form-input-wide">
						<span class="form-sub-label-container">
							<input id="max_size_attach" name="max_size_attach" type="text" size="30" value="<c:out value="${editBean.maxSizeAttach}"></c:out>" >
							<label class="form-sub-label">Размер указывается в Мбайт.</label>
						</span>
					</div>
					<c:if test="${editBean.validMaxSizeAttach eq editBean.idle}">
						<div class="form-error-message for_max_size_attach">
	  						Поле обязательно для заполнения.
	  					</div>
					</c:if>
					<c:if test="${editBean.validMaxSizeAttach eq editBean.badMaxSizeAttach}">
						<div class="form-error-message for_max_size_attach">
	  						Некорректны ввод. Допустимыми являются цифры и символ точки.
	  					</div>
					</c:if>
				</li>
			</ul>
			
			<ul class="form-section">
				<li class="form-line">
					<label class="form-label-left">Допустимые форматы прикрепляемого файла<span class="form-required">*</span></label>
					<div class="form-input-wide">
						<span class="form-sub-label-container">
							<input id="format_attach" name="format_attach" type="text" size="30" value="<c:out value="${editBean.formatAttach}"></c:out>" >
							<label class="form-sub-label">Форматы указать через запятую с пробелом, пустая строка означает - без ограничений.</label>
						</span>
					</div>
				</li>
			</ul>
			
			<ul class="form-section">
				<li class="form-line">
					<div class="form-input-wide" id="cid_2">
						<div class="form-buttons-wrapper" style="text-align:left">
							<button class="form-submit-button" type="submit" id="save_info_different_areas">
							Сохранить
							</button>
						</div>
					</div>
				</li>
			</ul>
			
		</div>
	</form>
</div>
