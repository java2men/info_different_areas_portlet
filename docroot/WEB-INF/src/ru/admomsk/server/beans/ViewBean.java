package ru.admomsk.server.beans;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;

import javax.portlet.ReadOnlyException;

import org.apache.commons.io.FileUtils;
import ru.admomsk.utils.MyUtils;
import ru.admomsk.utils.SendMailUsage;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.FileUtil;

public class ViewBean implements Serializable {
	
	private static final long serialVersionUID = -5375463793530628306L;

	//Закончил на тему логирования, как вести логи, на случай если произойдет ошибка
	//надо поправить xml ку
	//убедиться что файлы в temp удаляются после отправки письма
	private static Log _log = LogFactoryUtil.getLog("InfoDifferentАreas");
	
	static public String SENT_SUCCESSFULLY = "SENT_SUCCESSFULLY";
	static public String SENT_FAILS = "SENT_FAILS";
	static public String SENT_EXCEPT = "SENT_EXCEPT";
	
	private String badEmail = "bad_email";
	private String badLink = "bad_link";
	private String idle = "idle";
	private String ok = "ok";
	private String badSizeAttach = "bad_size_attach";
	private String badFormatAttach = "bad_format_attach";
	
	private String reqDocGroup = "";
	private String reqDocGroupVal1 = "";
	private String reqDocGroupVal2 = "";
	//private String reqDocGroupVal2Val1 = "";
	//private String reqDocGroupVal2Val2 = "";
	private String reqDocGroupVal3 = "";
	private String reqDocGroupVal4 = "";
	private String reqDocGroupVal5 = "";
	private String reqDocGroupVal6 = "";
	private String reqDocGroupVal7 = "";
	/*bo//private String reqDocGroupVal8 = "";*/
	/*bo//private String reqDocGroupVal9 = "";*/
	private String reqDocGroupVal10 = "";
	private String reqDocGroupVal11 = "";
	private String reqDocGroupVal12 = "";
	
	private String purposeRequest = "";
	private String typeApplicant = "";
	private String typeApplicantVal1 = "";
	private String typeApplicantVal2 = "";
	
	private String individualLastname = "";
	private String individualFirstname = "";
	private String individualMiddlename = "";
	private String individualDayBirthday = "";
	private String individualMonthBirthday = "";
	private String individualYearBirthday = "";
	private String individualChangeLastFirstMiddleName = "";
	private String individualSeriesPassport = "";
	private String individualNumberPassport = "";
	private String individualWhoGivePassport = "";
	private String individualDayIssuePassport = "";
	private String individualMonthIssuePassport = "";
	private String individualYearIssuePassport = "";
	private File individualAttachPassport = null;
	private File individualProcuratoryAttachPassport = null;
	private String individualCity = "";
	private String individualStreet = "";
	private String individualHome = "";
	private String individualHousing = "";
	private String individualApartment = "";
	private String individualSection = "";
	private String individualRoom = "";
	private String individualNumberTelephone = "";
	private String individualEmail = "";
	
	
	private String legalName = "";
	private String legalOgrn = "";
	private String legalInn = "";
	private String legalHeadLastname = "";
	private String legalHeadFirstname = "";
	private String legalHeadMiddlename = "";
	private File legalProcuratoryAttach = null;
	private String legalAsHead = "";
	private String legalIndex = "";
	private String legalCountry = "";
	private String legalCity = "";
	private String legalStreet = "";
	private String legalHome = "";
	private String legalHousing = "";
	private String legalApartment = "";
	private String legalSection = "";
	private String legalRoom = "";
	private String legalNumberTelephone = "";
	private String legalEmail = "";
	
	private String contractNumber = "";
	private String dateContract = "";
	private String dateContractVal1 = "";
	private String dateContractVal2 = "";
	private String dayExactDateContract = "";
	private String monthExactDateContract = "";
	private String yearExactDateContract = "";
	private String dayDateRangeContract1 = "";
	private String monthDateRangeContract1 = "";
	private String yearDateRangeContract1 = "";
	private String dayDateRangeContract2 = "";
	private String monthDateRangeContract2 = "";
	private String yearDateRangeContract2 = "";
	
	private String yearHousingProgram = "";
	private String yearHousingProgramVal1 = "";
	private String yearHousingProgramVal2 = "";
	private String yearHousingProgramVal3 = "";
	private String kindObject = "";
	private String objectCity = "";
	private String objectStreet = "";
	private String objectHome = "";
	private String objectHousing = "";
	private String objectApartment = "";
	private String objectSection = "";
	private String objectRoom = "";
	private String objectMoreInfo1 = "";
	private String objectMoreInfo2 = "";
	private String objectMoreInfo2Val1 = "";
	private String objectMoreInfo2Val2 = "";
	private String objectUrlGeo = "";
	private String objectOtherInfo = "";
	//private String objectInventoryNumber = "";
	private String objectInventoryNumberVal = "";
	private File objectAttach = null;
	private String objectDoc = "";
	private String numberDoc = "";
	private String publicDateDoc = "";
	private String publicDateDocVal1 = "";
	private String publicDateDocVal2 = "";
	private String publicDayExactDateDoc = "";
	private String publicMonthExactDateDoc = "";
	private String publicYearExactDateDoc = "";
	private String dayPublicDateRangeDoc1 = "";
	private String monthPublicDateRangeDoc1 = "";
	private String yearPublicDateRangeDoc1 = "";
	private String dayPublicDateRangeDoc2 = "";
	private String monthPublicDateRangeDoc2 = "";
	private String yearPublicDateRangeDoc2 = "";
	private String authorDoc = "";
	
	/////////////////////////////!!!!!!!!!!!!!
	private String validReqDocGroup = ok;
	private String validPurposeRequest = ok;
	private String validTypeApplicant = ok;
	
	private String validIndividualLastname = ok;
	private String validIndividualFirstname = ok;
	private String validIndividualDayBirthday = ok;
	private String validIndividualMonthBirthday = ok;
	private String validIndividualYearBirthday = ok;
	private String validIndividualSeriesPassport = ok;
	private String validIndividualNumberPassport = ok;
	private String validIndividualWhoGivePassport = ok;
	private String validIndividualDayIssuePassport = ok;
	private String validIndividualMonthIssuePassport = ok;
	private String validIndividualYearIssuePassport = ok;
	private String validIndividualAttachPassport = ok;
	private String validIndividualProcuratoryAttachPassport = ok;
	private String validIndividualCity = ok;
	private String validIndividualStreet = ok;
	private String validIndividualHome = ok;
	private String validIndividualNumberTelephone = ok;
	private String validIndividualEmail = ok;
	
	private String validLegalName = ok;
	private String validLegalOgrn = ok;
	private String validLegalInn = ok;
	private String validLegalHeadLastname = ok;
	private String validLegalHeadFirstname = ok;
	private String validLegalProcuratoryAttach = ok;
	private String validLegalAsHead = ok;
	private String validLegalIndex = ok;
	private String validLegalCountry = ok;
	private String validLegalCity = ok;
	private String validLegalStreet = ok;
	private String validLegalHome = ok;
	private String validLegalNumberTelephone = ok;
	private String validLegalEmail = ok;
	
	private String validYearHousingProgram = ok;
	private String validKindObject = ok;
	private String validObjectCity = ok;
	private String validObjectStreet = ok;
	private String validObjectHome = ok;
	private String validObjectUrlGeo = ok;
	private String validObjectAttach = ok;
	
	//private File attach = null;
	//private File tempAttach;
	private File requestDir;
	private EditBean editBean;
	private String viewJSPURL;
	//private String realAttachName;
	//private String regexFormatAttach = ", ";
	
	private boolean validAll = true;
	private String sent = SENT_EXCEPT;
	//private boolean viewForm = true;
	//private boolean back = false;
	//
	/*private boolean editValidAll = false;*/
	private boolean processing = false;
	
	//private Exception exception = null;
	//private String localizedMessage = "";
	/*
	public String NOT_EMAIL = "not_email";
	public String EMPTY = "empty";
	public String OK = "ok";
	*/
	
	public ViewBean() {
		
	}
	
	public String getBadEmail() {
		return badEmail;
	}
	public void setBadEmail(String badEmail) {
		this.badEmail = badEmail;
	}
	public String getBadLink() {
		return badLink;
	}
	public void setBadLink(String badLink) {
		this.badLink = badLink;
	}
	public String getIdle() {
		return idle;
	}
	public void setIdle(String idle) {
		this.idle = idle;
	}
	public String getOk() {
		return ok;
	}
	public void setOk(String ok) {
		this.ok = ok;
	}
	public String getBadSizeAttach() {
		return badSizeAttach;
	}
	public void setBadSizeAttach(String badSizeAttach) {
		this.badSizeAttach = badSizeAttach;
	}
	public String getBadFormatAttach() {
		return badFormatAttach;
	}
	public void setBadFormatAttach(String badFormatAttach) {
		this.badFormatAttach = badFormatAttach;
	}
	public String getReqDocGroup() {
		return reqDocGroup;
	}
	public void setReqDocGroup(String reqDocGroup) {
		if (reqDocGroup == null) {
			this.reqDocGroup = "";
		}
		else {
			this.reqDocGroup = reqDocGroup;
		}
	}
	public String getReqDocGroupVal1() {
		return reqDocGroupVal1;
	}
	public void setReqDocGroupVal1(String reqDocGroupVal1) {
		this.reqDocGroupVal1 = reqDocGroupVal1;
	}
	public String getReqDocGroupVal2() {
		return reqDocGroupVal2;
	}
	public void setReqDocGroupVal2(String reqDocGroupVal2) {
		this.reqDocGroupVal2 = reqDocGroupVal2;
	}
	public String getReqDocGroupVal3() {
		return reqDocGroupVal3;
	}
	public void setReqDocGroupVal3(String reqDocGroupVal3) {
		this.reqDocGroupVal3 = reqDocGroupVal3;
	}
	public String getReqDocGroupVal4() {
		return reqDocGroupVal4;
	}
	public void setReqDocGroupVal4(String reqDocGroupVal4) {
		this.reqDocGroupVal4 = reqDocGroupVal4;
	}
	public String getReqDocGroupVal5() {
		return reqDocGroupVal5;
	}
	public void setReqDocGroupVal5(String reqDocGroupVal5) {
		this.reqDocGroupVal5 = reqDocGroupVal5;
	}
	public String getReqDocGroupVal6() {
		return reqDocGroupVal6;
	}
	public void setReqDocGroupVal6(String reqDocGroupVal6) {
		this.reqDocGroupVal6 = reqDocGroupVal6;
	}
	public String getReqDocGroupVal7() {
		return reqDocGroupVal7;
	}
	public void setReqDocGroupVal7(String reqDocGroupVal7) {
		this.reqDocGroupVal7 = reqDocGroupVal7;
	}
	/*bo//public String getReqDocGroupVal8() {
		return reqDocGroupVal8;
	}public void setReqDocGroupVal8(String reqDocGroupVal8) {
		this.reqDocGroupVal8 = reqDocGroupVal8;
	}*/
	/*bo//public String getReqDocGroupVal9() {
		return reqDocGroupVal9;
	}
	public void setReqDocGroupVal9(String reqDocGroupVal9) {
		this.reqDocGroupVal9 = reqDocGroupVal9;
	}*/
	public String getReqDocGroupVal10() {
		return reqDocGroupVal10;
	}
	public void setReqDocGroupVal10(String reqDocGroupVal10) {
		this.reqDocGroupVal10 = reqDocGroupVal10;
	}
	public String getReqDocGroupVal11() {
		return reqDocGroupVal11;
	}
	public void setReqDocGroupVal11(String reqDocGroupVal11) {
		this.reqDocGroupVal11 = reqDocGroupVal11;
	}
	public String getReqDocGroupVal12() {
		return reqDocGroupVal12;
	}
	public void setReqDocGroupVal12(String reqDocGroupVal12) {
		this.reqDocGroupVal12 = reqDocGroupVal12;
	}
	public String getPurposeRequest() {
		return purposeRequest;
	}
	public void setPurposeRequest(String purposeRequest) {
		if (purposeRequest == null) {
			this.purposeRequest = "";
		}
		else {
			this.purposeRequest = purposeRequest;
		}
	}
	public String getTypeApplicant() {
		return typeApplicant;
	}
	public void setTypeApplicant(String typeApplicant) {
		if (typeApplicant == null) {
			this.typeApplicant = "";
		}
		else {
			this.typeApplicant = typeApplicant;
		}
	}
	public String getTypeApplicantVal1() {
		return typeApplicantVal1;
	}
	public void setTypeApplicantVal1(String typeApplicantVal1) {
		this.typeApplicantVal1 = typeApplicantVal1;
	}
	public String getTypeApplicantVal2() {
		return typeApplicantVal2;
	}
	public void setTypeApplicantVal2(String typeApplicantVal2) {
		this.typeApplicantVal2 = typeApplicantVal2;
	}
	public String getIndividualLastname() {
		return individualLastname;
	}
	public void setIndividualLastname(String individualLastname) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualLastname == null) {
				this.individualLastname = "";
			} else {
				this.individualLastname = individualLastname;
			}
		}
	}
	public String getIndividualFirstname() {
		return individualFirstname;
	}
	public void setIndividualFirstname(String individualFirstname) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualFirstname == null) {
				this.individualFirstname = "";
			} else {
				this.individualFirstname = individualFirstname;
			}
		}
	}
	public String getIndividualMiddlename() {
		return individualMiddlename;
	}
	public void setIndividualMiddlename(String individualMiddlename) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualMiddlename == null) {
				this.individualMiddlename = "";
			} else {
				this.individualMiddlename = individualMiddlename;
			}
		}
	}
	public String getIndividualDayBirthday() {
		return individualDayBirthday;
	}
	public void setIndividualDayBirthday(String individualDayBirthday) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualDayBirthday == null) {
				this.individualDayBirthday = "";
			} else {
				this.individualDayBirthday = individualDayBirthday;
			}
		}
	}
	public String getIndividualMonthBirthday() {
		return individualMonthBirthday;
	}
	public void setIndividualMonthBirthday(String individualMonthBirthday) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualMonthBirthday == null) {
				this.individualMonthBirthday = "";
			} else {
				this.individualMonthBirthday = individualMonthBirthday;
			}
		}
	}
	public String getIndividualYearBirthday() {
		return individualYearBirthday;
	}
	public void setIndividualYearBirthday(String individualYearBirthday) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualYearBirthday == null) {
				this.individualYearBirthday = "";
			} else {
				this.individualYearBirthday = individualYearBirthday;
			}
		}
	}
	public String getIndividualChangeLastFirstMiddleName() {
		return individualChangeLastFirstMiddleName;
	}
	public void setIndividualChangeLastFirstMiddleName(String individualChangeLastFirstMiddleName) {
		
		if (reqDocGroup.equals(reqDocGroupVal1) || 
			reqDocGroup.equals(reqDocGroupVal2) ||
			reqDocGroup.equals(reqDocGroupVal3) ||
			reqDocGroup.equals(reqDocGroupVal4) ||
			reqDocGroup.equals(reqDocGroupVal5) ||
			reqDocGroup.equals(reqDocGroupVal6) ||
			reqDocGroup.equals(reqDocGroupVal7)
		) {
			if (typeApplicant.equals(typeApplicantVal1)) {
				if (individualChangeLastFirstMiddleName == null) {
					this.individualChangeLastFirstMiddleName = "";
				} else {
					this.individualChangeLastFirstMiddleName = individualChangeLastFirstMiddleName;
				}
			}
		}
		
	}
	public String getIndividualSeriesPassport() {
		return individualSeriesPassport;
	}
	public void setIndividualSeriesPassport(String individualSeriesPassport) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualSeriesPassport == null) {
				this.individualSeriesPassport = "";
			} else {
				this.individualSeriesPassport = individualSeriesPassport;
			}
		}
	}
	public String getIndividualNumberPassport() {
		return individualNumberPassport;
	}
	public void setIndividualNumberPassport(String individualNumberPassport) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualNumberPassport == null) {
				this.individualNumberPassport = "";
			} else {
				this.individualNumberPassport = individualNumberPassport;
			}
		}
	}
	public String getIndividualWhoGivePassport() {
		return individualWhoGivePassport;
	}
	public void setIndividualWhoGivePassport(String individualWhoGivePassport) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualWhoGivePassport == null) {
				this.individualWhoGivePassport = "";
			} else {
				this.individualWhoGivePassport = individualWhoGivePassport;
			}
		}
	}
	public String getIndividualDayIssuePassport() {
		return individualDayIssuePassport;
	}
	public void setIndividualDayIssuePassport(String individualDayIssuePassport) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualDayIssuePassport == null) {
				this.individualDayIssuePassport = "";
			} else {
				this.individualDayIssuePassport = individualDayIssuePassport;
			}
		}
	}
	public String getIndividualMonthIssuePassport() {
		return individualMonthIssuePassport;
	}
	public void setIndividualMonthIssuePassport(String individualMonthIssuePassport) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualMonthIssuePassport == null) {
				this.individualMonthIssuePassport = "";
			} else {
				this.individualMonthIssuePassport = individualMonthIssuePassport;
			}
		}
	}
	public String getIndividualYearIssuePassport() {
		return individualYearIssuePassport;
	}
	public void setIndividualYearIssuePassport(String individualYearIssuePassport) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualYearIssuePassport == null) {
				this.individualYearIssuePassport = "";
			} else {
				this.individualYearIssuePassport = individualYearIssuePassport;
			}
		}
	}
	public File getIndividualAttachPassport() {
		return individualAttachPassport;
	}
	public void setIndividualAttachPassport(File individualAttachPassport) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualAttachPassport == null) {
				this.individualAttachPassport = null;
			} else {
				this.individualAttachPassport = individualAttachPassport;
			}
		}
	}
	public File getIndividualProcuratoryAttachPassport() {
		return individualProcuratoryAttachPassport;
	}
	public void setIndividualProcuratoryAttachPassport(File individualProcuratoryAttachPassport) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualProcuratoryAttachPassport == null) {
				this.individualProcuratoryAttachPassport = null;
			} else {
				this.individualProcuratoryAttachPassport = individualProcuratoryAttachPassport;
			}
		}
	}
	public String getIndividualCity() {
		return individualCity;
	}
	public void setIndividualCity(String individualCity) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualCity == null) {
				this.individualCity = "";
			} else {
				this.individualCity = individualCity;
			}
		}
	}
	public String getIndividualStreet() {
		return individualStreet;
	}
	public void setIndividualStreet(String individualStreet) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualStreet == null) {
				this.individualStreet = "";
			} else {
				this.individualStreet = individualStreet;
			}
		}
	}
	public String getIndividualHome() {
		return individualHome;
	}
	public void setIndividualHome(String individualHome) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualHome == null) {
				this.individualHome = "";
			} else {
				this.individualHome = individualHome;
			}
		}
	}
	public String getIndividualHousing() {
		return individualHousing;
	}
	public void setIndividualHousing(String individualHousing) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualHousing == null) {
				this.individualHousing = "";
			} else {
				this.individualHousing = individualHousing;
			}
		}
	}
	public String getIndividualApartment() {
		return individualApartment;
	}
	public void setIndividualApartment(String individualApartment) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualApartment == null) {
				this.individualApartment = "";
			} else {
				this.individualApartment = individualApartment;
			}
		}
	}
	public String getIndividualSection() {
		return individualSection;
	}
	public void setIndividualSection(String individualSection) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualSection == null) {
				this.individualSection = "";
			} else {
				this.individualSection = individualSection;
			}
		}
	}
	public String getIndividualRoom() {
		return individualRoom;
	}
	public void setIndividualRoom(String individualRoom) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualRoom == null) {
				this.individualRoom = "";
			} else {
				this.individualRoom = individualRoom;
			}
		}
	}
	public String getIndividualNumberTelephone() {
		return individualNumberTelephone;
	}
	public void setIndividualNumberTelephone(String individualNumberTelephone) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualNumberTelephone == null) {
				this.individualNumberTelephone = "";
			} else {
				this.individualNumberTelephone = individualNumberTelephone;
			}
		}
	}
	public String getIndividualEmail() {
		return individualEmail;
	}
	public void setIndividualEmail(String individualEmail) {
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualEmail == null) {
				this.individualEmail = "";
			} else {
				this.individualEmail = individualEmail;
			}
		}
	}
	public String getLegalName() {
		return legalName;
	}
	public void setLegalName(String legalName) {
		if (typeApplicant.equals(typeApplicantVal2)) {
			if (legalName == null) {
				this.legalName = "";
			} else {
				this.legalName = legalName;
			}
		}
	}
	public String getLegalOgrn() {
		return legalOgrn;
	}
	public void setLegalOgrn(String legalOgrn) {
		if (typeApplicant.equals(typeApplicantVal2)) {
			if (legalOgrn == null) {
				this.legalOgrn = "";
			} else {
				this.legalOgrn = legalOgrn;
			}
		}
	}
	public String getLegalInn() {
		return legalInn;
	}
	public void setLegalInn(String legalInn) {
		if (typeApplicant.equals(typeApplicantVal2)) {
			if (legalInn == null) {
				this.legalInn = "";
			} else {
				this.legalInn = legalInn;
			}
		}
	}
	public String getLegalHeadLastname() {
		return legalHeadLastname;
	}
	public void setLegalHeadLastname(String legalHeadLastname) {
		if (typeApplicant.equals(typeApplicantVal2)) {
			if (legalHeadLastname == null) {
				this.legalHeadLastname = "";
			} else {
				this.legalHeadLastname = legalHeadLastname;
			}
		}
	}
	public String getLegalHeadFirstname() {
		return legalHeadFirstname;
	}
	public void setLegalHeadFirstname(String legalHeadFirstname) {
		if (typeApplicant.equals(typeApplicantVal2)) {
			if (legalHeadFirstname == null) {
				this.legalHeadFirstname = "";
			} else {
				this.legalHeadFirstname = legalHeadFirstname;
			}
		}
	}
	public String getLegalHeadMiddlename() {
		return legalHeadMiddlename;
	}
	public void setLegalHeadMiddlename(String legalHeadMiddlename) {
		if (typeApplicant.equals(typeApplicantVal2)) {
			if (legalHeadMiddlename == null) {
				this.legalHeadMiddlename = "";
			} else {
				this.legalHeadMiddlename = legalHeadMiddlename;
			}
		}
	}
	
	public File getLegalProcuratoryAttachPassport() {
		return legalProcuratoryAttach;
	}
	public void setLegalProcuratoryAttach(File legalProcuratoryAttach) {
		if (typeApplicant.equals(typeApplicantVal2)) {
			if (legalProcuratoryAttach == null) {
				this.legalProcuratoryAttach = null;
			} else {
				this.legalProcuratoryAttach = legalProcuratoryAttach;
			}
		}
	}
	
	public String getLegalAsHead() {
		return legalAsHead;
	}
	public void setLegalAsHead(String legalAsHead) {
		if (typeApplicant.equals(typeApplicantVal2)) {
			if (legalAsHead == null) {
				this.legalAsHead = "";
			} else {
				this.legalAsHead = legalAsHead;
			}
		}
	}
	public String getLegalIndex() {
		return legalIndex;
	}
	public void setLegalIndex(String legalIndex) {
		if (typeApplicant.equals(typeApplicantVal2)) {		
			if (legalIndex == null) {
				this.legalIndex = "";
			} else {
				this.legalIndex = legalIndex;
			}
		}
		
	}
	public String getLegalCountry() {
		return legalCountry;
	}
	public void setLegalCountry(String legalCountry) {
		if (typeApplicant.equals(typeApplicantVal2)) {
			if (legalCountry == null) {
				this.legalCountry = "";
			} else {
				this.legalCountry = legalCountry;
			}
		}
	}
	public String getLegalCity() {
		return legalCity;
	}
	public void setLegalCity(String legalCity) {
		if (typeApplicant.equals(typeApplicantVal2)) {
			if (legalCity == null) {
				this.legalCity = "";
			} else {
				this.legalCity = legalCity;
			}
		}
	}
	public String getLegalStreet() {
		return legalStreet;
	}
	public void setLegalStreet(String legalStreet) {
		if (typeApplicant.equals(typeApplicantVal2)) {
			if (legalStreet == null) {
				this.legalStreet = "";
			} else {
				this.legalStreet = legalStreet;
			}
		}
	}
	public String getLegalHome() {
		return legalHome;
	}
	public void setLegalHome(String legalHome) {
		if (typeApplicant.equals(typeApplicantVal2)) {
			if (legalHome == null) {
				this.legalHome = "";
			} else {
				this.legalHome = legalHome;
			}
		}
	}
	public String getLegalHousing() {
		return legalHousing;
	}
	public void setLegalHousing(String legalHousing) {
		if (typeApplicant.equals(typeApplicantVal2)) {
			if (legalHousing == null) {
				this.legalHousing = "";
			} else {
				this.legalHousing = legalHousing;
			}
		}
	}
	public String getLegalApartment() {
		return legalApartment;
	}
	public void setLegalApartment(String legalApartment) {
		if (typeApplicant.equals(typeApplicantVal2)) {
			if (legalApartment == null) {
				this.legalApartment = "";
			} else {
				this.legalApartment = legalApartment;
			}
		}
	}
	public String getLegalSection() {
		return legalSection;
	}
	public void setLegalSection(String legalSection) {
		if (typeApplicant.equals(typeApplicantVal2)) {
			if (legalSection == null) {
				this.legalSection = "";
			} else {
				this.legalSection = legalSection;
			}
		}
	}
	public String getLegalRoom() {
		return legalRoom;
	}
	public void setLegalRoom(String legalRoom) {
		if (typeApplicant.equals(typeApplicantVal2)) {
			if (legalRoom == null) {
				this.legalRoom = "";
			} else {
				this.legalRoom = legalRoom;
			}
		}
	}
	public String getLegalNumberTelephone() {
		return legalNumberTelephone;
	}
	public void setLegalNumberTelephone(String legalNumberTelephone) {
		if (typeApplicant.equals(typeApplicantVal2)) {
			if (legalNumberTelephone == null) {
				this.legalNumberTelephone = "";
			} else {
				this.legalNumberTelephone = legalNumberTelephone;
			}
		}
	}
	public String getLegalEmail() {
		return legalEmail;
	}
	public void setLegalEmail(String legalEmail) {
		if (typeApplicant.equals(typeApplicantVal2)) {
			if (legalEmail == null) {
				this.legalEmail = "";
			} else {
				this.legalEmail = legalEmail;
			}
		}
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		if (reqDocGroup.equals(reqDocGroupVal6) || 
			reqDocGroup.equals(reqDocGroupVal7)) {
			if (contractNumber == null) {
				this.contractNumber = "";
			} else {
				this.contractNumber = contractNumber;
			}
		}
	}
	public String getDateContract() {
		return dateContract;
	}
	public void setDateContract(String dateContract) {
		if (reqDocGroup.equals(reqDocGroupVal6) || 
			reqDocGroup.equals(reqDocGroupVal7)) {
			this.dateContract = dateContract;
		}
	}
	public String getDateContractVal1() {
		return dateContractVal1;
	}
	public void setDateContractVal1(String dateContractVal1) {
		//if (reqDocGroup.equals(reqDocGroupVal6) || 
			//reqDocGroup.equals(reqDocGroupVal7)) {
			this.dateContractVal1 = dateContractVal1;
		//}
	}
	public String getDateContractVal2() {
		return dateContractVal2;
	}
	public void setDateContractVal2(String dateContractVal2) {
		//if (reqDocGroup.equals(reqDocGroupVal6) || 
			//reqDocGroup.equals(reqDocGroupVal7)) {
			this.dateContractVal2 = dateContractVal2;
		//}
	}
	public String getDayExactDateContract() {
		return dayExactDateContract;
	}
	public void setDayExactDateContract(String dayExactDateContract) {
		if (reqDocGroup.equals(reqDocGroupVal6) || 
			reqDocGroup.equals(reqDocGroupVal7)) {
			if (dateContract.equals(dateContractVal1)) {
				if (dayExactDateContract == null) {
					this.dayExactDateContract = "";
				} else {
					this.dayExactDateContract = dayExactDateContract;
				}
			}
		}
	}
	public String getMonthExactDateContract() {
		return monthExactDateContract;
	}
	public void setMonthExactDateContract(String monthExactDateContract) {
		if (reqDocGroup.equals(reqDocGroupVal6) || 
			reqDocGroup.equals(reqDocGroupVal7)) {
			if (dateContract.equals(dateContractVal1)) {
				if (monthExactDateContract == null) {
					this.monthExactDateContract = "";
				} else {
					this.monthExactDateContract = monthExactDateContract;
				}
			}
		}
	}
	public String getYearExactDateContract() {
		return yearExactDateContract;
	}
	public void setYearExactDateContract(String yearExactDateContract) {
		if (reqDocGroup.equals(reqDocGroupVal6) || 
			reqDocGroup.equals(reqDocGroupVal7)) {
			if (dateContract.equals(dateContractVal1)) {
				if (yearExactDateContract == null) {
					this.yearExactDateContract = "";
				} else {
					this.yearExactDateContract = yearExactDateContract;
				}
			}	
		}
	}
	public String getDayDateRangeContract1() {
		return dayDateRangeContract1;
	}
	public void setDayDateRangeContract1(String dayDateRangeContract1) {
		if (reqDocGroup.equals(reqDocGroupVal6) || 
			reqDocGroup.equals(reqDocGroupVal7)) {
			if (dateContract.equals(dateContractVal2)) {
				if (dayDateRangeContract1 == null) {
					this.dayDateRangeContract1 = "";
				} else {
					this.dayDateRangeContract1 = dayDateRangeContract1;
				}
			}
		}
	}
	public String getMonthDateRangeContract1() {
		return monthDateRangeContract1;
	}
	public void setMonthDateRangeContract1(String monthDateRangeContract1) {
		if (reqDocGroup.equals(reqDocGroupVal6) || 
			reqDocGroup.equals(reqDocGroupVal7)) {
			if (dateContract.equals(dateContractVal2)) {
				if (monthDateRangeContract1 == null) {
					this.monthDateRangeContract1 = "";
				} else {
					this.monthDateRangeContract1 = monthDateRangeContract1;
				}
			}
		}
	}
	public String getYearDateRangeContract1() {
		return yearDateRangeContract1;
	}
	public void setYearDateRangeContract1(String yearDateRangeContract1) {
		if (reqDocGroup.equals(reqDocGroupVal6) || 
			reqDocGroup.equals(reqDocGroupVal7)) {
			if (dateContract.equals(dateContractVal2)) {
				if (yearDateRangeContract1 == null) {
					this.yearDateRangeContract1 = "";
				} else {
					this.yearDateRangeContract1 = yearDateRangeContract1;
				}
			}
		}
	}
	public String getDayDateRangeContract2() {
		return dayDateRangeContract2;
	}
	public void setDayDateRangeContract2(String dayDateRangeContract2) {
		if (reqDocGroup.equals(reqDocGroupVal6) || 
			reqDocGroup.equals(reqDocGroupVal7)) {
			if (dateContract.equals(dateContractVal2)) {
				if (dayDateRangeContract2 == null) {
					this.dayDateRangeContract2 = "";
				} else {
					this.dayDateRangeContract2 = dayDateRangeContract2;
				}
			}
		}
		
	}
	public String getMonthDateRangeContract2() {
		return monthDateRangeContract2;
	}
	public void setMonthDateRangeContract2(String monthDateRangeContract2) {
		if (reqDocGroup.equals(reqDocGroupVal6) || 
			reqDocGroup.equals(reqDocGroupVal7)) {
			if (dateContract.equals(dateContractVal2)) {
				if (monthDateRangeContract2 == null) {
					this.monthDateRangeContract2 = "";
				} else {
					this.monthDateRangeContract2 = monthDateRangeContract2;
				}
			}
		}
	}
	public String getYearDateRangeContract2() {
		return yearDateRangeContract2;
	}
	public void setYearDateRangeContract2(String yearDateRangeContract2) {
		if (reqDocGroup.equals(reqDocGroupVal6) || 
			reqDocGroup.equals(reqDocGroupVal7)) {
			if (dateContract.equals(dateContractVal2)) {
				if (yearDateRangeContract2 == null) {
					this.yearDateRangeContract2 = "";
				} else {
					this.yearDateRangeContract2 = yearDateRangeContract2;
				}
			}
		}		
	}
	//годы участия граждан в подпрограммах, предусмотренных федеральной целевой программой «Жилище»
	public String getYearHousingProgram() {
		return yearHousingProgram;
	}
	public void setYearHousingProgram(String yearHousingProgram) {
		if (reqDocGroup.equals(reqDocGroupVal2)) {
			this.yearHousingProgram = yearHousingProgram;
		}
	}
	public String getYearHousingProgramVal1() {
		return yearHousingProgramVal1;
	}
	public void setYearHousingProgramVal1(String yearHousingProgramVal1) {
			this.yearHousingProgramVal1 = yearHousingProgramVal1;
	}
	public String getYearHousingProgramVal2() {
		return yearHousingProgramVal2;
	}
	public void setYearHousingProgramVal2(String yearHousingProgramVal2) {
			this.yearHousingProgramVal2 = yearHousingProgramVal2;
	}
	public String getYearHousingProgramVal3() {
		return yearHousingProgramVal3;
	}
	public void setYearHousingProgramVal3(String yearHousingProgramVal3) {
			this.yearHousingProgramVal3 = yearHousingProgramVal3;
	}
	public String getKindObject() {
		return kindObject;
	}
	public void setKindObject(String kindObject) {
		if (reqDocGroup.equals(reqDocGroupVal10) ||
			reqDocGroup.equals(reqDocGroupVal11) ||
			reqDocGroup.equals(reqDocGroupVal12)) {
			if (kindObject == null) {
				this.kindObject = "";
			} else {
				this.kindObject = kindObject;
			}
		}
	}
	public String getObjectCity() {
		return objectCity;
	}
	public void setObjectCity(String objectCity) {
		if (reqDocGroup.equals(reqDocGroupVal4) ||
			reqDocGroup.equals(reqDocGroupVal5) ||
			reqDocGroup.equals(reqDocGroupVal6) ||
			reqDocGroup.equals(reqDocGroupVal7) ||
			reqDocGroup.equals(reqDocGroupVal10) ||
			reqDocGroup.equals(reqDocGroupVal11) ||
			reqDocGroup.equals(reqDocGroupVal12)) {
			if (objectCity == null) {
				this.objectCity = "";
			} else {
				this.objectCity = objectCity;
			}
		}
	}
	public String getObjectStreet() {
		return objectStreet;
	}
	public void setObjectStreet(String objectStreet) {
		if (reqDocGroup.equals(reqDocGroupVal4) ||
			reqDocGroup.equals(reqDocGroupVal5) ||
			reqDocGroup.equals(reqDocGroupVal6) ||
			reqDocGroup.equals(reqDocGroupVal7) ||
			reqDocGroup.equals(reqDocGroupVal10) ||
			reqDocGroup.equals(reqDocGroupVal11) ||
			reqDocGroup.equals(reqDocGroupVal12)) {
			if (objectStreet == null) {
				this.objectStreet = "";
			} else {
				this.objectStreet = objectStreet;
			}
		}
	}
	public String getObjectHome() {
		return objectHome;
	}
	public void setObjectHome(String objectHome) {
		if (reqDocGroup.equals(reqDocGroupVal4) ||
			reqDocGroup.equals(reqDocGroupVal5) ||
			reqDocGroup.equals(reqDocGroupVal6) ||
			reqDocGroup.equals(reqDocGroupVal7) ||
			reqDocGroup.equals(reqDocGroupVal10) ||
			reqDocGroup.equals(reqDocGroupVal11) ||
			reqDocGroup.equals(reqDocGroupVal12)) {
			if (objectHome == null) {
				this.objectHome = "";
			} else {
				this.objectHome = objectHome;
			}
		}
	}
	public String getObjectHousing() {
		return objectHousing;
	}
	public void setObjectHousing(String objectHousing) {
		if (reqDocGroup.equals(reqDocGroupVal4) ||
			reqDocGroup.equals(reqDocGroupVal5) ||
			reqDocGroup.equals(reqDocGroupVal6) ||
			reqDocGroup.equals(reqDocGroupVal7) ||
			reqDocGroup.equals(reqDocGroupVal10) ||
			reqDocGroup.equals(reqDocGroupVal11) ||
			reqDocGroup.equals(reqDocGroupVal12)) {
			if (objectHousing == null) {
				this.objectHousing = "";
			} else {
				this.objectHousing = objectHousing;
			}
		}
		
	}
	public String getObjectApartment() {
		return objectApartment;
	}
	public void setObjectApartment(String objectApartment) {
		if (reqDocGroup.equals(reqDocGroupVal4) ||
			reqDocGroup.equals(reqDocGroupVal5) ||
			reqDocGroup.equals(reqDocGroupVal6) ||
			reqDocGroup.equals(reqDocGroupVal7) ||
			reqDocGroup.equals(reqDocGroupVal10) ||
			reqDocGroup.equals(reqDocGroupVal11) ||
			reqDocGroup.equals(reqDocGroupVal12)) {
			if (objectApartment == null) {
				this.objectApartment = "";
			} else {
				this.objectApartment = objectApartment;
			}
		}
	}
	public String getObjectSection() {
		return objectSection;
	}
	public void setObjectSection(String objectSection) {
		if (reqDocGroup.equals(reqDocGroupVal4) ||
			reqDocGroup.equals(reqDocGroupVal5) ||
			reqDocGroup.equals(reqDocGroupVal6) ||
			reqDocGroup.equals(reqDocGroupVal7) ||
			reqDocGroup.equals(reqDocGroupVal10) ||
			reqDocGroup.equals(reqDocGroupVal11) ||
			reqDocGroup.equals(reqDocGroupVal12)) {
			if (objectSection == null) {
				this.objectSection = "";
			} else {
				this.objectSection = objectSection;
			}
		}
		
	}
	public String getObjectRoom() {
		return objectRoom;
	}
	public void setObjectRoom(String objectRoom) {
		if (reqDocGroup.equals(reqDocGroupVal4) ||
			reqDocGroup.equals(reqDocGroupVal5) ||
			reqDocGroup.equals(reqDocGroupVal6) ||
			reqDocGroup.equals(reqDocGroupVal7) ||
			reqDocGroup.equals(reqDocGroupVal10) ||
			reqDocGroup.equals(reqDocGroupVal11) ||
			reqDocGroup.equals(reqDocGroupVal12)) {
			if (objectRoom == null) {
				this.objectRoom = "";
			} else {
				this.objectRoom = objectRoom;
			}
		}
	}
	public String getObjectMoreInfo1() {
		return objectMoreInfo1;
	}
	public void setObjectMoreInfo1(String objectMoreInfo1) {
		if (reqDocGroup.equals(reqDocGroupVal10) ||
			reqDocGroup.equals(reqDocGroupVal11)) {
			if (objectMoreInfo1 == null) {
				this.objectMoreInfo1 = "";
			} else {
				this.objectMoreInfo1 = objectMoreInfo1;
			}
		}
		
	}
	public String getObjectMoreInfo2() {
		return objectMoreInfo2;
	}
	public void setObjectMoreInfo2(String objectMoreInfo2) {
		if (reqDocGroup.equals(reqDocGroupVal12)) {
			this.objectMoreInfo2 = objectMoreInfo2;
		}
	}
	public String getObjectMoreInfo2Val1() {
		return objectMoreInfo2Val1;
	}
	public void setObjectMoreInfo2Val1(String objectMoreInfo2Val1) {
		//if (reqDocGroup.equals(reqDocGroupVal12)) {
			this.objectMoreInfo2Val1 = objectMoreInfo2Val1;
		//}
	}
	public String getObjectMoreInfo2Val2() {
		return objectMoreInfo2Val2;
	}
	public void setObjectMoreInfo2Val2(String objectMoreInfo2Val2) {
		//if (reqDocGroup.equals(reqDocGroupVal12)) {
			this.objectMoreInfo2Val2 = objectMoreInfo2Val2;
		//}
	}
	public String getObjectUrlGeo() {
		return objectUrlGeo;
	}
	public void setObjectUrlGeo(String objectUrlGeo) {
		if (reqDocGroup.equals(reqDocGroupVal12)) {
			if (objectMoreInfo2.equals(objectMoreInfo2Val1)) {
				if (objectUrlGeo == null) {
					this.objectUrlGeo = "";
				} else {
					this.objectUrlGeo = objectUrlGeo;
				}
			}
		}
	}
	public String getObjectOtherInfo() {
		return objectOtherInfo;
	}
	public void setObjectOtherInfo(String objectOtherInfo) {
		if (reqDocGroup.equals(reqDocGroupVal12)) {
			if (objectMoreInfo2.equals(objectMoreInfo2Val1)) {
				if (objectOtherInfo == null) {
					this.objectOtherInfo = "";
				} else {
					this.objectOtherInfo = objectOtherInfo;
				}
			}
		}
	}
	/*public String getObjectInventoryNumber() {
		return objectInventoryNumber;
	}
	public void setObjectInventoryNumber(String objectInventoryNumber) {
		if (reqDocGroup.equals(reqDocGroupVal10)) {
			if (objectMoreInfo2.equals(objectMoreInfo2Val2)) {
				if (objectInventoryNumber == null) {
					this.objectInventoryNumber = "";
				} else {
					this.objectInventoryNumber = objectInventoryNumber;
				}
			}
		}
	}*/
	public String getObjectInventoryNumberVal() {
		return objectInventoryNumberVal;
	}
	public void setObjectInventoryNumberVal(String objectInventoryNumberVal) {
		if (reqDocGroup.equals(reqDocGroupVal12)) {
			if (objectMoreInfo2.equals(objectMoreInfo2Val2)) {
				if (objectInventoryNumberVal == null) {
					this.objectInventoryNumberVal = "";
				} else {
					this.objectInventoryNumberVal = objectInventoryNumberVal;
				}
			}
		}
	}
	public File getObjectAttach() {
		return objectAttach;
	}
	public void setObjectAttach(File objectAttach) {
		if (reqDocGroup.equals(reqDocGroupVal12)) {
			if (objectAttach == null) {
				this.objectAttach = null;
			} else {
				
				this.objectAttach = objectAttach;
			}
		}
	}
	public String getObjectDoc() {
		return objectDoc;
	}
	public void setObjectDoc(String objectDoc) {
		if (reqDocGroup.equals(reqDocGroupVal12)) {
			if (objectDoc == null) {
				this.objectDoc = "";
			} else {
				this.objectDoc = objectDoc;
			}
		}
	}
	public String getNumberDoc() {
		return numberDoc;
	}
	public void setNumberDoc(String numberDoc) {
		if (reqDocGroup.equals(reqDocGroupVal12)) {
			if (numberDoc == null) {
				this.numberDoc = "";
			} else {
				this.numberDoc = numberDoc;
			}
		}
	}
	public String getPublicDateDoc() {
		return publicDateDoc;
	}
	public void setPublicDateDoc(String publicDateDoc) {
		if (reqDocGroup.equals(reqDocGroupVal12)) {
			if (publicDateDoc == null) {
				this.publicDateDoc = "";
			} else {
				this.publicDateDoc = publicDateDoc;
			}
		}
	}
	public String getPublicDateDocVal1() {
		return publicDateDocVal1;
	}
	public void setPublicDateDocVal1(String publicDateDocVal1) {
		//if (reqDocGroup.equals(reqDocGroupVal12)) {
			this.publicDateDocVal1 = publicDateDocVal1;
		//}
	}
	public String getPublicDateDocVal2() {
		return publicDateDocVal2;
	}
	public void setPublicDateDocVal2(String publicDateDocVal2) {
		//if (reqDocGroup.equals(reqDocGroupVal12)) {
			this.publicDateDocVal2 = publicDateDocVal2;
		//}
	}
	public String getPublicDayExactDateDoc() {
		return publicDayExactDateDoc;
	}
	public void setPublicDayExactDateDoc(String publicDayExactDateDoc) {
		if (reqDocGroup.equals(reqDocGroupVal12)) {
			if (publicDateDoc.equals(publicDateDocVal1)) {
				if (publicDayExactDateDoc == null) {
					this.publicDayExactDateDoc = "";
				} else {
					this.publicDayExactDateDoc = publicDayExactDateDoc;
				}
			}
		}
	}
	public String getPublicMonthExactDateDoc() {
		return publicMonthExactDateDoc;
	}
	public void setPublicMonthExactDateDoc(String publicMonthExactDateDoc) {
		if (reqDocGroup.equals(reqDocGroupVal12)) {
			if (publicDateDoc.equals(publicDateDocVal1)) {
				if (publicMonthExactDateDoc == null) {
					this.publicMonthExactDateDoc = "";
				} else {
					this.publicMonthExactDateDoc = publicMonthExactDateDoc;
				}
			}
		}
	}
	public String getPublicYearExactDateDoc() {
		return publicYearExactDateDoc;
	}
	public void setPublicYearExactDateDoc(String publicYearExactDateDoc) {
		if (reqDocGroup.equals(reqDocGroupVal12)) {
			if (publicDateDoc.equals(publicDateDocVal1)) {
				if (publicYearExactDateDoc == null) {
					this.publicYearExactDateDoc = "";
				} else {
					this.publicYearExactDateDoc = publicYearExactDateDoc;
				}
			}
		}
	}
	public String getDayPublicDateRangeDoc1() {
		return dayPublicDateRangeDoc1;
	}
	public void setDayPublicDateRangeDoc1(String dayPublicDateRangeDoc1) {
		if (reqDocGroup.equals(reqDocGroupVal12)) {
			if (publicDateDoc.equals(publicDateDocVal2)) {
				if (dayPublicDateRangeDoc1 == null) {
					this.dayPublicDateRangeDoc1 = "";
				} else {
					this.dayPublicDateRangeDoc1 = dayPublicDateRangeDoc1;
				}
			}
		}
	}
	public String getMonthPublicDateRangeDoc1() {
		return monthPublicDateRangeDoc1;
	}
	public void setMonthPublicDateRangeDoc1(String monthPublicDateRangeDoc1) {
		if (reqDocGroup.equals(reqDocGroupVal12)) {
			if (publicDateDoc.equals(publicDateDocVal2)) {
				if (monthPublicDateRangeDoc1 == null) {
					this.monthPublicDateRangeDoc1 = "";
				} else {
					this.monthPublicDateRangeDoc1 = monthPublicDateRangeDoc1;
				}
			}
		}
	}
	public String getYearPublicDateRangeDoc1() {
		return yearPublicDateRangeDoc1;
	}
	public void setYearPublicDateRangeDoc1(String yearPublicDateRangeDoc1) {
		if (reqDocGroup.equals(reqDocGroupVal12)) {
			if (publicDateDoc.equals(publicDateDocVal2)) {
				if (yearPublicDateRangeDoc1 == null) {
					this.yearPublicDateRangeDoc1 = "";
				} else {
					this.yearPublicDateRangeDoc1 = yearPublicDateRangeDoc1;
				}
			}
		}
	}
	public String getDayPublicDateRangeDoc2() {
		return dayPublicDateRangeDoc2;
	}
	public void setDayPublicDateRangeDoc2(String dayPublicDateRangeDoc2) {
		if (reqDocGroup.equals(reqDocGroupVal12)) {
			if (publicDateDoc.equals(publicDateDocVal2)) {
				if (dayPublicDateRangeDoc2 == null) {
					this.dayPublicDateRangeDoc2 = "";
				} else {
					this.dayPublicDateRangeDoc2 = dayPublicDateRangeDoc2;
				}
			}
		}
	}
	public String getMonthPublicDateRangeDoc2() {
		return monthPublicDateRangeDoc2;
	}
	public void setMonthPublicDateRangeDoc2(String monthPublicDateRangeDoc2) {
		if (reqDocGroup.equals(reqDocGroupVal12)) {
			if (publicDateDoc.equals(publicDateDocVal2)) {
				if (monthPublicDateRangeDoc2 == null) {
					this.monthPublicDateRangeDoc2 = "";
				} else {
					this.monthPublicDateRangeDoc2 = monthPublicDateRangeDoc2;
				}
			}
		}
	}
	public String getYearPublicDateRangeDoc2() {
		return yearPublicDateRangeDoc2;
	}
	public void setYearPublicDateRangeDoc2(String yearPublicDateRangeDoc2) {
		if (reqDocGroup.equals(reqDocGroupVal12)) {
			if (publicDateDoc.equals(publicDateDocVal2)) {
				if (yearPublicDateRangeDoc2 == null) {
					this.yearPublicDateRangeDoc2 = "";
				} else {
					this.yearPublicDateRangeDoc2 = yearPublicDateRangeDoc2;
				}
			}
		}
	}
	public String getAuthorDoc() {
		return authorDoc;
	}
	public void setAuthorDoc(String authorDoc) {
		if (reqDocGroup.equals(reqDocGroupVal12)) {
			if (authorDoc == null) {
				this.authorDoc = "";
			} else {
				this.authorDoc = authorDoc;
			}
		}
	}
	public String getValidReqDocGroup() {
		return validReqDocGroup;
	}
	
	public void setValidReqDocGroup(String validReqDocGroup) {
		this.validReqDocGroup = validReqDocGroup;
	}
	public String getValidPurposeRequest() {
		return validPurposeRequest;
	}
	public void setValidPurposeRequest(String validPurposeRequest) {
		this.validPurposeRequest = validPurposeRequest;
	}
	public String getValidTypeApplicant() {
		return validTypeApplicant;
	}
	public void setValidTypeApplicant(String validTypeApplicant) {
		this.validTypeApplicant = validTypeApplicant;
	}
	public String getValidIndividualLastname() {
		return validIndividualLastname;
	}
	public void setValidIndividualLastname(String validIndividualLastname) {
		this.validIndividualLastname = validIndividualLastname;
	}
	public String getValidIndividualFirstname() {
		return validIndividualFirstname;
	}
	public void setValidIndividualFirstname(String validIndividualFirstname) {
		this.validIndividualFirstname = validIndividualFirstname;
	}
	public String getValidIndividualDayBirthday() {
		return validIndividualDayBirthday;
	}
	public void setValidIndividualDayBirthday(
			String validIndividualDayBirthday) {
		this.validIndividualDayBirthday = validIndividualDayBirthday;
	}
	public String getValidIndividualMonthBirthday() {
		return validIndividualMonthBirthday;
	}
	public void setValidIndividualMonthBirthday(
			String validIndividualMonthBirthday) {
		this.validIndividualMonthBirthday = validIndividualMonthBirthday;
	}
	public String getValidIndividualYearBirthday() {
		return validIndividualYearBirthday;
	}
	public void setValidIndividualYearBirthday(
			String validIndividualYearBirthday) {
		this.validIndividualYearBirthday = validIndividualYearBirthday;
	}
	public String getValidIndividualSeriesPassport() {
		return validIndividualSeriesPassport;
	}
	public void setValidIndividualSeriesPassport(
			String validIndividualSeriesPassport) {
		this.validIndividualSeriesPassport = validIndividualSeriesPassport;
	}
	public String getValidIndividualNumberPassport() {
		return validIndividualNumberPassport;
	}
	public void setValidIndividualNumberPassport(
			String validIndividualNumberPassport) {
		this.validIndividualNumberPassport = validIndividualNumberPassport;
	}
	public String getValidIndividualWhoGivePassport() {
		return validIndividualWhoGivePassport;
	}
	public void setValidIndividualWhoGivePassport(
			String validIndividualWhoGivePassport) {
		this.validIndividualWhoGivePassport = validIndividualWhoGivePassport;
	}
	public String getValidIndividualDayIssuePassport() {
		return validIndividualDayIssuePassport;
	}
	public void setValidIndividualDayIssuePassport(
			String validIndividualDayIssuePassport) {
		this.validIndividualDayIssuePassport = validIndividualDayIssuePassport;
	}
	public String getValidIndividualMonthIssuePassport() {
		return validIndividualMonthIssuePassport;
	}
	public void setValidIndividualMonthIssuePassport(
			String validIndividualMonthIssuePassport) {
		this.validIndividualMonthIssuePassport = validIndividualMonthIssuePassport;
	}
	public String getValidIndividualYearIssuePassport() {
		return validIndividualYearIssuePassport;
	}
	public void setValidIndividualYearIssuePassport(
			String validIndividualYearIssuePassport) {
		this.validIndividualYearIssuePassport = validIndividualYearIssuePassport;
	}
	public String getValidIndividualAttachPassport() {
		return validIndividualAttachPassport;
	}
	public void setValidIndividualAttachPassport(
			String validIndividualAttachPassport) {
		this.validIndividualAttachPassport = validIndividualAttachPassport;
	}
	public String getValidIndividualProcuratoryAttachPassport() {
		return validIndividualProcuratoryAttachPassport;
	}
	public void setValidIndividualProcuratoryAttachPassport(String validIndividualProcuratoryAttachPassport) {
		this.validIndividualProcuratoryAttachPassport = validIndividualProcuratoryAttachPassport;
	}
	public String getValidIndividualCity() {
		return validIndividualCity;
	}
	public void setValidIndividualCity(String validIndividualCity) {
		this.validIndividualCity = validIndividualCity;
	}
	public String getValidIndividualStreet() {
		return validIndividualStreet;
	}
	public void setValidIndividualStreet(String validIndividualStreet) {
		this.validIndividualStreet = validIndividualStreet;
	}
	public String getValidIndividualHome() {
		return validIndividualHome;
	}
	public void setValidIndividualHome(String validIndividualHome) {
		this.validIndividualHome = validIndividualHome;
	}
	public String getValidIndividualNumberTelephone() {
		return validIndividualNumberTelephone;
	}
	public void setValidIndividualNumberTelephone(
			String validIndividualNumberTelephone) {
		this.validIndividualNumberTelephone = validIndividualNumberTelephone;
	}
	public String getValidIndividualEmail() {
		return validIndividualEmail;
	}
	public void setValidIndividualEmail(String validIndividualEmail) {
		this.validIndividualEmail = validIndividualEmail;
	}
	public String getValidLegalName() {
		return validLegalName;
	}
	public void setValidLegalName(String validLegalName) {
		this.validLegalName = validLegalName;
	}
	public String getValidLegalOgrn() {
		return validLegalOgrn;
	}
	public void setValidLegalOgrn(String validLegalOgrn) {
		this.validLegalOgrn = validLegalOgrn;
	}
	public String getValidLegalInn() {
		return validLegalInn;
	}
	public void setValidLegalInn(String validLegalInn) {
		this.validLegalInn = validLegalInn;
	}
	public String getValidLegalHeadLastname() {
		return validLegalHeadLastname;
	}
	public void setValidLegalHeadLastname(String validLegalHeadLastname) {
		this.validLegalHeadLastname = validLegalHeadLastname;
	}
	public String getValidLegalHeadFirstname() {
		return validLegalHeadFirstname;
	}
	public void setValidLegalHeadFirstname(String validLegalHeadFirstname) {
		this.validLegalHeadFirstname = validLegalHeadFirstname;
	}
	
	public String getValidLegalProcuratoryAttach() {
		return validLegalProcuratoryAttach;
	}
	public void setValidLegalProcuratoryAttach(String validLegalProcuratoryAttach) {
		this.validLegalProcuratoryAttach = validLegalProcuratoryAttach;
	}
	
	public String getValidLegalAsHead() {
		return validLegalAsHead;
	}
	public void setValidLegalAsHead(String validLegalAsHead) {
		this.validLegalAsHead = validLegalAsHead;
	}
	public String getValidLegalIndex() {
		return validLegalIndex;
	}
	public void setValidLegalIndex(String validLegalIndex) {
		this.validLegalIndex = validLegalIndex;
	}
	public String getValidLegalCountry() {
		return validLegalCountry;
	}
	public void setValidLegalCountry(String validLegalCountry) {
		this.validLegalCountry = validLegalCountry;
	}
	public String getValidLegalCity() {
		return validLegalCity;
	}
	public void setValidLegalCity(String validLegalCity) {
		this.validLegalCity = validLegalCity;
	}
	public String getValidLegalStreet() {
		return validLegalStreet;
	}
	public void setValidLegalStreet(String validLegalStreet) {
		this.validLegalStreet = validLegalStreet;
	}
	public String getValidLegalHome() {
		return validLegalHome;
	}
	public void setValidLegalHome(String validLegalHome) {
		this.validLegalHome = validLegalHome;
	}
	public String getValidLegalNumberTelephone() {
		return validLegalNumberTelephone;
	}
	public void setValidLegalNumberTelephone(String validLegalNumberTelephone) {
		this.validLegalNumberTelephone = validLegalNumberTelephone;
	}
	public String getValidLegalEmail() {
		return validLegalEmail;
	}
	public void setValidLegalEmail(String validLegalEmail) {
		this.validLegalEmail = validLegalEmail;
	}
	public String getValidYearHousingProgram() {
		return validYearHousingProgram;
	}
	public void setValidYearHousingProgram(String validYearHousingProgram) {
		this.validYearHousingProgram = validYearHousingProgram;
	}
	public String getValidKindObject() {
		return validKindObject;
	}
	public void setValidKindObject(String validKindObject) {
		this.validKindObject = validKindObject;
	}
	public String getValidObjectCity() {
		return validObjectCity;
	}
	public void setValidObjectCity(String validObjectCity) {
		this.validObjectCity = validObjectCity;
	}
	public String getValidObjectStreet() {
		return validObjectStreet;
	}
	public void setValidObjectStreet(String validObjectStreet) {
		this.validObjectStreet = validObjectStreet;
	}
	public String getValidObjectHome() {
		return validObjectHome;
	}
	public void setValidObjectHome(String validObjectHome) {
		this.validObjectHome = validObjectHome;
	}
	public String getValidObjectUrlGeo() {
		return validObjectUrlGeo;
	}
	public void setValidObjectUrlGeo(String validObjectUrlGeo) {
		this.validObjectUrlGeo = validObjectUrlGeo;
	}
	public String getValidObjectAttach() {
		return validObjectAttach;
	}
	public void setValidObjectAttach(String validObjectAttach) {
		this.validObjectAttach = validObjectAttach;
	}
	
	/*
	public File getRequestDir() {
		return requestDir;
	}
	public void setRequestDir(File requestDir) {
		this.requestDir = requestDir;
	}*/
	public EditBean getEditBean() {
		return editBean;
	}
	public void setEditBean(EditBean editBean) {
		this.editBean = editBean;
	}
	/*
	public String getViewURL() {
		return viewURL;
	}
	public void setViewURL(String viewURL) {
		this.viewURL = viewURL;
	}
	public String getRealAttachName() {
		return realAttachName;
	}
	public void setRealAttachName(String realAttachName) {
		this.realAttachName = realAttachName;
	}
	*/
	/*public String getRegexFormatAttach() {
		return regexFormatAttach;
	}
	public void setRegexFormatAttach(String regexFormatAttach) {
		this.regexFormatAttach = regexFormatAttach;
	}*/
	public boolean isValidAll() {
		//вернуть в исходное состояние - истину 
		validAll = true;
		
		if (reqDocGroup == null) {
			validReqDocGroup = idle;
			validAll = false;
		}
		else if (reqDocGroup.equals("")) {
			validReqDocGroup = idle;
			validAll = false;
		}
		else {
			validReqDocGroup = ok;
		}
		
		if (purposeRequest == null) {
			validPurposeRequest = idle;
			validAll = false;
		}
		else if (purposeRequest.equals("")) {
			validPurposeRequest = idle;
			validAll = false;
		}
		else {
			validPurposeRequest = ok;
		}
		
		if (typeApplicant == null) {
			validTypeApplicant = idle;
			validAll = false;
		}
		else if (typeApplicant.equals("")) {
			validTypeApplicant = idle;
			validAll = false;
		}
		else {
			validTypeApplicant = ok;
		}
		
		if (typeApplicant.equals(typeApplicantVal1)) {
			if (individualLastname == null) {
				validIndividualLastname = idle;
				validAll = false;
			}
			else if (individualLastname.equals("")) {
				validIndividualLastname = idle;
				validAll = false;
			}
			else {
				validIndividualLastname = ok;
			}
		
			if (individualFirstname == null) {
				validIndividualFirstname = idle;
				validAll = false;
			}
			else if (individualFirstname.equals("")) {
				validIndividualFirstname = idle;
				validAll = false;
			}
			else {
				validIndividualFirstname = ok;
			}
			
			if (reqDocGroup.equals(reqDocGroupVal1) ||
				reqDocGroup.equals(reqDocGroupVal2) ||
				reqDocGroup.equals(reqDocGroupVal3) ||
				reqDocGroup.equals(reqDocGroupVal4) ||
				reqDocGroup.equals(reqDocGroupVal5) ||
				reqDocGroup.equals(reqDocGroupVal6) ||
				reqDocGroup.equals(reqDocGroupVal7)) {
				
				if (individualDayBirthday == null) {
					validIndividualDayBirthday = idle;
					validAll = false;
				}
				else if (individualDayBirthday.equals("")) {
					validIndividualDayBirthday = idle;
					validAll = false;
				}
				else {
					validIndividualDayBirthday = ok;
				}
				
				if (individualMonthBirthday == null) {
					validIndividualMonthBirthday = idle;
					validAll = false;
				}
				else if (individualMonthBirthday.equals("")) {
					validIndividualMonthBirthday = idle;
					validAll = false;
				}
				else {
					validIndividualMonthBirthday = ok;
				}
				
				if (individualYearBirthday == null) {
					validIndividualYearBirthday = idle;
					validAll = false;
				}
				else if (individualYearBirthday.equals("")) {
					validIndividualYearBirthday = idle;
					validAll = false;
				}
				else {
					validIndividualYearBirthday = ok;
				}
				
			}
			
			if (individualSeriesPassport == null) {
				validIndividualSeriesPassport = idle;
				validAll = false;
			}
			else if (individualSeriesPassport.equals("")) {
				validIndividualSeriesPassport = idle;
				validAll = false;
			}
			else {
				validIndividualSeriesPassport = ok;
			}
		
			if (individualNumberPassport == null) {
				validIndividualNumberPassport = idle;
				validAll = false;
			}
			else if (individualNumberPassport.equals("")) {
				validIndividualNumberPassport = idle;
				validAll = false;
			}
			else {
				validIndividualNumberPassport = ok;
			}
			
			if (individualWhoGivePassport == null) {
				validIndividualWhoGivePassport = idle;
				validAll = false;
			}
			else if (individualWhoGivePassport.equals("")) {
				validIndividualWhoGivePassport = idle;
				validAll = false;
			}
			else {
				validIndividualWhoGivePassport = ok;
			}
			
			if (individualDayIssuePassport == null) {
				validIndividualDayIssuePassport = idle;
				validAll = false;
			}
			else if (individualDayIssuePassport.equals("")) {
				validIndividualDayIssuePassport = idle;
				validAll = false;
			}
			else {
				validIndividualDayIssuePassport = ok;
			}
			
			if (individualMonthIssuePassport == null) {
				validIndividualMonthIssuePassport = idle;
				validAll = false;
			}
			else if (individualMonthIssuePassport.equals("")) {
				validIndividualMonthIssuePassport = idle;
				validAll = false;
			}
			else {
				validIndividualMonthIssuePassport = ok;
			}
			
			if (individualYearIssuePassport == null) {
				validIndividualYearIssuePassport = idle;
				validAll = false;
			}
			else if (individualYearIssuePassport.equals("")) {
				validIndividualYearIssuePassport = idle;
				validAll = false;
			}
			else {
				validIndividualYearIssuePassport = ok;
			}
			
			if (individualAttachPassport == null) {
				validIndividualAttachPassport = idle;
				validAll = false;
			}
			else {
				//сделаем ok, чтобы убедиться что не старое состояние
				validIndividualAttachPassport = ok;
				try {
					
					//проверка на формат
					if (!editBean.getFormatAttach().equals("")) {
			            if (!MyUtils.findExtInRow(MyUtils.getExtNotDot(individualAttachPassport.getName()), editBean.getFormatAttach(), editBean.getRegexFormatAttach())){
			            	validIndividualAttachPassport = badFormatAttach;
			            	validAll = false;
			            	//Удалить файл
			            	FileUtils.forceDelete(individualAttachPassport);
			            } else validIndividualAttachPassport = ok;
		            } else validIndividualAttachPassport = ok;
					
					//проверка на размер
		            if (Double.valueOf(String.valueOf(individualAttachPassport.length())) > Double.valueOf(editBean.getMaxSizeAttach())*1024.0*1024.0) {
		            	validIndividualAttachPassport = badSizeAttach;
		            	validAll = false;
		            	//Удалить файл
		            	FileUtils.forceDelete(individualAttachPassport);
		            } else {
		            	//если не недопустимый формат, то валидируем положительно
		            	if (!validIndividualAttachPassport.equals(badFormatAttach) ){
		            		validIndividualAttachPassport = ok;
		            	}
		            }
		            
		            ///Если файл прошел валидацию, отправим его во временную папку
		            //if (validIndividualAttachPassport.equals("ok")) {
						//создать временную папку для request.xml
							
		            	//if (requestDir == null || !requestDir.exists()) {
	            			//requestDir = createRequestDir();
	            		//}
						/*в commons io v.1.3.1 отсутсвует moveFileToDirectory, 
						 поэтому копируем в папку requestDir, удаляем objectAttach
						 и objectAttach ссылаем на файл в папке requestDir*/
						//FileUtils.moveFileToDirectory(objectAttach, requestDir, false);
						//FileUtils.copyFileToDirectory(individualAttachPassport, requestDir);
						//FileUtils.forceDelete(individualAttachPassport);
		            	//FileUtils.moveFileToDirectory(individualAttachPassport, requestDir, false);
						//по идее нужный файл в папке будет последним, поэтому индекс в листе делаем последним
						//individualAttachPassport = requestDir.listFiles()[requestDir.list().length - 1];
		            //}
		            
				} catch (Exception e) {
					e.printStackTrace();
				}
		            
			}
			
			if (individualProcuratoryAttachPassport != null) {
				
				//сделаем ok, чтобы убедиться что не старое состояние
				validIndividualProcuratoryAttachPassport = ok;
				try {
					
					//проверка на формат
					if (!editBean.getFormatAttach().equals("")) {
			            if (!MyUtils.findExtInRow(MyUtils.getExtNotDot(individualProcuratoryAttachPassport.getName()), editBean.getFormatAttach(), editBean.getRegexFormatAttach())){
			            	validIndividualProcuratoryAttachPassport = badFormatAttach;
			            	validAll = false;
			            	//Удалить файл
			            	FileUtils.forceDelete(individualProcuratoryAttachPassport);
			            } else validIndividualProcuratoryAttachPassport = ok;
		            } else validIndividualProcuratoryAttachPassport = ok;
					
					//проверка на размер
		            if (Double.valueOf(String.valueOf(individualProcuratoryAttachPassport.length())) > Double.valueOf(editBean.getMaxSizeAttach())*1024.0*1024.0) {
		            	validIndividualProcuratoryAttachPassport = badSizeAttach;
		            	validAll = false;
		            	//Удалить файл
		            	FileUtils.forceDelete(individualProcuratoryAttachPassport);
		            } else {
		            	//если не недопустимый формат, то валидируем положительно
		            	if (!validIndividualProcuratoryAttachPassport.equals(badFormatAttach) ){
		            		validIndividualProcuratoryAttachPassport = ok;
		            	}
		            }
		            
		            ///Если файл прошел валидацию, отправим его во временную папку
		            //if (validIndividualProcuratoryAttachPassport.equals("ok")) {
						//создать временную папку для request.xml
	            		//if (requestDir == null || !requestDir.exists()) {
	            			//requestDir = createRequestDir();
	            		//}
						/*в commons io v.1.3.1 отсутсвует moveFileToDirectory, 
						 поэтому копируем в папку requestDir, удаляем objectAttach
						 и objectAttach ссылаем на файл в папке requestDir*/
						//FileUtils.moveFileToDirectory(objectAttach, requestDir, false);
						/*
	            		FileUtils.copyFileToDirectory(individualProcuratoryAttachPassport, requestDir);
						FileUtils.forceDelete(individualProcuratoryAttachPassport);
						*/
	            		
						//FileUtils.moveFileToDirectory(individualProcuratoryAttachPassport, requestDir, false);
						//по идее нужный файл в папке будет последним, поэтому индекс в листе делаем последним
						//individualProcuratoryAttachPassport = requestDir.listFiles()[requestDir.list().length - 1];
		            //}
		            
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
			
			if (individualCity == null) {
				validIndividualCity = idle;
				validAll = false;
			}
			else if (individualCity.equals("")) {
				validIndividualCity = idle;
				validAll = false;
			}
			else {
				validIndividualCity = ok;
			}
			
			if (individualStreet == null) {
				validIndividualStreet = idle;
				validAll = false;
			}
			else if (individualStreet.equals("")) {
				validIndividualStreet = idle;
				validAll = false;
			}
			else {
				validIndividualStreet = ok;
			}
			
			if (individualHome == null) {
				validIndividualHome = idle;
				validAll = false;
			}
			else if (individualHome.equals("")) {
				validIndividualHome = idle;
				validAll = false;
			}
			else {
				validIndividualHome = ok;
			}
			
			if (individualNumberTelephone == null) {
				validIndividualNumberTelephone = idle;
				validAll = false;
			}
			else if (individualNumberTelephone.equals("")) {
				validIndividualNumberTelephone = idle;
				validAll = false;
			}
			else {
				validIndividualNumberTelephone = ok;
			}
		
			if (individualEmail == null) {
				validIndividualEmail = idle;
				validAll = false;
			}
			else if (individualEmail.equals("")) {
				validIndividualEmail = idle;
				validAll = false;
			}
			else {
				if (MyUtils.doMatch(individualEmail, MyUtils.EMAIL)) {
					validIndividualEmail = ok;
				}
				else {
					validIndividualEmail = badEmail;
					validAll = false;
				}
			}
		
		}
		
		if (typeApplicant.equals(typeApplicantVal2)) {
			if (legalName == null) {
				validLegalName = idle;
				validAll = false;
			}
			else if (legalName.equals("")) {
				validLegalName = idle;
				validAll = false;
			}
			else {
				validLegalName = ok;
			}
			
			if (legalOgrn == null) {
				validLegalOgrn = idle;
				validAll = false;
			}
			else if (legalOgrn.equals("")) {
				validLegalOgrn = idle;
				validAll = false;
			}
			else {
				validLegalOgrn = ok;
			}
			
			if (legalInn == null) {
				validLegalInn = idle;
				validAll = false;
			}
			else if (legalInn.equals("")) {
				validLegalInn = idle;
				validAll = false;
			}
			else {
				validLegalInn = ok;
			}
			
			if (legalHeadLastname == null) {
				validLegalHeadLastname = idle;
				validAll = false;
			}
			else if (legalHeadLastname.equals("")) {
				validLegalHeadLastname = idle;
				validAll = false;
			}
			else {
				validLegalHeadLastname = ok;
			}
			
			if (legalHeadLastname == null) {
				validLegalHeadLastname = idle;
				validAll = false;
			}
			else if (legalHeadLastname.equals("")) {
				validLegalHeadLastname = idle;
				validAll = false;
			}
			else {
				validLegalHeadLastname = ok;
			}
			
			if (legalHeadFirstname == null) {
				validLegalHeadFirstname = idle;
				validAll = false;
			}
			else if (legalHeadFirstname.equals("")) {
				validLegalHeadFirstname = idle;
				validAll = false;
			}
			else {
				validLegalHeadFirstname = ok;
			}
			
			if (legalProcuratoryAttach == null) {
				validLegalProcuratoryAttach = idle;
				validAll = false;
			} else {
				
				//сделаем ok, чтобы убедиться что не старое состояние
				validLegalProcuratoryAttach = ok;
				try {
					
					//проверка на формат
					if (!editBean.getFormatAttach().equals("")) {
			            if (!MyUtils.findExtInRow(MyUtils.getExtNotDot(legalProcuratoryAttach.getName()), editBean.getFormatAttach(), editBean.getRegexFormatAttach())){
			            	validLegalProcuratoryAttach = badFormatAttach;
			            	validAll = false;
			            	//Удалить файл
			            	FileUtils.forceDelete(legalProcuratoryAttach);
			            } else validLegalProcuratoryAttach = ok;
		            } else validLegalProcuratoryAttach = ok;
					
					//проверка на размер
		            if (Double.valueOf(String.valueOf(legalProcuratoryAttach.length())) > Double.valueOf(editBean.getMaxSizeAttach())*1024.0*1024.0) {
		            	validLegalProcuratoryAttach = badSizeAttach;
		            	validAll = false;
		            	//Удалить файл
		            	FileUtils.forceDelete(legalProcuratoryAttach);
		            } else {
		            	//если не недопустимый формат, то валидируем положительно
		            	if (!validLegalProcuratoryAttach.equals(badFormatAttach) ){
		            		validLegalProcuratoryAttach = ok;
		            	}
		            }
		            
		            ///Если файл прошел валидацию, отправим его во временную папку
		            //if (validLegalProcuratoryAttach.equals("ok")) {
						//создать временную папку для request.xml
	            		//if (requestDir == null || !requestDir.exists()) {
	            			//requestDir = createRequestDir();
	            		//}
						/*в commons io v.1.3.1 отсутсвует moveFileToDirectory, 
						 поэтому копируем в папку requestDir, удаляем objectAttach
						 и objectAttach ссылаем на файл в папке requestDir*/
						//FileUtils.moveFileToDirectory(objectAttach, requestDir, false);
						//FileUtils.copyFileToDirectory(legalProcuratoryAttach, requestDir);
						//FileUtils.forceDelete(legalProcuratoryAttach);
	            		//FileUtils.moveFileToDirectory(legalProcuratoryAttach, requestDir, false);
						//по идее нужный файл в папке будет последним, поэтому индекс в листе делаем последним
						//legalProcuratoryAttach = requestDir.listFiles()[requestDir.list().length - 1];
		            //}
		            
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
			
			if (legalAsHead == null) {
				validLegalAsHead = idle;
				validAll = false;
			}
			else if (legalAsHead.equals("")) {
				validLegalAsHead = idle;
				validAll = false;
			}
			else {
				validLegalAsHead = ok;
			}
					
			if (legalIndex == null) {
				validLegalIndex = idle;
				validAll = false;
			}
			else if (legalIndex.equals("")) {
				validLegalIndex = idle;
				validAll = false;
			}
			else {
				validLegalIndex = ok;
			}
			
			if (legalCountry == null) {
				validLegalCountry = idle;
				validAll = false;
			}
			else if (legalCountry.equals("")) {
				validLegalCountry = idle;
				validAll = false;
			}
			else {
				validLegalCountry = ok;
			}
			
			if (legalCity == null) {
				validLegalCity = idle;
				validAll = false;
			}
			else if (legalCity.equals("")) {
				validLegalCity = idle;
				validAll = false;
			}
			else {
				validLegalCity = ok;
			}
			
			if (legalStreet == null) {
				validLegalStreet = idle;
				validAll = false;
			}
			else if (legalStreet.equals("")) {
				validLegalStreet = idle;
				validAll = false;
			}
			else {
				validLegalStreet = ok;
			}
			
			if (legalHome == null) {
				validLegalStreet = idle;
				validAll = false;
			}
			else if (legalHome.equals("")) {
				validLegalHome = idle;
				validAll = false;
			}
			else {
				validLegalHome = ok;
			}
			
			if (legalNumberTelephone == null) {
				validLegalNumberTelephone = idle;
				validAll = false;
			}
			else if (legalNumberTelephone.equals("")) {
				validLegalNumberTelephone = idle;
				validAll = false;
			}
			else {
				validLegalNumberTelephone = ok;
			}
			
			if (legalEmail == null) {
				validLegalEmail = idle;
				validAll = false;
			}
			else if (legalEmail.equals("")) {
				validLegalEmail = idle;
				validAll = false;
			}
			else {
				if (MyUtils.doMatch(legalEmail, MyUtils.EMAIL)) {
					validLegalEmail = ok;
				}
				else {
					validLegalEmail = badEmail;
					validAll = false;
				}
			}
			
		}
		
		if (reqDocGroup.equals(reqDocGroupVal2)) {
				if (yearHousingProgram == null) {
					validYearHousingProgram = idle;
					validAll = false;
				}
				else if (yearHousingProgram.equals("")) {
					validYearHousingProgram = idle;
					validAll = false;
				}
				else {
					validYearHousingProgram = ok;
				}
		}
		
		if (reqDocGroup.equals(reqDocGroupVal10) ||
			reqDocGroup.equals(reqDocGroupVal11) ||
			reqDocGroup.equals(reqDocGroupVal12)) {
			if (kindObject == null) {
				validKindObject = idle;
				validAll = false;
			}
			else if (kindObject.equals("")) {
				validKindObject = idle;
				validAll = false;
			}
			else {
				validKindObject = ok;
			}
		}
		
		
		if (reqDocGroup.equals(reqDocGroupVal4) ||
			reqDocGroup.equals(reqDocGroupVal5) ||
			reqDocGroup.equals(reqDocGroupVal6) ||
			reqDocGroup.equals(reqDocGroupVal7) ||
			reqDocGroup.equals(reqDocGroupVal10) ||
			reqDocGroup.equals(reqDocGroupVal11) ||
			reqDocGroup.equals(reqDocGroupVal12)) {
			if (objectCity == null) {
				validObjectCity = idle;
				validAll = false;
			}
			else if (objectCity.equals("")) {
				validObjectCity = idle;
				validAll = false;
			}
			else {
				validObjectCity = ok;
			}
		}
		
		if (reqDocGroup.equals(reqDocGroupVal4) ||
			reqDocGroup.equals(reqDocGroupVal5) ||
			reqDocGroup.equals(reqDocGroupVal6) ||
			reqDocGroup.equals(reqDocGroupVal7) ||
			reqDocGroup.equals(reqDocGroupVal10) ||
			reqDocGroup.equals(reqDocGroupVal11) ||
			reqDocGroup.equals(reqDocGroupVal12)) {
			if (objectStreet == null) {
				validObjectStreet = idle;
				validAll = false;
			}
			else if (objectStreet.equals("")) {
				validObjectStreet = idle;
				validAll = false;
			}
			else {
				validObjectStreet = ok;
			}
		}
		
		if (reqDocGroup.equals(reqDocGroupVal4) ||
			reqDocGroup.equals(reqDocGroupVal5) ||
			reqDocGroup.equals(reqDocGroupVal6) ||
			reqDocGroup.equals(reqDocGroupVal7) ||
			reqDocGroup.equals(reqDocGroupVal10) ||
			reqDocGroup.equals(reqDocGroupVal11) ||
			reqDocGroup.equals(reqDocGroupVal12)) {
			if (objectHome == null) {
				validObjectHome = idle;
				validAll = false;
			}
			else if (objectHome.equals("")) {
				validObjectHome = idle;
				validAll = false;
			}
			else {
				validObjectHome = ok;
			}
		}
		
		
		
		if (objectMoreInfo2.equals(objectMoreInfo2Val1)) {
			if (reqDocGroup.equals(reqDocGroupVal12)) {
				if (objectUrlGeo != null) {
					if (!objectUrlGeo.equals("")) {
						String MAPS_YANDEX_2GIS_GOOGLE_URL = "^(http(s{0,1})://)?(www.)?((maps.google.com)|(maps.yandex.ru)|(maps.2gis.ru)|(openstreetmap.ru))/.*+$";
						if (MyUtils.doMatch(objectUrlGeo, MAPS_YANDEX_2GIS_GOOGLE_URL)) {
							validObjectUrlGeo = ok;
						}
						else {
							validObjectUrlGeo = badLink;
							validAll = false;
						}
					}
					else {
						validObjectUrlGeo = ok;
					}
				} 
			}
		}
		
		//если файл был прикреплен, то свалидируем его
		if (objectAttach != null){
			if (reqDocGroup.equals(reqDocGroupVal12)) {
				//сделаем ok, чтобы убедиться что не старое состояние
				validObjectAttach = ok;
				try {
					//проверка на формат
					if (!editBean.getFormatAttach().equals("")) {
			            if (!MyUtils.findExtInRow(MyUtils.getExtNotDot(objectAttach.getName()), editBean.getFormatAttach(), editBean.getRegexFormatAttach())){
			            	validObjectAttach = badFormatAttach;
			            	validAll = false;
			            	//Удалить файл
			            	FileUtils.forceDelete(objectAttach);
			            } else validObjectAttach = ok;
		            } else validObjectAttach = ok;
					
					//проверка на размер
		            if (Double.valueOf(String.valueOf(objectAttach.length())) > Double.valueOf(editBean.getMaxSizeAttach())*1024.0*1024.0) {
		            	validObjectAttach = badSizeAttach;
		            	validAll = false;
		            	//Удалить файл
		            	FileUtils.forceDelete(objectAttach);
		            } else {
		            	//если не недопустимый формат, то валидируем положительно
		            	if (!validObjectAttach.equals(badFormatAttach) ){
		            		validObjectAttach = ok;
		            	}
		            }
		            
		            ///Если файл прошел валидацию, отправим его во временную папку
		            //if (validObjectAttach.equals("ok")) {
						//создать временную папку для request.xml
		            	//if (requestDir == null || !requestDir.exists()) {
	            			//requestDir = createRequestDir();
	            		//}
							
						/*в commons io v.1.3.1 отсутсвует moveFileToDirectory, 
						 поэтому копируем в папку requestDir, удаляем objectAttach
						 и objectAttach ссылаем на файл в папке requestDir*/
						//FileUtils.moveFileToDirectory(objectAttach, requestDir, false);
						//FileUtils.copyFileToDirectory(objectAttach, requestDir);
						//FileUtils.forceDelete(objectAttach);
		            	//FileUtils.moveFileToDirectory(objectAttach, requestDir, false);
		            	//по идее нужный файл в папке будет последним, поэтому индекс в листе делаем последним
						//objectAttach = requestDir.listFiles()[requestDir.list().length - 1];
		            //}
		            
		            
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		return validAll;
	}
	public boolean getValidAll() {
		return validAll;
	}
	public void setValidAll(boolean validAll) {
		this.validAll = validAll;
	}
	//НЕ ЗАБЫВАЙТЕ СБРАСЫВАТЬ!!!
	public void resetValid() {
		validReqDocGroup = ok;
		validPurposeRequest = ok;
		validTypeApplicant = ok;
		
		validIndividualLastname = ok;
		validIndividualFirstname = ok;
		
		validIndividualDayBirthday = ok;
		validIndividualMonthBirthday = ok;
		validIndividualYearBirthday = ok;
		
		validIndividualSeriesPassport = ok;
		validIndividualNumberPassport = ok;
		validIndividualWhoGivePassport = ok;
		validIndividualDayIssuePassport = ok;
		validIndividualMonthIssuePassport = ok;
		validIndividualYearIssuePassport = ok;
		validIndividualAttachPassport = ok;
		validIndividualProcuratoryAttachPassport = ok;
		validIndividualCity = ok;
		validIndividualStreet = ok;
		validIndividualHome = ok;
		validIndividualNumberTelephone = ok;
		validIndividualEmail = ok;
		
		validLegalName = ok;
		validLegalOgrn = ok;
		validLegalInn = ok;
		validLegalHeadLastname = ok;
		validLegalHeadFirstname = ok;
		validLegalProcuratoryAttach = ok;
		validLegalAsHead = ok;
		validLegalIndex = ok;
		validLegalCountry = ok;
		validLegalCity = ok;
		validLegalStreet = ok;
		validLegalHome = ok;
		validLegalNumberTelephone = ok;
		validLegalEmail = ok;
		
		validYearHousingProgram = ok;
		validKindObject = ok;
		validObjectCity = ok;
		validObjectStreet = ok;
		validObjectHome = ok;
		validObjectUrlGeo = ok;
		validObjectAttach = ok;
		validAll = true;
	}
	//НЕ ЗАБЫВАЙТЕ СБРАСЫВАТЬ ЗНАЧЕНИЯ!!!
	public void resetValue() {
		reqDocGroup = "";
		
		purposeRequest = "";
		typeApplicant = "";
		
		individualLastname = "";
		individualFirstname = "";
		individualMiddlename = "";
		individualDayBirthday = "";
		individualMonthBirthday = "";
		individualYearBirthday = "";
		individualChangeLastFirstMiddleName = "";
		individualSeriesPassport = "";
		individualNumberPassport = "";
		individualWhoGivePassport = "";
		individualDayIssuePassport = "";
		individualMonthIssuePassport = "";
		individualYearIssuePassport = "";
		individualAttachPassport = null;
		individualProcuratoryAttachPassport = null;
		individualCity = "";
		individualStreet = "";
		individualHome = "";
		individualHousing = "";
		individualApartment = "";
		individualSection = "";
		individualRoom = "";
		individualNumberTelephone = "";
		individualEmail = "";
		
		legalName = "";
		legalOgrn = "";
		legalInn = "";
		legalHeadLastname = "";
		legalHeadFirstname = "";
		legalHeadMiddlename = "";
		legalProcuratoryAttach = null;
		legalAsHead = "";
		legalIndex = "";
		legalCountry = "";
		legalCity = "";
		legalStreet = "";
		legalHome = "";
		legalHousing = "";
		legalApartment = "";
		legalSection = "";
		legalRoom = "";
		legalNumberTelephone = "";
		legalEmail = "";
		
		contractNumber = "";
		dateContract = "";
		dayExactDateContract = "";
		monthExactDateContract = "";
		yearExactDateContract = "";
		dayDateRangeContract1 = "";
		monthDateRangeContract1 = "";
		yearDateRangeContract1 = "";
		dayDateRangeContract2 = "";
		monthDateRangeContract2 = "";
		yearDateRangeContract2 = "";
		
		yearHousingProgram = "";
		kindObject = "";
		objectCity = "";
		objectStreet = "";
		objectHome = "";
		objectHousing = "";
		objectApartment = "";
		objectSection = "";
		objectRoom = "";
		objectMoreInfo1 = "";
		objectMoreInfo2 = "";
		objectUrlGeo = "";
		objectOtherInfo = "";
		//objectInventoryNumber = "";
		objectInventoryNumberVal = "";
		objectAttach = null;
		objectDoc = "";
		numberDoc = "";
		publicDateDoc = "";
		publicDayExactDateDoc = "";
		publicMonthExactDateDoc = "";
		publicYearExactDateDoc = "";
		dayPublicDateRangeDoc1 = "";
		monthPublicDateRangeDoc1 = "";
		yearPublicDateRangeDoc1 = "";
		dayPublicDateRangeDoc2 = "";
		monthPublicDateRangeDoc2 = "";
		yearPublicDateRangeDoc2 = "";
		authorDoc = "";
	}
	public String getSent() {
		return sent;
	}
	public void setSent(String sent) {
		this.sent = sent;
	}
	/*
	public boolean isViewForm() {
		return viewForm;
	}
	public void setViewForm(boolean viewForm) {
		this.viewForm = viewForm;
	}
	*/
	public boolean isProcessing() {
		return processing;
	}
	public void setProcessing(boolean processing) {
		this.processing = processing;
	}
	
	public String getQuestionList(String reqDocGroup) {
		return reqDocGroup;
	}
	public String getSubTipList(String reqDocGroup) {
		//1
		if (reqDocGroup.equals(reqDocGroupVal1)) {
			return "3.1. Услуги в сфере жилищной политики";
		}
		//2
		if (reqDocGroup.equals(reqDocGroupVal2)) {
			return "3.1. Услуги в сфере жилищной политики";
		}
		//3
		if (reqDocGroup.equals(reqDocGroupVal3)) {
			return "3.1. Услуги в сфере жилищной политики"; 
		}
		//4
		if (reqDocGroup.equals(reqDocGroupVal4)) {
			return "3.1. Услуги в сфере жилищной политики"; 
		}
		//5
		if (reqDocGroup.equals(reqDocGroupVal5)) {
			return "3.1. Услуги в сфере жилищной политики";
		}
		//6
		if (reqDocGroup.equals(reqDocGroupVal6)) {
			return "3.1. Услуги в сфере жилищной политики";
		}
		//7
		if (reqDocGroup.equals(reqDocGroupVal7)) {
			return "3.1. Услуги в сфере жилищной политики";
		}
		//8
		/*if (reqDocGroup.equals("Справка о перспективах застройки города Омска")) {
			return "4.1. Услуги в сфере архитектуры и градостроительства";
		}*/
		//9
		/*if (reqDocGroup.equals("Справка о разрешенном использовании земельных участков в соответствии с Правилами " +
		"землепользования и застройки муниципального образования городской округ город Омск Омской области")) {
			return "4.1. Услуги в сфере архитектуры и градостроительства";
		}*/
		//10
		if (reqDocGroup.equals(reqDocGroupVal10)) {
			return "5.1. Услуги в сфере имущественных отнош";
		}
		//11
		if (reqDocGroup.equals(reqDocGroupVal11)) {
			return "5.1. Услуги в сфере имущественных отнош";
		}
		//12
		if (reqDocGroup.equals(reqDocGroupVal12)) {
			return "8.1. Услуги округов";
		}
		
		return "";
	}
	public String getTopicKodList(String reqDocGroup) {
		
		//1
		if (reqDocGroup.equals(reqDocGroupVal1)) {
			return "3";
		}
		//2
		if (reqDocGroup.equals(reqDocGroupVal2)) {
			return "3";
		}
		//3
		if (reqDocGroup.equals(reqDocGroupVal3)) {
			return "3"; 
		}
		//4
		if (reqDocGroup.equals(reqDocGroupVal4)) {
			return "3"; 
		}
		//5
		if (reqDocGroup.equals(reqDocGroupVal5)) {
			return "3";
		}
		//6
		if (reqDocGroup.equals(reqDocGroupVal6)) {
			return "3";
		}
		//7
		if (reqDocGroup.equals(reqDocGroupVal7)) {
			return "3";
		}
		//8
		/*if (reqDocGroup.equals("Справка о перспективах застройки города Омска")) {
			return "4";
		}*/
		//9
		/*if (reqDocGroup.equals("Справка о разрешенном использовании земельных участков в соответствии с Правилами " +
		"землепользования и застройки муниципального образования городской округ город Омск Омской области")) {
			return "4";
		}*/
		//10
		if (reqDocGroup.equals(reqDocGroupVal10)) {
			return "5";
		}
		//11
		if (reqDocGroup.equals(reqDocGroupVal11)) {
			return "5";
		}
		//12
		if (reqDocGroup.equals(reqDocGroupVal12)) {
			return "8";
		}
		
		return "";
	}
	public String getTopicList(String reqDocGroup) {
		
		//1
		if (reqDocGroup.equals(reqDocGroupVal1)) {
			return "Услуги в сфере жилищной политики";
		}
		//2
		if (reqDocGroup.equals(reqDocGroupVal2)) {
			return "Услуги в сфере жилищной политики";
		}
		//3
		if (reqDocGroup.equals(reqDocGroupVal3)) {
			return "Услуги в сфере жилищной политики"; 
		}
		//4
		if (reqDocGroup.equals(reqDocGroupVal4)) {
			return "Услуги в сфере жилищной политики"; 
		}
		//5
		if (reqDocGroup.equals(reqDocGroupVal5)) {
			return "Услуги в сфере жилищной политики";
		}
		//6
		if (reqDocGroup.equals(reqDocGroupVal6)) {
			return "Услуги в сфере жилищной политики";
		}
		//7
		if (reqDocGroup.equals(reqDocGroupVal7)) {
			return "Услуги в сфере жилищной политики";
		}
		//8
		/*if (reqDocGroup.equals("Справка о перспективах застройки города Омска")) {
			return "Услуги в сфере архитектуры и градостроительства";
		}*/
		//9
		/*if (reqDocGroup.equals("Справка о разрешенном использовании земельных участков в соответствии с Правилами " +
		"землепользования и застройки муниципального образования городской округ город Омск Омской области")) {
			return "Услуги в сфере архитектуры и градостроительства";
		}*/
		//10
		if (reqDocGroup.equals(reqDocGroupVal10)) {
			return "Услуги в сфере имущественных отнош";
		}
		//11
		if (reqDocGroup.equals(reqDocGroupVal11)) {
			return "Услуги в сфере имущественных отнош";
		}
		//12
		if (reqDocGroup.equals(reqDocGroupVal12)) {
			return "Услуги округов";
		}
		
		return "";
	}
	/*
	public Exception getException() {
		return exception;
	}
	public void setException(Exception exception) {
		this.exception = exception;
	}
	*/
	/*public String getLocalizedMessage() {
		return localizedMessage;
	}
	public void setLocalizedMessage(String localizedMessage) {
		this.localizedMessage = localizedMessage;
	}*/
	@SuppressWarnings("finally")
	public boolean sendMessage() throws ReadOnlyException {
		boolean result = false;
		
		SendMailUsage sendMailUsage = new SendMailUsage();
		ArrayList<File> alAttach = null;
		sendMailUsage.setFrom(editBean.getFrom());
		sendMailUsage.setPassword(editBean.getPassFrom());
		sendMailUsage.setTo(editBean.getTo());
		sendMailUsage.setHost(editBean.getHost());
		sendMailUsage.setPort(Integer.valueOf(editBean.getPort()));
		sendMailUsage.setUsername("Portal");
		sendMailUsage.setSubject("InfoDifferentAreas");
		sendMailUsage.setText("");
		
		//Создать request.xml
		PrintWriter pw = null;
		//создать request.xml
		File request = new File(requestDir, "request.xml");
		
		try {
			//pw = new PrintWriter(new FileOutputStream(request));
			pw = new PrintWriter(request, "UTF-8");
			//pw = new PrintWriter(request);
			
			/*Записать данные request xml*/
			/*pw.println("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
			pw.println("<UserQuestion xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"UserRequests.xsd\" >");
			
			pw.println("<reqDocGroup>"+reqDocGroup+"</reqDocGroup>");
			pw.println("<purposeRequest>"+purposeRequest+"</purposeRequest>");
			pw.println("<typeApplicant>"+typeApplicant+"</typeApplicant>");
			pw.println("<individualLastname>"+individualLastname+"</individualLastname>");
			pw.println("<individualFirstname>"+individualFirstname+"</individualFirstname>");
			pw.println("<individualMiddlename>"+individualMiddlename+"</individualMiddlename>");
			pw.println("<individualSeriesPassport>"+individualSeriesPassport+"</individualSeriesPassport>");
			pw.println("<individualNumberPassport>"+individualNumberPassport+"</individualNumberPassport>");
			pw.println("<individualWhoGivePassport>"+individualWhoGivePassport+"</individualWhoGivePassport>");
			pw.println("<individualDayMonthYearIssuePassport>"+individualDayIssuePassport+"."+individualMonthIssuePassport+"."+individualYearIssuePassport+"</individualDayMonthYearIssuePassport>");
			pw.println("<individualCity>"+individualCity+"</individualCity>");
			pw.println("<individualStreet>"+individualStreet+"</individualStreet>");
			pw.println("<individualHome>"+individualHome+"</individualHome>");
			pw.println("<individualHousing>"+individualHousing+"</individualHousing>");
			pw.println("<individualApartment>"+individualApartment+"</individualApartment>");
			pw.println("<individualSection>"+individualSection+"</individualSection>");
			pw.println("<individualRoom>"+individualRoom+"</individualRoom>");
			pw.println("<individualNumberTelephone>"+individualNumberTelephone+"</individualNumberTelephone>");
			pw.println("<individualEmail>"+individualEmail+"</individualEmail>");
			
			pw.println("<legalName>"+legalName+"</legalName>");
			pw.println("<legalOgrn>"+legalOgrn+"</legalOgrn>");
			pw.println("<legalInn>"+legalInn+"</legalInn>");
			pw.println("<legalHeadLastname>"+legalHeadLastname+"</legalHeadLastname>");
			pw.println("<legalHeadFirstname>"+legalHeadFirstname+"</legalHeadFirstname>");
			pw.println("<legalHeadMiddlename>"+legalHeadMiddlename+"</legalHeadMiddlename>");
			pw.println("<legalAsHead>"+legalAsHead+"</legalAsHead>");
			pw.println("<legalIndex>"+legalIndex+"</legalIndex>");
			pw.println("<legalCountry>"+legalCountry+"</legalCountry>");
			pw.println("<legalCity>"+legalCity+"</legalCity>");
			pw.println("<legalStreet>"+legalStreet+"</legalStreet>");
			pw.println("<legalHome>"+legalHome+"</legalHome>");
			pw.println("<legalHousing>"+legalHousing+"</legalHousing>");
			pw.println("<legalApartment>"+legalApartment+"</legalApartment>");
			pw.println("<legalSection>"+legalSection+"</legalSection>");
			pw.println("<legalRoom>"+legalRoom+"</legalRoom>");
			pw.println("<legalNumberTelephone>"+legalNumberTelephone+"</legalNumberTelephone>");
			pw.println("<legalEmail>"+legalEmail+"</legalEmail>");
			
			pw.println("<contractNumber>"+contractNumber+"</contractNumber>");
			//pw.println("<dateContract>"+dateContract+"</dateContract>");
			pw.println("<dayMonthYearExactDateContract>"+dayExactDateContract+"."+monthExactDateContract+"."+yearExactDateContract+"</dayMonthYearExactDateContract>");
			pw.println("<dayMonthYearDateRangeContract1>"+dayDateRangeContract1+"."+monthDateRangeContract1+"."+yearDateRangeContract1+"</dayMonthYearDateRangeContract1>");
			pw.println("<dayMonthYearDateRangeContract2>"+dayDateRangeContract2+"."+monthDateRangeContract2+"."+yearDateRangeContract2+"</dayMonthYearDateRangeContract1>");
			
			pw.println("<kindObject>"+kindObject+"</kindObject>");
			pw.println("<objectCity>"+objectCity+"</objectCity>");
			pw.println("<objectStreet>"+objectStreet+"</objectStreet>");
			pw.println("<objectHome>"+objectHome+"</objectHome>");
			pw.println("<objectHousing>"+objectHousing+"</objectHousing>");
			pw.println("<objectApartment>"+objectApartment+"</objectApartment>");
			pw.println("<objectSection>"+objectSection+"</objectSection>");
			pw.println("<objectRoom>"+objectRoom+"</objectRoom>");
			pw.println("<objectMoreInfo1>"+objectMoreInfo1+"</objectMoreInfo1>");
			//pw.println("<objectMoreInfo2>"+objectMoreInfo2+"</objectMoreInfo2>");
			pw.println("<objectUrlGeo>"+objectUrlGeo+"</objectUrlGeo>");
			pw.println("<objectOtherInfo>"+objectOtherInfo+"</objectOtherInfo>");
			pw.println("<objectInventoryNumber>"+objectInventoryNumber+"</objectInventoryNumber>");
			if (objectAttach != null) {
				pw.println("<objectAttach>"+objectAttach.getName()+"</objectAttach>");
			} else {
				pw.println("<objectAttach></objectAttach>");
			}
			//pw.println("<objectAttach>"+objectAttach+"</objectAttach>");
			pw.println("<objectDoc>"+objectDoc+"</objectDoc>");
			pw.println("<numberDoc>"+numberDoc+"</numberDoc>");
			//pw.println("<publicDateDoc>"+publicDateDoc+"</publicDateDoc>");
			pw.println("<publicDayMonthYearExactDateDoc>"+publicDayExactDateDoc+"."+publicMonthExactDateDoc+"."+publicYearExactDateDoc+"</publicDayMonthYearExactDateDoc>");
			pw.println("<dayMonthYearPublicDateRangeDoc1>"+dayPublicDateRangeDoc1+"."+monthPublicDateRangeDoc1+"."+yearPublicDateRangeDoc1+"</dayMonthYearPublicDateRangeDoc1>");
			pw.println("<dayMonthYearPublicDateRangeDoc2>"+dayPublicDateRangeDoc2+"."+monthPublicDateRangeDoc2+"."+yearPublicDateRangeDoc2+"</dayMonthYearPublicDateRangeDoc2>");
			pw.println("<authorDoc>"+authorDoc+"</authorDoc>");
			pw.println("<depCode>"+"61"+"</depCode>");
			
			pw.println("</UserQuestion>");
			*/
			
			//////!!!!!!!!!!!НОВЫЙ
			pw.println("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
			pw.println("<UniRequest xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"UniRequest.xsd\">");
			pw.println("<alterName></alterName>");
			pw.println("<nameLast>" + individualLastname + "</nameLast>");
			pw.println("<nameFirst>" + individualFirstname + "</nameFirst>");
			pw.println("<nameMiddle>" + individualMiddlename + "</nameMiddle>");
			StringBuffer passportData = new StringBuffer();
			passportData.append("");
			if (individualSeriesPassport != null) {
				if (!individualSeriesPassport.equals("")) {
					passportData.append(individualSeriesPassport);
					passportData.append('\n');
				}
			}
			if (individualNumberPassport != null) {
				if (!individualNumberPassport.equals("")) {
					passportData.append(individualNumberPassport);
					passportData.append('\n');
				}
			}
			if (individualWhoGivePassport != null) {
				if (!individualWhoGivePassport.equals("")) {
					passportData.append(individualWhoGivePassport);
					passportData.append('\n');
				}
			}
			if (individualDayIssuePassport != null && individualMonthIssuePassport != null && individualYearIssuePassport != null) {
				if (!individualDayIssuePassport.equals("") && !individualMonthIssuePassport.equals("") && !individualYearIssuePassport.equals("")) {
					passportData.append(individualDayIssuePassport);
					passportData.append(".");
					passportData.append(individualMonthIssuePassport);
					passportData.append(".");
					passportData.append(individualYearIssuePassport);
				}
			}
			pw.println("<passport>" + passportData.toString() + "</passport>");
			pw.println("<phone>" + individualNumberTelephone + "</phone>");
			pw.println("<login>" + individualEmail + "</login>");        
			//Адресная информация о гражданине
			pw.println("<country></country>");
			pw.println("<state></state>");
			pw.println("<city>" + individualCity + "</city>");
			pw.println("<region></region>");
			pw.println("<street>" + individualStreet + "</street>");
			pw.println("<house>" + individualHome + "</house>");
			pw.println("<corp>" + individualHousing + "</corp>");
			pw.println("<flat>" + individualApartment + "</flat>");
			pw.println("<blok>" + individualSection + "</blok>");
			pw.println("<room>" + individualRoom + "</room>");
			pw.println("<zipCode></zipCode>");
			pw.println("<locality></locality>");
			//// Адресная информация о орагнизации-->
			pw.println("<orgName>" + legalName + "</orgName>");
			pw.println("<orgNameShort></orgNameShort>");
			pw.println("<orgOgrn>" + legalOgrn + "</orgOgrn>");
			pw.println("<orgInn>" + legalInn + "</orgInn>");
			pw.println("<orgDirectorLastName>" + legalHeadLastname + "</orgDirectorLastName>");
			pw.println("<orgDirectorFirstName>" + legalHeadFirstname + "</orgDirectorFirstName>");
			pw.println("<orgDirectorMiddleName>" + legalHeadMiddlename + "</orgDirectorMiddleName>");
			pw.println("<orgDirectorPost>" + legalAsHead + "</orgDirectorPost>");
			
			StringBuffer orgAddress = new StringBuffer();
			orgAddress.append("");
			if (legalIndex != null) {
				if (!legalIndex.equals("")) {
					orgAddress.append(legalIndex);
					orgAddress.append('\n');
				}
			}
			if (legalCountry != null) {
				if (!legalCountry.equals("")) {
					orgAddress.append(legalCountry);
					orgAddress.append('\n');
				}
			}
			if (legalCity != null) {
				if (!legalCity.equals("")) {
					orgAddress.append(legalCity);
					orgAddress.append('\n');
				}
			}
			if (legalStreet != null) {
				if (!legalStreet.equals("")) {
					orgAddress.append(legalStreet);
					orgAddress.append('\n');
				}
			}
			if (legalHome != null) {
				if (!legalHome.equals("")) {
					orgAddress.append(legalHome);
					orgAddress.append('\n');
				}
			}
			if (legalHousing != null) {
				if (!legalHousing.equals("")) {
					orgAddress.append(legalHousing);
					orgAddress.append('\n');
				}
			}
			if (legalApartment != null) {
				if (!legalApartment.equals("")) {
					orgAddress.append(legalApartment);
					orgAddress.append('\n');
				}
			}
			if (legalSection != null) {
				if (!legalSection.equals("")) {
					orgAddress.append(legalSection);
					orgAddress.append('\n');
				}
			}
			if (legalRoom != null) {
				if (!legalRoom.equals("")) {
					orgAddress.append(legalRoom);
					orgAddress.append('\n');
				}
			}
			pw.println("<orgAddress>"+ orgAddress.toString() + "</orgAddress>");
			pw.println("<orgTelefon>" + legalNumberTelephone + "</orgTelefon>");
			pw.println("<orgEmail>" + legalEmail + "</orgEmail>");
			/////Информация для маршрутизации обращения-->
			//Код структурного подразделения АГО в котором должна регистрироваться Услуга
			pw.println("<depCode>61</depCode>");
			pw.println("<depCodeSub>72</depCodeSub>");
			////Информация об услуги заполняется на основании настройки Услуги-->
			///если указан год прогаммы жилище (это reqDocGroupVal2)
			if (reqDocGroup.equals(reqDocGroupVal2) && !yearHousingProgram.equals("")) {
				pw.println("<QuestionList>" + getQuestionList(reqDocGroup + " " + yearHousingProgram) + "</QuestionList>");	
			} else {
				pw.println("<QuestionList>" + getQuestionList(reqDocGroup) + "</QuestionList>");
			}
			pw.println("<SubTipList>" + getSubTipList(reqDocGroup) + "</SubTipList>");
			pw.println("<TopicKodList>" + getTopicKodList(reqDocGroup) + "</TopicKodList>");
			pw.println("<TopicList>" + getTopicList(reqDocGroup) + "</TopicList>");
			/*pw.println("<object>");
			pw.println("<Name>Название обьекта</objectName>");
			pw.println("<Value>Значение обьекта</objectName>");
			pw.println("</object>");*/
			
			if (individualAttachPassport != null) {
				pw.println("<file>"+individualAttachPassport.getName()+"</file>");
			}
			
			if (individualProcuratoryAttachPassport != null) {
				pw.println("<file>"+individualProcuratoryAttachPassport.getName()+"</file>");
			}
			
			if (legalProcuratoryAttach != null) {
				pw.println("<file>"+legalProcuratoryAttach.getName()+"</file>");
			}
			
			if (objectAttach != null) {
				pw.println("<file>"+objectAttach.getName()+"</file>");
			} 
			
			if ( (individualAttachPassport == null) && (individualProcuratoryAttachPassport == null) && (legalProcuratoryAttach == null) && (objectAttach == null) ) {
				pw.println("<file></file>");
			}
			
			StringBuffer body = new StringBuffer();
			body.append("");
			
			if (purposeRequest != null) {
				if (!purposeRequest.equals("")) {
					body.append("Цель (суть) запроса: ");
					body.append(purposeRequest);
					body.append('\n');
				}
			}
			
			if (individualDayBirthday != null && individualMonthBirthday != null && individualYearBirthday != null) {
				if (!individualDayBirthday.equals("") && !individualMonthBirthday.equals("") && !individualYearBirthday.equals("")) {
					body.append("Дата рождения заявителя: ");
					body.append(individualDayBirthday);
					body.append(".");
					body.append(individualMonthBirthday);
					body.append(".");
					body.append(individualYearBirthday);
					body.append('\n');
				}
			}
			
			if (individualChangeLastFirstMiddleName != null) {
				if (!individualChangeLastFirstMiddleName.equals("")) {
					body.append("Заявитель менял фамилию, имя или отчество: ");
					body.append(individualChangeLastFirstMiddleName);
					body.append('\n');
				}
			}
			
			if (contractNumber != null) {
				if (!contractNumber.equals("")) {
					body.append("Номер договора: ");
					body.append(contractNumber);
					body.append('\n');
				}
			}
			
			if (dayExactDateContract != null && monthExactDateContract != null && yearExactDateContract != null) {
				if (!dayExactDateContract.equals("") && !monthExactDateContract.equals("") && !yearExactDateContract.equals("")) {
					body.append("Точная дата договора: ");
					body.append(dayExactDateContract);
					body.append(".");
					body.append(monthExactDateContract);
					body.append(".");
					body.append(yearExactDateContract);
					body.append('\n');
				}
			}
			
			if (dayDateRangeContract1 != null && monthDateRangeContract1 != null && yearDateRangeContract1 != null &&
				dayDateRangeContract2 != null && monthDateRangeContract2 != null && yearDateRangeContract2 != null
			) {
				if (!dayDateRangeContract1.equals("") && !monthDateRangeContract1.equals("") && !yearDateRangeContract1.equals("") &&
					!dayDateRangeContract2.equals("") && !monthDateRangeContract2.equals("") && !yearDateRangeContract2.equals("")
				) {
					body.append("Диапазон дат договора: ");
					body.append(dayDateRangeContract1);
					body.append(".");
					body.append(monthDateRangeContract1);
					body.append(".");
					body.append(yearDateRangeContract1);
					body.append(" - ");
					body.append(dayDateRangeContract2);
					body.append(".");
					body.append(monthDateRangeContract2);
					body.append(".");
					body.append(yearDateRangeContract2);
					body.append('\n');
				}
			}
			
			if (kindObject != null) {
				if (!kindObject.equals("")) {
					body.append("Вид объекта: ");
					body.append(kindObject);
					body.append('\n');
				}
			}
			
			if (objectCity != null) {
				if (!objectCity.equals("")) {
					body.append("Город объекта: ");
					body.append(objectCity);
					body.append('\n');
				}
			}
			
			if (objectStreet != null) {
				if (!objectStreet.equals("")) {
					body.append("Улица объекта: ");
					body.append(objectStreet);
					body.append('\n');
				}
			}
			
			if (objectHome != null) {
				if (!objectHome.equals("")) {
					body.append("Дом объекта: ");
					body.append(objectHome);
					body.append('\n');
				}
			}
			
			if (objectHousing != null) {
				if (!objectHousing.equals("")) {
					body.append("Корпус объекта: ");
					body.append(objectHousing);
					body.append('\n');
				}
			}
			
			if (objectApartment != null) {
				if (!objectApartment.equals("")) {
					body.append("Квартира объекта: ");
					body.append(objectApartment);
					body.append('\n');
				}
			}
			
			if (objectSection != null) {
				if (!objectSection.equals("")) {
					body.append("Секция объекта: ");
					body.append(objectSection);
					body.append('\n');
				}
			}
			
			if (objectRoom != null) {
				if (!objectRoom.equals("")) {
					body.append("Комната объекта: ");
					body.append(objectRoom);
					body.append('\n');
				}
			}
			
			if (objectMoreInfo1 != null) {
				if (!objectMoreInfo1.equals("")) {
					body.append("Дополнительные сведения, " +
							"которые могут облегчить идентификацию объекта — " +
							"например, кадастровый или условный номер, а также " +
							"иные характеристики объекта: ");
					body.append(objectMoreInfo1);
					body.append('\n');
				}
			}
			
			if (objectUrlGeo != null) {
				if (!objectUrlGeo.equals("")) {
					body.append("Гиперссылка на фрагмент карты в одной из геоинформационных систем: ");
					body.append(objectUrlGeo);
					body.append('\n');
				}
			}
			
			if (objectOtherInfo != null) {
				if (!objectOtherInfo.equals("")) {
					body.append("Иные сведения о местоположении объекта на карте: ");
					body.append(objectOtherInfo);
					body.append('\n');
				}
			}
			
			if (objectInventoryNumberVal != null) {
				if (!objectInventoryNumberVal.equals("")) {
					body.append("Кадастровый номер: ");
					body.append(objectInventoryNumberVal);
					body.append('\n');
				}
			}
			
			if (objectDoc != null) {
				if (!objectDoc.equals("")) {
					body.append("Вид, название документа или опорные слова из " +
							"содержательной части документа по объекту, которому присвоен адрес: ");
					body.append(objectDoc);
					body.append('\n');
				}
			}
			
			if (numberDoc != null) {
				if (!numberDoc.equals("")) {
					body.append("Номер документа: ");
					body.append(numberDoc);
					body.append('\n');
				}
			}
			
			if (publicDayExactDateDoc != null && publicMonthExactDateDoc != null && publicYearExactDateDoc != null) {
				if (!publicDayExactDateDoc.equals("") && !publicMonthExactDateDoc.equals("") && !publicYearExactDateDoc.equals("")) {
					body.append("Точная дата издания документа: ");
					body.append(publicDayExactDateDoc);
					body.append(".");
					body.append(publicMonthExactDateDoc);
					body.append(".");
					body.append(publicYearExactDateDoc);
					body.append('\n');
				}
			}
			
			if (dayPublicDateRangeDoc1 != null && monthPublicDateRangeDoc1 != null && yearPublicDateRangeDoc1 != null &&
				dayPublicDateRangeDoc2 != null && monthPublicDateRangeDoc2 != null && yearPublicDateRangeDoc2 != null
			) {
				if (!dayPublicDateRangeDoc1.equals("") && !monthPublicDateRangeDoc1.equals("") && !yearPublicDateRangeDoc1.equals("") &&
					!dayPublicDateRangeDoc2.equals("") && !monthPublicDateRangeDoc2.equals("") && !yearPublicDateRangeDoc2.equals("")
				) {
					body.append("Диапазон дат издания документа: ");
					body.append(dayPublicDateRangeDoc1);
					body.append(".");
					body.append(monthPublicDateRangeDoc1);
					body.append(".");
					body.append(yearPublicDateRangeDoc1);
					body.append(" - ");
					body.append(dayPublicDateRangeDoc2);
					body.append(".");
					body.append(monthPublicDateRangeDoc2);
					body.append(".");
					body.append(yearPublicDateRangeDoc2);
					body.append('\n');
				}
			}
			
			if (authorDoc != null) {
				if (!authorDoc.equals("")) {
					body.append("Администрация соответствующего административного округа города Омска: ");
					body.append(authorDoc);
				}
			}
			
			pw.println("<body>" + body.toString() + "</body>");
			//"<body>" + new String(body.toString().getBytes(), "utf-8") + "</body>");
			
			pw.println("</UniRequest>");
			
			pw.close();
			
			//Прикрепить request.xml
			alAttach = new ArrayList<File>();
			alAttach.add(request);
			
			if (individualAttachPassport != null){
				alAttach.add(individualAttachPassport);
			}
			
			if (individualProcuratoryAttachPassport != null){
				alAttach.add(individualProcuratoryAttachPassport);
			}
			
			if (legalProcuratoryAttach != null){
				alAttach.add(legalProcuratoryAttach);
			}
			
			if (objectAttach != null){
				alAttach.add(objectAttach);
			}
			
			sendMailUsage.setFileAsAttachment(alAttach);
			result = sendMailUsage.sendMessage();
			
		}
		catch(Exception e) {
			_log.info("Get StackTrace:");
			e.printStackTrace();
		} finally {
			
			try {
				//удалить файлы
				if (alAttach != null) {
					for (File f:alAttach) {
						if (f != null) {
							if (f.exists()) {
								FileUtils.forceDelete(f);
								f = null;
							}
						} 
					}
				}
				//удалить папку 
				if (requestDir != null) {
					FileUtils.deleteDirectory(requestDir);
					//FileUtil.deltree(requestDir);
					//FileUtils.forceDelete(requestDir);
				}
				
				alAttach = null;
				requestDir = null;
				
			} catch (Exception e) {
				_log.info("Get StackTrace:");
				e.printStackTrace();
			}
			return result;
		}
		
		
	}
	
	public File getRequestDir() {
		if (requestDir == null) {
			requestDir = createRequestDir();
		}
		return requestDir;
	}
	
	@SuppressWarnings("finally")
	public File createRequestDir() {
		File rd = null;
		File tempFile = null;
		/*новый способ через commons io, в надеже что это поможет манипулировать 
		с файлами, без хвостов*/
		//Создать временный файл tempFile(во временной директории)
		try {
			tempFile = File.createTempFile("info_different_areas", "");
			//Получить path временного файла
			String pathTempFile = tempFile.getAbsolutePath();
			//Удалить временный файл
			FileUtils.forceDelete(tempFile);
			//создать директорию на основе path временного файла
			rd = new File(pathTempFile);
			FileUtils.forceMkdir(rd);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			return rd;
		}
		
	}
	
	public String getViewJSPURL() {
		return viewJSPURL;
	}
	public void setViewJSPURL(String viewJSPURL) {
		this.viewJSPURL = viewJSPURL;
	}
}