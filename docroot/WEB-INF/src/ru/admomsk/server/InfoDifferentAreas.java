/**
 * Copyright (c) 2000-2009 Liferay, Inc. All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package ru.admomsk.server;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.portlet.PortletFileUpload;
import org.apache.commons.io.FileUtils;

import ru.admomsk.server.beans.EditBean;
import ru.admomsk.server.beans.ViewBean;
import ru.admomsk.utils.MyUtils;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.util.PortalUtil;

/**
 * <a href="JSPPortlet.java.html"><b><i>View Source</i></b></a>
 *
 * @author ilya
 *
 */
public class InfoDifferentAreas extends GenericPortlet {
	
	protected String editJSP;
	protected String helpJSP;
	protected String viewJSP;

	private static Log _log = LogFactoryUtil.getLog("InfoDifferentAreas");
	
	public void init() throws PortletException {
		editJSP = getInitParameter("edit-jsp");
		helpJSP = getInitParameter("help-jsp");
		viewJSP = getInitParameter("view-jsp");
	}

	public void doDispatch(
			RenderRequest renderRequest, RenderResponse renderResponse)
		throws IOException, PortletException {

		String jspPage = renderRequest.getParameter("jspPage");

		if (jspPage != null) {
			include(jspPage, renderRequest, renderResponse);
		}
		else {
			super.doDispatch(renderRequest, renderResponse);
		}
	}
	
	@Override
    protected void doEdit(RenderRequest renderRequest, RenderResponse renderResponse)
    		throws PortletException, IOException {
    	PortletSession portletSession = renderRequest.getPortletSession();
    	//если EditBean не создан, то создать его
    	if ((EditBean)portletSession.getAttribute("editBean") == null){
    		portletSession.setAttribute("editBean", new EditBean());
    	}
    	//Установить настройки
		((EditBean)portletSession.getAttribute("editBean")).setPortletPreferences(renderRequest.getPreferences());
		
		renderRequest.setAttribute("editBean", (EditBean)portletSession.getAttribute("editBean"));
		
    	include(editJSP, renderRequest, renderResponse);
    	
    }
	
	@Override
	public void doView(
			RenderRequest renderRequest, RenderResponse renderResponse)
		throws IOException, PortletException {
		
		PortletSession portletSession = renderRequest.getPortletSession();
		
		//создать бины, если они не созданы
		if ((ViewBean)portletSession.getAttribute("viewBean") == null){
    		portletSession.setAttribute("viewBean", new ViewBean());
    	}
		if ((EditBean)portletSession.getAttribute("editBean") == null){
    		//editBean = new EditBean();
    		portletSession.setAttribute("editBean", new EditBean());
    	}
		
		//Проверка настроек портлета
    	((EditBean)portletSession.getAttribute("editBean")).setPortletPreferences(renderRequest.getPreferences());
    	((ViewBean)portletSession.getAttribute("viewBean")).setEditBean((EditBean)portletSession.getAttribute("editBean"));
    	
    	//Создать URL(для ссылки назад) для вида SENT_EXCEPT  
    	if (((ViewBean)portletSession.getAttribute("viewBean")).getViewJSPURL() == null){
    			((ViewBean)portletSession.getAttribute("viewBean")).setViewJSPURL(renderResponse.createRenderURL().toString());
    	} else {
    		if (((ViewBean)portletSession.getAttribute("viewBean")).getViewJSPURL().equals("")){
    			((ViewBean)portletSession.getAttribute("viewBean")).setViewJSPURL(renderResponse.createRenderURL().toString());
    			//viewURL.setPortletMode(PortletMode.VIEW);
    		}
    	}
    	
    	//если вид - форма с полями
    	if (((ViewBean)portletSession.getAttribute("viewBean")).getSent().equals(ViewBean.SENT_EXCEPT)) {
    		//если форма не была обработана(processAction не отработан), то сбросить валидацию
    		//если обработана(processAction отработан), то не сбрасывать валидацию, а то результат валидации не отобразится на форме 
			if (!((ViewBean)portletSession.getAttribute("viewBean")).isProcessing()) {
				((ViewBean)portletSession.getAttribute("viewBean")).resetValid();
			}
			//вернуть состояние формы - не обработана
			((ViewBean)portletSession.getAttribute("viewBean")).setProcessing(false);
		}
		//установить viewBean для view.jsp
    	renderRequest.setAttribute("viewBean", (ViewBean)portletSession.getAttribute("viewBean"));
    	
		include(viewJSP, renderRequest, renderResponse);
	}
	
	
	@Override
	public void processAction(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws IOException, PortletException {
		
		ViewBean vb = new ViewBean();
		EditBean eb = new EditBean();
		
		//получить сессию
		PortletSession portletSession = actionRequest.getPortletSession();
		
		//если бины не созданы то создать их
		if ((ViewBean)portletSession.getAttribute("viewBean") == null){
			portletSession.setAttribute("viewBean", vb);
			//portletSession.setAttribute("viewBean", new ViewBean());
		}
		//portletSession.setAttribute("viewBean", new ViewBean());
		if ((EditBean)portletSession.getAttribute("editBean") == null){
			portletSession.setAttribute("editBean", eb);
			//portletSession.setAttribute("editBean", new EditBean());
		}
		
		//получить свойства портлета
    	((EditBean)portletSession.getAttribute("editBean")).setPortletPreferences(actionRequest.getPreferences());
    	
    	//если обрабатывать вид EDIT
    	if (actionRequest.getPortletMode().toString().equalsIgnoreCase("edit")) {
    		
    		((EditBean)portletSession.getAttribute("editBean")).setHost(actionRequest.getParameter("email_smpt"));
    		((EditBean)portletSession.getAttribute("editBean")).validateHost();
    		
    		((EditBean)portletSession.getAttribute("editBean")).setPort(actionRequest.getParameter("email_port"));
    		((EditBean)portletSession.getAttribute("editBean")).validatePort();
    		
    		((EditBean)portletSession.getAttribute("editBean")).setTo(actionRequest.getParameter("email_sed"));
    		((EditBean)portletSession.getAttribute("editBean")).validateTo();
    		
    		((EditBean)portletSession.getAttribute("editBean")).setFrom(actionRequest.getParameter("email_from"));
    		((EditBean)portletSession.getAttribute("editBean")).validateFrom();
    		
    		((EditBean)portletSession.getAttribute("editBean")).setPassFrom(actionRequest.getParameter("pass_email_from"));
    		
    		((EditBean)portletSession.getAttribute("editBean")).setMaxSizeAttach(actionRequest.getParameter("max_size_attach"));
    		((EditBean)portletSession.getAttribute("editBean")).validateMaxSizeAttach();
    		
    		((EditBean)portletSession.getAttribute("editBean")).setFormatAttach(actionRequest.getParameter("format_attach"));
    		((EditBean)portletSession.getAttribute("editBean")).setRegexFormatAttach(", ");
    		
    	}
    	
    	//если обрабатывать вид VIEW
		if (actionRequest.getPortletMode().toString().equalsIgnoreCase("view")) {
			//сбросить значения
			((ViewBean)portletSession.getAttribute("viewBean")).resetValue();
			//установить EditBean, для получения значений настроек
    		((ViewBean)portletSession.getAttribute("viewBean")).setEditBean(((EditBean)portletSession.getAttribute("editBean")));
    		
    		//попробовать установить максимальные значения пропуска аплоада на портале, возможно это не нужно
    		PortalUtil.getPortalProperties().setProperty("com.liferay.portal.upload.UploadServletRequestImpl.max.size", String.valueOf(Integer.MAX_VALUE));
    		PortalUtil.getPortalProperties().setProperty("com.liferay.portal.upload.UploadServletRequestImpl.max.size", "104857600"); 
    		
			FileItemFactory factory = new DiskFileItemFactory();
			PortletFileUpload upload = new PortletFileUpload(factory);
			upload.setHeaderEncoding(actionRequest.getCharacterEncoding());
			
			//UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
			//UploadServletRequest usr = PortalUtil.getUploadServletRequest(PortalUtil.getHttpServletRequest(actionRequest));
			
			//System.out.println("ParamUtil.getString = " + ParamUtil.getString(usr, "reqDocGroupVal1"));
			//System.out.println("ParamUtil.getString = " + usr.getParameter("reqDocGroupVal1"));
			/*HttpServletRequest request = PortalUtil.getHttpServletRequest(actionRequest);
			String param = PortalUtil.getOriginalServletRequest(request).getParameter("param");*/ 
			
			//System.out.println("actionRequest.getParameter(reqDocGroupVal1) = " + actionRequest.getParameter("reqDocGroupVal1"));
			
			try {
				//upload.setHeaderEncoding("UTF-8");
				//спарсим в List запрос
				List lParseRequest = upload.parseRequest(actionRequest);
				
				/*сначала получим параметры значения от которых зависит валидация
				т.к. после удаления сессии мы их больше не сможем получить, они находятся во view.jsp*/
				for (Object dfi:lParseRequest) {
					
					if (((DiskFileItem)dfi).getFieldName().equals("req_doc_group_val1")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setReqDocGroupVal1(((DiskFileItem)dfi).getString("Utf-8"));
						((DiskFileItem)dfi).delete();
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("req_doc_group_val2")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setReqDocGroupVal2(((DiskFileItem)dfi).getString("Utf-8"));
						((DiskFileItem)dfi).delete();
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("year_housing_program_val1")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setYearHousingProgramVal1(((DiskFileItem)dfi).getString("Utf-8"));
						((DiskFileItem)dfi).delete();
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("year_housing_program_val2")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setYearHousingProgramVal2(((DiskFileItem)dfi).getString("Utf-8"));
						((DiskFileItem)dfi).delete();
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("year_housing_program_val3")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setYearHousingProgramVal3(((DiskFileItem)dfi).getString("Utf-8"));
						((DiskFileItem)dfi).delete();
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("req_doc_group_val3")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setReqDocGroupVal3(((DiskFileItem)dfi).getString("Utf-8"));
						((DiskFileItem)dfi).delete();
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("req_doc_group_val4")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setReqDocGroupVal4(((DiskFileItem)dfi).getString("Utf-8"));
						((DiskFileItem)dfi).delete();
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("req_doc_group_val5")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setReqDocGroupVal5(((DiskFileItem)dfi).getString("Utf-8"));
						((DiskFileItem)dfi).delete();
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("req_doc_group_val6")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setReqDocGroupVal6(((DiskFileItem)dfi).getString("Utf-8"));
						((DiskFileItem)dfi).delete();
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("req_doc_group_val7")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setReqDocGroupVal7(((DiskFileItem)dfi).getString("Utf-8"));
						((DiskFileItem)dfi).delete();
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("req_doc_group_val10")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setReqDocGroupVal10(((DiskFileItem)dfi).getString("Utf-8"));
						((DiskFileItem)dfi).delete();
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("req_doc_group_val11")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setReqDocGroupVal11(((DiskFileItem)dfi).getString("Utf-8"));
						((DiskFileItem)dfi).delete();
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("req_doc_group_val12")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setReqDocGroupVal12(((DiskFileItem)dfi).getString("Utf-8"));
						((DiskFileItem)dfi).delete();
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("type_applicant_val1")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setTypeApplicantVal1(((DiskFileItem)dfi).getString("Utf-8"));
						((DiskFileItem)dfi).delete();
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("type_applicant_val2")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setTypeApplicantVal2(((DiskFileItem)dfi).getString("Utf-8"));
						((DiskFileItem)dfi).delete();
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("date_contract_val1")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setDateContractVal1(((DiskFileItem)dfi).getString("Utf-8"));
						((DiskFileItem)dfi).delete();
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("date_contract_val2")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setDateContractVal2(((DiskFileItem)dfi).getString("Utf-8"));
						((DiskFileItem)dfi).delete();
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("object_more_info2_val1")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setObjectMoreInfo2Val1(((DiskFileItem)dfi).getString("Utf-8"));
						((DiskFileItem)dfi).delete();
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("object_more_info2_val2")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setObjectMoreInfo2Val2(((DiskFileItem)dfi).getString("Utf-8"));
						((DiskFileItem)dfi).delete();
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("public_date_doc_val1")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setPublicDateDocVal1(((DiskFileItem)dfi).getString("Utf-8"));
						((DiskFileItem)dfi).delete();
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("public_date_doc_val2")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setPublicDateDocVal2(((DiskFileItem)dfi).getString("Utf-8"));
						((DiskFileItem)dfi).delete();
						continue;
					}
				}
				
				//получаем остальные параметры
				for (Object dfi:lParseRequest) {
					//получаем файл
					if (!((DiskFileItem)dfi).isFormField()) {
						
						DiskFileItem dfiUploadFile = ((DiskFileItem)dfi);
						
						//Если пришел какой-то файл
	                	if (!dfiUploadFile.getFieldName().equals("")) {
	                		
	                		//если имя файла отсутсвует, то пропустить итерацию
	                		if (dfiUploadFile.getName() == null) continue;
	                		if (dfiUploadFile.getName().equals("")) continue;
	                		
	                		
	                		//File uploadFile = File.createTempFile("uploadFile", MyUtils.getExt(dfiUploadFile.getName()) );
	                		//попробуем создать темповые файлы через средства портала, чтобы исключить повторений
	                		//File uploadFile = FileUtil.createTempFile(MyUtils.getExtNotDot(dfiUploadFile.getName()));
	                		File uploadFile = File.createTempFile("uploadFile", MyUtils.getExt(dfiUploadFile.getName()), ((ViewBean)portletSession.getAttribute("viewBean")).getRequestDir());
	                		FileUtils.writeByteArrayToFile(uploadFile, FileUtil.getBytes(dfiUploadFile.getInputStream()));
	                		
	                		/*
	                		//размер части сколько читать в файл в килобайтах
	                		int partData = 32*1024;
	                		FileOutputStream fos = new FileOutputStream(uploadFile);
	                		BufferedOutputStream bos = new BufferedOutputStream(fos, partData);
	                		BufferedInputStream bis = new BufferedInputStream(dfiUploadFile.getInputStream(), partData);
	                		
	                		int c;
	                		byte data[] = new byte[partData];
	                		while (((c = bis.read(data)) != -1) && (Double.valueOf(String.valueOf(uploadFile.length())) <= ((EditBean)portletSession.getAttribute("editBean")).getMaxSizeAttachInDouble() + 1.0) ){
	                			bos.write(data, 0, c);
	                			bos.flush();
	                		}
	                		
	                		bos.close();
	                		bis.close();
	                		fos.close();
	                		*/
	                		
	                		//установить файл
	                		if (dfiUploadFile.getFieldName().equals("individual_attach_passport")) {
	                			((ViewBean)portletSession.getAttribute("viewBean")).setIndividualAttachPassport(uploadFile);
	                			dfiUploadFile.delete();
		                    	continue;
	                		}
	                		
	                		if (dfiUploadFile.getFieldName().equals("individual_procuratory_attach_passport")) {
	                			((ViewBean)portletSession.getAttribute("viewBean")).setIndividualProcuratoryAttachPassport(uploadFile);
	                			dfiUploadFile.delete();
		                    	continue;
	                		}
	                		
	                		if (dfiUploadFile.getFieldName().equals("legal_procuratory_attach")) {
	                			((ViewBean)portletSession.getAttribute("viewBean")).setLegalProcuratoryAttach(uploadFile);
	                			dfiUploadFile.delete();
		                    	continue;
	                		}
	                		
	                		if (dfiUploadFile.getFieldName().equals("object_attach")) {
	                			((ViewBean)portletSession.getAttribute("viewBean")).setObjectAttach(uploadFile);
	                			dfiUploadFile.delete();
		                    	continue;
	                		}
	                    	
	                    	
	                	}
						
					}
					
					////в соотвествии с названием поля
					if (((DiskFileItem)dfi).getFieldName().equals("req_doc_group")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setReqDocGroup(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					
					if (((DiskFileItem)dfi).getFieldName().equals("purpose_request")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setPurposeRequest(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("type_applicant")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setTypeApplicant(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("individual_lastname")) {
						ViewBean vb1 = ((ViewBean)portletSession.getAttribute("viewBean"));
						vb1.setIndividualLastname(((DiskFileItem)dfi).getString("Utf-8"));
						//((ViewBean)portletSession.getAttribute("viewBean")).setIndividualLastname(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("individual_firstname")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setIndividualFirstname(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("individual_middlename")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setIndividualMiddlename(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("individual_day_birthday")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setIndividualDayBirthday(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("individual_month_birthday")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setIndividualMonthBirthday(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("individual_year_birthday")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setIndividualYearBirthday(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("individual_change_last_first_middle_name")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setIndividualChangeLastFirstMiddleName(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("individual_series_passport")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setIndividualSeriesPassport(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("individual_number_passport")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setIndividualNumberPassport(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("individual_who_give_passport")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setIndividualWhoGivePassport(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("individual_day_issue_passport")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setIndividualDayIssuePassport(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("individual_month_issue_passport")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setIndividualMonthIssuePassport(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("individual_year_issue_passport")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setIndividualYearIssuePassport(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("individual_city")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setIndividualCity(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("individual_street")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setIndividualStreet(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("individual_home")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setIndividualHome(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("individual_housing")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setIndividualHousing(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("individual_apartment")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setIndividualApartment(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("individual_section")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setIndividualSection(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("individual_room")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setIndividualRoom(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("individual_number_telephone")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setIndividualNumberTelephone(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("individual_email")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setIndividualEmail(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("legal_name")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setLegalName(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("legal_ogrn")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setLegalOgrn(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("legal_inn")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setLegalInn(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("legal_head_lastname")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setLegalHeadLastname(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("legal_head_firstname")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setLegalHeadFirstname(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("legal_head_middlename")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setLegalHeadMiddlename(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("legal_as_head")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setLegalAsHead(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("legal_index")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setLegalIndex(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("legal_country")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setLegalCountry(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("legal_city")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setLegalCity(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("legal_street")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setLegalStreet(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("legal_home")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setLegalHome(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("legal_housing")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setLegalHousing(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("legal_apartment")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setLegalApartment(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("legal_section")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setLegalSection(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("legal_room")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setLegalRoom(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("legal_number_telephone")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setLegalNumberTelephone(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("legal_email")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setLegalEmail(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					
					if (((DiskFileItem)dfi).getFieldName().equals("contract_number")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setContractNumber(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("date_contract")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setDateContract(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("day_exact_date_contract")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setDayExactDateContract(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("month_exact_date_contract")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setMonthExactDateContract(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("year_exact_date_contract")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setYearExactDateContract(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("day_date_range_contract1")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setDayDateRangeContract1(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("month_date_range_contract1")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setMonthDateRangeContract1(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("year_date_range_contract1")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setYearDateRangeContract1(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("day_date_range_contract2")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setDayDateRangeContract2(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("month_date_range_contract2")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setMonthDateRangeContract2(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("year_date_range_contract2")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setYearDateRangeContract2(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("year_housing_program")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setYearHousingProgram(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("kind_object")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setKindObject(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("object_city")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setObjectCity(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("object_street")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setObjectStreet(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("object_home")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setObjectHome(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("object_housing")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setObjectHousing(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("object_apartment")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setObjectApartment(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("object_section")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setObjectSection(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("object_room")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setObjectRoom(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("object_more_info1")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setObjectMoreInfo1(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("object_more_info2")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setObjectMoreInfo2(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("object_url_geo")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setObjectUrlGeo(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("object_other_info")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setObjectOtherInfo(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("object_inventory_number_val")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setObjectInventoryNumberVal(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("object_doc")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setObjectDoc(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("number_doc")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setNumberDoc(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("public_date_doc")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setPublicDateDoc(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("public_day_exact_date_doc")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setPublicDayExactDateDoc(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("public_month_exact_date_doc")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setPublicMonthExactDateDoc(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("public_year_exact_date_doc")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setPublicYearExactDateDoc(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("day_public_date_range_doc1")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setDayPublicDateRangeDoc1(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("month_public_date_range_doc1")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setMonthPublicDateRangeDoc1(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("year_public_date_range_doc1")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setYearPublicDateRangeDoc1(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("day_public_date_range_doc2")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setDayPublicDateRangeDoc2(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("month_public_date_range_doc2")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setMonthPublicDateRangeDoc2(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("year_public_date_range_doc2")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setYearPublicDateRangeDoc2(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
					if (((DiskFileItem)dfi).getFieldName().equals("author_doc")) {
						((ViewBean)portletSession.getAttribute("viewBean")).setAuthorDoc(((DiskFileItem)dfi).getString("Utf-8"));
						continue;
					}
						
				}
				
				
			} catch (Exception e) {
				_log.info("Get StackTrace:");
				e.printStackTrace();
			}  finally{
				
				//Если провалидированно успешно
				if (((ViewBean)portletSession.getAttribute("viewBean")).isValidAll()) {
					
	    			//послать сообщение, если успешно, то поставить маркер SENT_SUCCESSFULLY
					//Иначе SENT_FAILS
					if (((ViewBean)portletSession.getAttribute("viewBean")).sendMessage()) {
						((ViewBean)portletSession.getAttribute("viewBean")).setSent(ViewBean.SENT_SUCCESSFULLY);
					} else {
						((ViewBean)portletSession.getAttribute("viewBean")).setSent(ViewBean.SENT_FAILS);
					}
	    		}
				
				//установить обработку формы
				((ViewBean)portletSession.getAttribute("viewBean")).setProcessing(true);
			}
		
    	}
		
	}
	
	
	protected void include(
			String path, RenderRequest renderRequest,
			RenderResponse renderResponse)
		throws IOException, PortletException {

		PortletRequestDispatcher portletRequestDispatcher =
			getPortletContext().getRequestDispatcher(path);

		if (portletRequestDispatcher == null) {
			_log.error(path + " is not a valid include");
		}
		else {
			portletRequestDispatcher.include(renderRequest, renderResponse);
		}
	}	

}