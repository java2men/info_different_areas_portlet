//разрешить конфликт версий
var $jq_ida = jQuery.noConflict(true);

function dinamicCountDay(el_day, el_month, el_year) {
	//месяц начинается с нуля!
	var dayCountInMonth = 32 - new Date(el_year.val(), el_month.val()-1, 32).getDate();
	var dayCountInSelectDay = el_day.find('option').length - 1;
	
	if (dayCountInMonth < dayCountInSelectDay) {
		var i = 0;
		$jq_ida(el_day.find('option')).each(function(){
			i++;
			if (i > (dayCountInMonth + 1)) $jq_ida(this).remove();
		});
	} 
	if (dayCountInMonth > dayCountInSelectDay) {
		for(var i = (dayCountInSelectDay + 1); i <= dayCountInMonth; i++){
			$jq_ida(el_day).append('<option value="'+ i +'">'+ i +'</option>');
		}
	}
	
}

function calendar(el_day, el_month, el_year) {
	
	if ($jq_ida(el_day).is('select') && $jq_ida(el_month).is('select') && $jq_ida(el_year).is('select')) {
		
		dinamicCountDay(el_day, el_month, el_year);
		
		$jq_ida(el_month).bind("change", function(event){
			dinamicCountDay(el_day, el_month, el_year);
		});
		
		$jq_ida(el_year).bind("change", function(event){
			dinamicCountDay(el_day, el_month, el_year);
		});
	}
}

//
function choice_req_doc_group() {
	//$jq("#radio_val_benefit").validateStop();
	//$jq("#copy_benefit").validateStop();
	
	
	//alert($jq_ida("#individual").prop("checked"));
	
	if ($jq_ida("#req_doc_group2").prop("checked")) {
		$jq_ida("#year_housing_program_div").attr("style", "");
		$jq_ida("#year_housing_program_div").validateStart();
	} else {
		$jq_ida("#year_housing_program_div").attr("style", "display: none;");
		$jq_ida("#year_housing_program_div").validateStop();
		$jq_ida("#year_housing_program_div").validateReset();
	}
	
	if ($jq_ida("#req_doc_group6").prop("checked") || $jq_ida("#req_doc_group7").prop("checked")) {
		$jq_ida("#details_contract").attr("style", "");
	} else {
		$jq_ida("#details_contract").attr("style", "display: none;");
	}
	
	if ($jq_ida("#req_doc_group10").prop("checked") || $jq_ida("#req_doc_group11").prop("checked") || $jq_ida("#req_doc_group12").prop("checked")) {
		$jq_ida("#kind_object_ul").attr("style", "");
		$jq_ida("#kind_object").validateStart();
	} else {
		$jq_ida("#kind_object_ul").attr("style", "display: none;");
		$jq_ida("#kind_object").validateStop();
	}
	
	if ($jq_ida("#req_doc_group4").prop("checked") || $jq_ida("#req_doc_group5").prop("checked") || $jq_ida("#req_doc_group6").prop("checked") || $jq_ida("#req_doc_group7").prop("checked") || $jq_ida("#req_doc_group10").prop("checked") || $jq_ida("#req_doc_group11").prop("checked") || $jq_ida("#req_doc_group12").prop("checked")) {
		$jq_ida("#address_object_ul").attr("style", "");
		$jq_ida("#object_city").validateStart();
		$jq_ida("#object_street").validateStart();
		$jq_ida("#object_home").validateStart();
	} else {
		$jq_ida("#address_object_ul").attr("style", "display: none;");
		$jq_ida("#object_city").validateStop();
		$jq_ida("#object_street").validateStop();
		$jq_ida("#object_home").validateStop();
	}
	
	if ($jq_ida("#req_doc_group10").prop("checked") || $jq_ida("#req_doc_group11").prop("checked")) {
		$jq_ida("#object_more_info1_ul").attr("style", "");
	} else {
		$jq_ida("#object_more_info1_ul").attr("style", "display: none;");
	}
	
	if ($jq_ida("#req_doc_group12").prop("checked")) {
		$jq_ida("#object_more_info2_ul").attr("style", "");
		$jq_ida("#object_url_geo").validateStart();
	} else {
		$jq_ida("#object_more_info2_ul").attr("style", "display: none;");
		$jq_ida("#object_url_geo").validateStop();
	}
	
	if ( $jq_ida("#req_doc_group1").prop("checked") 
			|| $jq_ida("#req_doc_group2").prop("checked") 
			|| $jq_ida("#req_doc_group3").prop("checked")
			|| $jq_ida("#req_doc_group4").prop("checked")
			|| $jq_ida("#req_doc_group5").prop("checked")
			|| $jq_ida("#req_doc_group6").prop("checked")
			|| $jq_ida("#req_doc_group7").prop("checked")
	) {
		$jq_ida("#individual_birthday_li").attr("style", "");
		$jq_ida("#individual_change_ul").attr("style", "");
		if ($jq_ida("#individual").prop("checked")) {
			$jq_ida("#individual_day_birthday").validateStart();
			$jq_ida("#individual_month_birthday").validateStart();
			$jq_ida("#individual_year_birthday").validateStart();
		}
	} else {
		$jq_ida("#individual_birthday_li").attr("style", "display: none;");
		$jq_ida("#individual_change_ul").attr("style", "display: none;");
		if ($jq_ida("#individual").prop("checked")) {
			$jq_ida("#individual_day_birthday").validateStop();
			$jq_ida("#individual_month_birthday").validateStop();
			$jq_ida("#individual_year_birthday").validateStop();
			$jq_ida("#individual_day_birthday").validateReset();
			$jq_ida("#individual_month_birthday").validateReset();
			$jq_ida("#individual_year_birthday").validateReset();
		}
	}
}

function choice_type_applicant() {
	
	//alert($jq_ida("#individual").prop("checked"));
	if ($jq_ida("#individual").prop("checked")) {
		$jq_ida("#individual_ul").attr("style", "");
		$jq_ida("#legal_ul").attr("style", "display: none;");
		
		$jq_ida("#individual_lastname").validateStart();
		$jq_ida("#individual_firstname").validateStart();
		
		$jq_ida("#individual_series_passport").validateStart();
		$jq_ida("#individual_number_passport").validateStart();
		$jq_ida("#individual_who_give_passport").validateStart();
		$jq_ida("#individual_day_issue_passport").validateStart();
		$jq_ida("#individual_month_issue_passport").validateStart();
		$jq_ida("#individual_year_issue_passport").validateStart();
		$jq_ida("#individual_attach_passport").validateStart();
		$jq_ida("#individual_city").validateStart();
		$jq_ida("#individual_street").validateStart();
		$jq_ida("#individual_home").validateStart();
		$jq_ida("#individual_number_telephone").validateStart();
		$jq_ida("#individual_email").validateStart();
		
		$jq_ida("#legal_name").validateStop();
		$jq_ida("#legal_ogrn").validateStop();
		$jq_ida("#legal_inn").validateStop();
		$jq_ida("#legal_head_lastname").validateStop();
		$jq_ida("#legal_head_firstname").validateStop();
		$jq_ida("#legal_procuratory_attach").validateStop();
		$jq_ida("#legal_as_head").validateStop();
		$jq_ida("#legal_index").validateStop();
		$jq_ida("#legal_country").validateStop();
		$jq_ida("#legal_city").validateStop();
		$jq_ida("#legal_street").validateStop();
		$jq_ida("#legal_home").validateStop();
		$jq_ida("#legal_number_telephone").validateStop();
		$jq_ida("#legal_email").validateStop();
		
		$jq_ida("#legal_name").validateReset();
		$jq_ida("#legal_ogrn").validateReset();
		$jq_ida("#legal_inn").validateReset();
		$jq_ida("#legal_head_lastname").validateReset();
		$jq_ida("#legal_head_firstname").validateReset();
		$jq_ida("#legal_procuratory_attach").validateReset();
		$jq_ida("#legal_as_head").validateReset();
		$jq_ida("#legal_index").validateReset();
		$jq_ida("#legal_country").validateReset();
		$jq_ida("#legal_city").validateReset();
		$jq_ida("#legal_street").validateReset();
		$jq_ida("#legal_home").validateReset();
		$jq_ida("#legal_number_telephone").validateReset();
		$jq_ida("#legal_email").validateReset();
	} 
	if ($jq_ida("#legal").prop("checked")) {
		$jq_ida("#individual_ul").attr("style", "display: none;");
		$jq_ida("#legal_ul").attr("style", "");
		
		$jq_ida("#individual_lastname").validateStop();
		$jq_ida("#individual_firstname").validateStop();
		
		$jq_ida("#individual_series_passport").validateStop();
		$jq_ida("#individual_number_passport").validateStop();
		$jq_ida("#individual_who_give_passport").validateStop();
		$jq_ida("#individual_day_issue_passport").validateStop();
		$jq_ida("#individual_month_issue_passport").validateStop();
		$jq_ida("#individual_year_issue_passport").validateStop();
		$jq_ida("#individual_attach_passport").validateStop();
		$jq_ida("#individual_city").validateStop();
		$jq_ida("#individual_street").validateStop();
		$jq_ida("#individual_home").validateStop();
		$jq_ida("#individual_number_telephone").validateStop();
		$jq_ida("#individual_email").validateStop();
		$jq_ida("#individual_lastname").validateReset();
		$jq_ida("#individual_firstname").validateReset();
		
		$jq_ida("#individual_series_passport").validateReset();
		$jq_ida("#individual_number_passport").validateReset();
		$jq_ida("#individual_who_give_passport").validateReset();
		$jq_ida("#individual_day_issue_passport").validateReset();
		$jq_ida("#individual_month_issue_passport").validateReset();
		$jq_ida("#individual_year_issue_passport").validateReset();
		$jq_ida("#individual_attach_passport").validateReset();
		$jq_ida("#individual_city").validateReset();
		$jq_ida("#individual_street").validateReset();
		$jq_ida("#individual_home").validateReset();
		$jq_ida("#individual_number_telephone").validateReset();
		$jq_ida("#individual_email").validateReset();
		
		$jq_ida("#legal_name").validateStart();
		$jq_ida("#legal_ogrn").validateStart();
		$jq_ida("#legal_inn").validateStart();
		$jq_ida("#legal_head_lastname").validateStart();
		$jq_ida("#legal_head_firstname").validateStart();
		$jq_ida("#legal_procuratory_attach").validateStart();
		$jq_ida("#legal_as_head").validateStart();
		$jq_ida("#legal_index").validateStart();
		$jq_ida("#legal_country").validateStart();
		$jq_ida("#legal_city").validateStart();
		$jq_ida("#legal_street").validateStart();
		$jq_ida("#legal_home").validateStart();
		$jq_ida("#legal_number_telephone").validateStart();
		$jq_ida("#legal_email").validateStart();
	}
}

//выбор дополнительных сведений об объекте
function choice_object_more_info2(){
	if ($jq_ida("#object_location").prop("checked")) {
		$jq_ida("#object_url_geo_span").attr("style", "");
		$jq_ida("#object_url_geo").validateStart();
		$jq_ida("#object_other_info_span").attr("style", "");
		$jq_ida("#object_inventory_number_val_span").attr("style", "display: none;");
	}
	if ($jq_ida("#object_inventory_number").prop("checked")) {
		$jq_ida("#object_url_geo_span").attr("style", "display: none;");
		$jq_ida("#object_url_geo").validateStop();
		$jq_ida("#object_url_geo").validateReset();
		$jq_ida("#object_other_info_span").attr("style", "display: none;");
		$jq_ida("#object_inventory_number_val_span").attr("style", "");
	}
}

//выбор даты даты договора
function choice_date_contract() {
	//если выбрана точная дата
	if ($jq_ida('#exact_date_contract').prop('checked')){
		
		$jq_ida("#exact_date_contract_val_div").attr("style", "");
		$jq_ida("#day_exact_date_contract").validateStart();
		$jq_ida("#month_exact_date_contract").validateStart();
		$jq_ida("#year_exact_date_contract").validateStart();
		
		$jq_ida("#date_range_contract_val_div").attr("style", "display: none;");
		
		$jq_ida("#day_date_range_contract1").validateStop();
		$jq_ida("#month_date_range_contract1").validateStop();
		$jq_ida("#year_date_range_contract1").validateStop();
		$jq_ida("#day_date_range_contract2").validateStop();
		$jq_ida("#month_date_range_contract2").validateStop();
		$jq_ida("#year_date_range_contract2").validateStop();
		$jq_ida("#day_date_range_contract1").validateReset();
		$jq_ida("#month_date_range_contract1").validateReset();
		$jq_ida("#year_date_range_contract1").validateReset();
		$jq_ida("#day_date_range_contract2").validateReset();
		$jq_ida("#month_date_range_contract2").validateReset();
		$jq_ida("#year_date_range_contract2").validateReset();
	}
	//если выбран диапозон дат
	if ($jq_ida('#date_range_contract').prop('checked')){
		
		$jq_ida("#exact_date_contract_val_div").attr("style", "display: none;");
		$jq_ida("#day_exact_date_contract").validateStop();
		$jq_ida("#month_exact_date_contract").validateStop();
		$jq_ida("#year_exact_date_contract").validateStop();
		$jq_ida("#day_exact_date_contract").validateReset();
		$jq_ida("#month_exact_date_contract").validateReset();
		$jq_ida("#year_exact_date_contract").validateReset();
		
		$jq_ida("#date_range_contract_val_div").attr("style", "");
		$jq_ida("#day_date_range_contract1").validateStart();
		$jq_ida("#month_date_range_contract1").validateStart();
		$jq_ida("#year_date_range_contract1").validateStart();
		$jq_ida("#day_date_range_contract2").validateStart();
		$jq_ida("#month_date_range_contract2").validateStart();
		$jq_ida("#year_date_range_contract2").validateStart();
	}
}

//выбор даты издания документов
function choice_public_date_doc() {
	if ($jq_ida("#public_exact_date_doc").prop("checked")) {
		
		$jq_ida("#public_exact_date_doc_val_div").attr("style", "");
		//$jq_ida("#public_exact_date_doc_val_div").show();
		
		$jq_ida("#public_day_exact_date_doc").validateStart();
		$jq_ida("#public_month_exact_date_doc").validateStart();
		$jq_ida("#public_year_exact_date_doc").validateStart();

		$jq_ida("#public_date_range_doc_val_div").attr("style", "display: none;");
		$jq_ida("#day_public_date_range_doc1").validateStop();
		$jq_ida("#month_public_date_range_doc1").validateStop();
		$jq_ida("#year_public_date_range_doc1").validateStop();
		$jq_ida("#day_public_date_range_doc2").validateStop();
		$jq_ida("#month_public_date_range_doc2").validateStop();
		$jq_ida("#year_public_date_range_doc2").validateStop();
		$jq_ida("#day_public_date_range_doc1").validateReset();
		$jq_ida("#month_public_date_range_doc1").validateReset();
		$jq_ida("#year_public_date_range_doc1").validateReset();
		$jq_ida("#day_public_date_range_doc2").validateReset();
		$jq_ida("#month_public_date_range_doc2").validateReset();
		$jq_ida("#year_public_date_range_doc2").validateReset();
	}
	if ($jq_ida("#public_date_range_doc").prop("checked")) {
		
		$jq_ida("#public_exact_date_doc_val_div").attr("style", "display: none;");
		//$jq_ida("#public_exact_date_doc_val_div").hide();
		
		$jq_ida("#public_day_exact_date_doc").validateStop();
		$jq_ida("#public_month_exact_date_doc").validateStop();
		$jq_ida("#public_year_exact_date_doc").validateStop();
		$jq_ida("#public_day_exact_date_doc").validateReset();
		$jq_ida("#public_month_exact_date_doc").validateReset();
		$jq_ida("#public_year_exact_date_doc").validateReset();
		
		$jq_ida("#public_date_range_doc_val_div").attr("style", "");
		$jq_ida("#day_public_date_range_doc1").validateStart();
		$jq_ida("#month_public_date_range_doc1").validateStart();
		$jq_ida("#year_public_date_range_doc1").validateStart();
		$jq_ida("#day_public_date_range_doc2").validateStart();
		$jq_ida("#month_public_date_range_doc2").validateStart();
		$jq_ida("#year_public_date_range_doc2").validateStart();
	}
}

//валидировать календарь
function validate_calendar(day, month, year) {
	//alert("момент истины настал!");
	$jq_ida(day).validate({
		//expression: "if ($jq_ida('#public_exact_date_doc').prop('checked')){" +
		expression:		"if ( ($jq_ida('" + day + "').val() != '') || ($jq_ida('" + month + "').val() != '') || ($jq_ida('" + year + "').val() != '') ) {" +
							"if (($jq_ida('" + day + "').val() == '')) return false; " +
							"else {" +
								"return true;" +
							"}" +
						"} else {" +
							"$jq_ida('" + month + "').validateReset();" +
							"$jq_ida('" + year + "').validateReset();" +
							"return true;" +
						"}",
		//			"}" ,
		message: "Необходимо выбрать день.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida(day).parent().parent()
	});
	//изначально остановить валидацию
	$jq_ida(day).validateStop();
	//$jq_ida(day).validateReset();
	
	$jq_ida(month).validate({
		expression:		"if ( ($jq_ida('" + day + "').val() != '') || ($jq_ida('" + month + "').val() != '') || ($jq_ida('" + year + "').val() != '') ) {" +
							"if (($jq_ida('" + month + "').val() == '')) return false; " +
							"else {" +
								"return true;" +
							"}" +
						"} else {" +
							"$jq_ida('" + day + "').validateReset();" +
							"$jq_ida('" + year + "').validateReset();" +
							"return true;" +
						"}",
		message: "Необходимо выбрать месяц.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida(month).parent().parent()
	});
	//изначально остановить валидацию
	$jq_ida(month).validateStop();
	//$jq_ida(month).validateReset();
	
	$jq_ida(year).validate({
		expression:		"if ( ($jq_ida('" + day + "').val() != '') || ($jq_ida('" + month + "').val() != '') || ($jq_ida('" + year + "').val() != '') ) {" +
							"if (($jq_ida('" + year + "').val() == '')) return false; " +
							"else {" +
								"return true;" +
							"}" +
						"} else {" +
							"$jq_ida('" + day + "').validateReset();" +
							"$jq_ida('" + month + "').validateReset();" +
							"return true;" +
						"}",
		message: "Необходимо выбрать год.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida(year).parent().parent()
	});
	//изначально остановить валидацию
	$jq_ida(year).validateStop();
	//$jq_ida(year).validateReset();
	/*
	if ( ($jq_ida(day).val() != '') && ($jq_ida(month).val() != '') && ($jq_ida(year).val() != '') ) {
		return 'full';
	} else {
		if ( ($jq_ida(day).val() != '')){
			if ( ($jq_ida(month).val() != '')){
				if ( ($jq_ida(year).val() != '')){
					return 'no';
				} else {
					return 'incomplete';
				}
			} else {
				return 'incomplete';
			}
		} else{
			return 'incomplete';
		}
		
	}
	*/
	
}
//объеденить валидацию 2 датапикеров, для диапозона,domoy - день или месяц или год 
/*function merge_validate_calendar1(domoy1, domoy2) {
	$jq_ida(domoy1).validate({
		expression:	"if ( $jq_ida('" + domoy1 + "').val() == ''){" +
						"return false;" +
					"} else {" +
						"return true;" +
					"}",
		message: "Необходимо выбрать начальную дату.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida(domoy1).parent().parent()
	});
	
	$jq_ida(domoy2).validate({
		expression:	"if ( $jq_ida('" + domoy2 + "').val() == ''){" +
						"return false;" +
					"} else {" +
						"return true;" +
					"}",
		message: "Необходимо выбрать конечную дату.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida(domoy2).parent().parent()
	});
}*/
//объеденить валидацию 2 датапикеров, для диапозона
function merge_validate_calendar(day1, month1, year1, day2, month2, year2) {
	$jq_ida(day1).validate({
		expression:	"if ($jq_ida('" + month1 + "').val() == '' && $jq_ida('" + year1 + "').val() == '' && $jq_ida('" + day2 + "').val() == '' && $jq_ida('" + month2 + "').val() == '' && $jq_ida('" + year2 + "').val() == '') return true;" +
					"if ( $jq_ida('" + day1 + "').val() == ''){" +
						"return false;" +
					"} else {" +
						"return true;" +
					"}",
		message: "Необходимо выбрать начальную дату.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida(day1).parent().parent()
	});
	
	$jq_ida(day2).validate({
		expression:	"if ($jq_ida('" + month2 + "').val() == '' && $jq_ida('" + year2 + "').val() == '' && $jq_ida('" + day1 + "').val() == '' && $jq_ida('" + month1 + "').val() == '' && $jq_ida('" + year1 + "').val() == '') return true;" +
					"if ( $jq_ida('" + day2 + "').val() == ''){" +
						"return false;" +
					"} else {" +
						"return true;" +
					"}",
		message: "Необходимо выбрать конечную дату.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida(day2).parent().parent()
	});
}

function validate_kindergarten_form() {
	$jq_ida("#radio_val_info").validate({
		//expression: "if (VAL){ alert('первый'); alert(VAL); return true;} else {alert('второй'); alert(VAL); return false;}",
		expression: "if (isCheckedRadio(SelfID)) return true; else return false;",
		message: "Необходимо выбрать наименование запрашиваемого документа.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#radio_val_info")
	});
	
	$jq_ida("#type_applicant_div").validate({
		//expression: "if (VAL){ alert('первый'); alert(VAL); return true;} else {alert('второй'); alert(VAL); return false;}",
		expression: "if (isCheckedRadio(SelfID)) return true; else return false;",
		message: "Необходимо выбрать заявителя.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#type_applicant_div")
	});
	
	$jq_ida("#individual_lastname").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести фамилию.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#individual_lastname").parent().parent()
	});
	
	$jq_ida("#individual_firstname").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести имя.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#individual_firstname").parent().parent()
	});
	
	$jq_ida("#individual_day_birthday").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо выбрать день рождения.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#individual_day_birthday").parent().parent()
	});
	//чтобы submit работал с первого раза
	$jq_ida("#individual_day_birthday").validateStop();
	
	$jq_ida("#individual_month_birthday").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо выбрать месяц рождения.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#individual_month_birthday").parent().parent()
	});
	//чтобы submit работал с первого раза
	$jq_ida("#individual_month_birthday").validateStop();
	
	$jq_ida("#individual_year_birthday").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо выбрать год рождения.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#individual_year_birthday").parent().parent()
	});
	//чтобы submit работал с первого раза
	$jq_ida("#individual_year_birthday").validateStop();
	
	$jq_ida("#individual_series_passport").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести серию паспорта.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#individual_series_passport").parent().parent()
	});
	
	$jq_ida("#individual_number_passport").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести номер паспорта.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#individual_number_passport").parent().parent()
	});
	
	$jq_ida("#individual_who_give_passport").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести кем выдан паспорт.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#individual_who_give_passport").parent()
	});
	
	$jq_ida("#individual_day_issue_passport").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо выбрать день выдачи паспорта.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#individual_day_issue_passport").parent().parent()
	});
	
	$jq_ida("#individual_month_issue_passport").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо выбрать месяц выдачи паспорта.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#individual_month_issue_passport").parent().parent()
	});
	
	$jq_ida("#individual_year_issue_passport").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо выбрать год выдачи паспорта.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#individual_year_issue_passport").parent().parent()
	});
	
	$jq_ida("#individual_attach_passport").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо загрузить копию паспорта.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#individual_attach_passport").parent().parent()
	});
	
	$jq_ida("#individual_city").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести город.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#individual_city").parent().parent()
	});
	
	$jq_ida("#individual_street").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести улицу.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#individual_street").parent().parent()
	});
	
	$jq_ida("#individual_home").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести дом.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#individual_home").parent().parent()
	});
	
	$jq_ida("#individual_number_telephone").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести номер телефона.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#individual_number_telephone").parent()
	});
	
	$jq_ida("#individual_email").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести адрес электронной почты.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#individual_email").parent().parent()
	});
	
	$jq_ida("#individual_email").validate({
		expression: "if (VAL.match(/^[^\\W][a-zA-Z0-9\\_\\-\\.]+([a-zA-Z0-9\\_\\-\\.]+)*\\@[a-zA-Z0-9_]+(\\.[a-zA-Z0-9_]+)*\\.[a-zA-Z]{2,4}$/)) return true; else return false;",
		message: "Адрес электронной почты введен некорректно.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#individual_email").parent().parent()
	});
	
	
	$jq_ida("#legal_name").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести полное наименование.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#legal_name").parent().parent()
	});
	
	$jq_ida("#legal_ogrn").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести ОГРН.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#legal_ogrn").parent().parent()
	});
	
	$jq_ida("#legal_inn").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести ИНН.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#legal_inn").parent().parent()
	});
	
	$jq_ida("#legal_head_lastname").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести фамилию.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#legal_head_lastname").parent().parent()
	});
	
	$jq_ida("#legal_head_firstname").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести имя.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#legal_head_firstname").parent().parent()
	});
	
	$jq_ida("#legal_procuratory_attach").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо загрузить документ, подтверждающий полномочия юридического лица.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#legal_procuratory_attach").parent().parent()
	});
	
	$jq_ida("#legal_as_head").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести должность.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#legal_as_head").parent()
	});
	
	$jq_ida("#legal_index").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести индекс.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#legal_index").parent().parent()
	});
	
	$jq_ida("#legal_country").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести страну.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#legal_country").parent().parent()
	});
	
	$jq_ida("#legal_city").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести город.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#legal_city").parent().parent()
	});
	
	$jq_ida("#legal_street").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести улицу.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#legal_street").parent().parent()
	});
	
	$jq_ida("#legal_home").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести дом.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#legal_home").parent().parent()
	});
	
	$jq_ida("#legal_number_telephone").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести номер телефона.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#legal_number_telephone").parent()
	});
	
	$jq_ida("#legal_email").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести адрес электронной почты.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#legal_email").parent().parent()
	});
	
	$jq_ida("#legal_email").validate({
		expression: "if (VAL.match(/^[^\\W][a-zA-Z0-9\\_\\-\\.]+([a-zA-Z0-9\\_\\-\\.]+)*\\@[a-zA-Z0-9_]+(\\.[a-zA-Z0-9_]+)*\\.[a-zA-Z]{2,4}$/)) return true; else return false;",
		message: "Адрес электронной почты введен некорректно.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#legal_email").parent().parent()
	});
	
	$jq_ida("#year_housing_program_div").validate({
		expression: "if ($jq_ida(\"[name='req_doc_group']:checked\").val() == null) return true;" +
					"if ($jq_ida(\"[name='req_doc_group']:checked\").val() == '') return true;" +
					"if ($jq_ida(\"[name='req_doc_group']:checked\").val() == $jq_ida('#req_doc_group2').val() ) {" +
						"if (isCheckedRadio(SelfID)) return true; else return false;" +
					"} else return true;",
		message: "Необходимо выбрать годы участия граждан в подпрограммах, предусмотренных федеральной целевой программой «Жилище».",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#radio_val_info")
	});
	
	$jq_ida("#kind_object").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести вид объекта.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#kind_object").parent().parent()
	});
	
	$jq_ida("#object_city").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести город.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#object_city").parent().parent()
	});
	
	$jq_ida("#object_street").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести улицу.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#object_street").parent().parent()
	});
	
	$jq_ida("#object_home").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести дом.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#object_home").parent().parent()
	});
	
	$jq_ida("#purpose_request").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести цель запроса.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#purpose_request").parent()
	});
	
	/*
	$jq_ida("#object_url_geo").validate({
		expression: "if (VAL) return true; else return false;",
		message: "Необходимо ввести гиперссылку.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#object_url_geo").parent()
	});
	*/
	
	$jq_ida("#object_url_geo").validate({
		//expression: "if (VAL.match('(https?:\/\/)?(www.)?(^.*)(?=(?:maps.yandex.ru)|(?:maps.2gis.ru)|(?:maps.google.com)|(?:openstreetmap.ru))([\/\w\.-=?]*)*\/?/')) return true; else return false;",
		//expression: "if (VAL.match('/(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/')) return true; else return false;",
		//"^([a-zA-Z0-9_\\.\\-+])+@(([a-zA-Z0-9-])+\\.)+([a-zA-Z0-9]{2,4})+$"
		//предусматривает так же, если ничего невведено(последний вопросительный знак)
		expression: "if (VAL.match('^(((?:https?:\/\/)?(?:www.)?((?:maps.yandex.ru)|(?:maps.2gis.ru)|(?:maps.google.com)|(?:openstreetmap.ru))\/(.*))?)+$')) return true; else return false;",
		message: "Возможно гиперссылка на фрагмент карты введена некорректно.",
		error_msg_class: "form-error-message",
        error_elm_class: "form-validation-error",
		container: "div",
        afterElement: $jq_ida("#object_url_geo").parent()
	});
	/****валидация для Дата договора, чтоб не забывали выбрать все поля(лучше б я сделал одно поле с датой....)****/
	//Точная дата
	validate_calendar("#day_exact_date_contract", "#month_exact_date_contract", "#year_exact_date_contract");
	//Диапозон дат
	validate_calendar("#day_date_range_contract1", "#month_date_range_contract1", "#year_date_range_contract1");
	validate_calendar("#day_date_range_contract2", "#month_date_range_contract2", "#year_date_range_contract2");
	merge_validate_calendar("#day_date_range_contract1", "#month_date_range_contract1", "#year_date_range_contract1", "#day_date_range_contract2", "#month_date_range_contract2", "#year_date_range_contract2");
	
	/****валидация для Дата издания документа, чтоб не забывали выбрать все поля(лучше б я сделал одно поле с датой....)****/
	//Точная дата		
	validate_calendar("#public_day_exact_date_doc", "#public_month_exact_date_doc", "#public_year_exact_date_doc");
	//Диапозон дат
	//блядь
	validate_calendar("#day_public_date_range_doc1", "#month_public_date_range_doc1", "#year_public_date_range_doc1");
	validate_calendar("#day_public_date_range_doc2", "#month_public_date_range_doc2", "#year_public_date_range_doc2");
	merge_validate_calendar("#day_public_date_range_doc1", "#month_public_date_range_doc1", "#year_public_date_range_doc1", "#day_public_date_range_doc2", "#month_public_date_range_doc2", "#year_public_date_range_doc2");
}



$jq_ida(document).ready(function(){
	
	//Замутить календари
	calendar($jq_ida('#individual_day_birthday'), $jq_ida('#individual_month_birthday'), $jq_ida('#individual_year_birthday'));
	calendar($jq_ida('#individual_day_issue_passport'), $jq_ida('#individual_month_issue_passport'), $jq_ida('#individual_year_issue_passport'));
	calendar($jq_ida('#day_exact_date_contract'), $jq_ida('#month_exact_date_contract'), $jq_ida('#year_exact_date_contract'));
	calendar($jq_ida('#day_date_range_contract1'), $jq_ida('#month_date_range_contract1'), $jq_ida('#year_date_range_contract1'));
	calendar($jq_ida('#day_date_range_contract2'), $jq_ida('#month_date_range_contract2'), $jq_ida('#year_date_range_contract2'));	
	calendar($jq_ida('#public_day_exact_date_doc'), $jq_ida('#public_month_exact_date_doc'), $jq_ida('#public_year_exact_date_doc'));
	calendar($jq_ida('#day_public_date_range_doc1'), $jq_ida('#month_public_date_range_doc1'), $jq_ida('#year_public_date_range_doc1'));
	calendar($jq_ida('#day_public_date_range_doc2'), $jq_ida('#month_public_date_range_doc2'), $jq_ida('#year_public_date_range_doc2'));
	
	//валидация формы
	validate_kindergarten_form();
	
	//Скрыть или отобразить зараннее элементы до загрузки страницы
	choice_req_doc_group();
	choice_type_applicant();
	choice_object_more_info2();
	choice_date_contract();
	choice_public_date_doc();
	
	//навешать обработчики событий
	//наименование запрашиваемого документа
	$jq_ida("[name='req_doc_group']").bind("change", function(event){
		choice_req_doc_group();
	});
	
	//выбор заявителя
	$jq_ida("[name='type_applicant']").bind("change", function(event){
		choice_type_applicant();
	});
	
	//выбор дополнительных сведений об объекте
	$jq_ida("[name='object_more_info2']").bind("change", function(event){
		choice_object_more_info2();
	});
	
	//выбор даты даты договора
	$jq_ida("[name='date_contract']").bind("change", function(event){
		choice_date_contract();
	});
	
	//выбор даты издания документов
	$jq_ida("[name='public_date_doc']").bind("change", function(event){
		choice_public_date_doc();
	});
	
	//submit происходит за счет plugin jquery.livevalidate.js
	$jq_ida("#infodiffrentareas_send").click(function(){
		
	});
});