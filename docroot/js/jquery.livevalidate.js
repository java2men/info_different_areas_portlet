/**
 * @author GeekTantra - fork f13 20 february 2013
 * @date 20 September 2009 
 */
(function(jQuery){
    var ValidationErrors = new Array();
    jQuery.fn.validate = function(options){
    	//alert("validate for " + jQuery(this).attr("id"));
        options = jQuery.extend({
            expression: "return true;",
            message: "",
            error_msg_class: "ValidationErrors",
            error_elm_class: "ErrorField",
            live: true,
            container: "span",
            afterElement: jQuery(this)
        }, options);
        jQuery(this).data("options", options);
        //jQuery.data(this, "options", options);
        
        /*if (jQuery(this).attr("id") == "public_day_exact_date_doc") {
        	alert("yes it's work!");
        	alert(jQuery(this).data("options").message);
        }*/
        //alert(jQuery.data(this, "options"));
        var SelfID = jQuery(this).attr("id");
        var unix_time = new Date();
        unix_time = parseInt(unix_time.getTime() / 1000);
        if (!jQuery(this).parents('form:first').attr("id")) {
            jQuery(this).parents('form:first').attr("id", "Form_" + unix_time);
        }
        var FormID = jQuery(this).parents('form:first').attr("id");
        if (!((typeof(ValidationErrors[FormID]) == 'object') && (ValidationErrors[FormID] instanceof Array))) {
            ValidationErrors[FormID] = new Array();
        }
        if (options['live']) {
        	//alert(jQuery(this).find('input').length);
        	//alert(jQuery(this).attr('id'));
            if (jQuery(this).find('input').length > 0) {
                jQuery(this).find('input').bind('blur', function(){
                	if (jQuery(this).hasClass('validate_stop')) return;
                    if (validate_field("#" + SelfID, options)) {
                        if (options.callback_success) 
                            options.callback_success(this);
                    }
                    else {
                        if (options.callback_failure) 
                            options.callback_failure(this);
                    }
                });
                jQuery(this).find('input').bind('focus keypress click', function(){
                	if (jQuery(this).hasClass('validate_stop')) return;
                    //jQuery("#" + SelfID).next('.' + options['error_msg_class']).remove();
                	//console.log(options['afterElement'].attr('id'));
                	options['afterElement'].nextAll('.' + 'for_id_' + SelfID).remove();
                    jQuery("#" + SelfID).removeClass(options['error_elm_class']);
                });
            }
            else {
                jQuery(this).bind('blur', function(){
                	if (jQuery(this).hasClass('validate_stop')) return;
                    validate_field(this);
                });
                jQuery(this).bind('focus keypress', function(){
                	if (jQuery(this).hasClass('validate_stop')) return;
                    //jQuery(this).next('.' + options['error_msg_class']).fadeOut("fast", function(){
                	/*options['afterElement'].next('.' + options['error_msg_class']).fadeOut("fast", function(){
                		jQuery(this).remove();
                    });*/
                	//console.log(options['afterElement'].next('.' + 'for_id_' + SelfID).attr('class'));
                	options['afterElement'].nextAll('.' + 'for_id_' + SelfID).remove();
                    jQuery(this).removeClass(options['error_elm_class']);
                });
            }
        }
        
        /*jQuery(this).parents("form").submit(function(){
        	//alert('mother fucker');
            if (validate_field('#' + SelfID)) 
                return true;
            else 
                return false;
        });*/
        
        //var e = jQuery.Event("submit");
        //event.
        
        jQuery(this).parents("form").bind("submit", function(){
        	//alert(jQuery('#' + SelfID).hasClass('validate_stop'));
        	if (jQuery('#' + SelfID).hasClass('validate_stop')) return true;
        	if (validate_field('#' + SelfID)) 
                return true;
            else 
                return false;
        });
        
        function validate_field(id){
            var self = jQuery(id).attr("id");
            var expression = 'function Validate(){' + options['expression'].replace(/VAL/g, 'jQuery(\'#' + self + '\').val()') + '} Validate()';
            var validation_state = eval(expression);
            if (!validation_state) {
            	if (!jQuery(id).hasClass(options['error_elm_class'])) {
                	options['afterElement'].after('<' + options['container'] + ' class="' + options['error_msg_class'] + ' for_id_' + self + '">' + options['message'] + '</' + options['container'] + '>');
                    jQuery(id).addClass(options['error_elm_class']);
                }
                if (ValidationErrors[FormID].join("|").search(id) == -1) 
                    ValidationErrors[FormID].push(id);
                return false;
            }
            else {
                for (var i = 0; i < ValidationErrors[FormID].length; i++) {
                    if (ValidationErrors[FormID][i] == id) 
                        ValidationErrors[FormID].splice(i, 1);
                }
                return true;
            }
        }
    };
    jQuery.fn.validateStop = function(){
    	//alert("validateStop for " + jQuery(this).attr("id"));
    	jQuery(this).addClass('validate_stop');
    };
    jQuery.fn.validateStart = function(){
    	//alert("validateStart for " + jQuery(this).attr("id"));
    	if (jQuery(this).hasClass('validate_stop'))	jQuery(this).removeClass('validate_stop');
    };
    jQuery.fn.validateReset = function(){
    	//alert("validateReset for " + jQuery(this).attr("id"));
    	//options['afterElement'].nextAll('.' + 'for_id_' + SelfID).remove();
    	//var opt = jQuery(this).data("options");
    	//console.log(jQuery.data(this, "options"));
    	//alert(opt["error_elm_class"]);
    	
    	//jQuery(this).data("options").afterElement.nextAll('.' + 'for_id_' + jQuery(this).attr("id")).remove();
    	//jQuery(this).removeClass(jQuery(this).data("options").error_elm_class);
    	
    	//console.log(jQuery.data(this, "options"));
    	//alert(jQuery.data(this, "options"));
    	//alert("validateReset");
    	
    	if (jQuery(this).data("options") != undefined) {
    	//if (jQuery(this).hasData()) {
    		//alert(jQuery.data(this, "options").message);
	    	//jQuery.data(this, "options").afterElement.nextAll('.' + 'for_id_' + jQuery(this).attr("id")).remove();
	    	//jQuery(this).removeClass(jQuery.data(this, "options").error_elm_class);
	    	jQuery(this).data("options").afterElement.nextAll('.' + 'for_id_' + jQuery(this).attr("id")).remove();
	    	jQuery(this).removeClass(jQuery(this).data("options").error_elm_class);
    	} 
    	/*else {
    		alert("validateReset not work with " + jQuery(this).attr('id'));
    		if (jQuery(this).attr("id") == "public_day_exact_date_doc") {
            	alert("yes it's public_day_exact_date_doc");
            	jQuery(this).data("options", {message: "хуй там вам"});
            	alert(jQuery(this).data("options").message);
            }
    	}*/
    };
    jQuery.fn.validated = function(callback){
        jQuery(this).each(function(){
            if (this.tagName == "FORM") {
                jQuery(this).submit(function(){
                    if (ValidationErrors[jQuery(this).attr("id")].length == 0)
                        callback();
					return false;
                });
            }
        });
    };
    
    function isValidDate(year, month, day){
        var date = new Date(year, (month - 1), day);
        var DateYear = date.getFullYear();
        var DateMonth = date.getMonth();
        var DateDay = date.getDate();
        if (DateYear == year && DateMonth == (month - 1) && DateDay == day) 
            return true;
        else 
            return false;
    }
    /*
     * This function checks if there is at-least one element checked in a group of check-boxes or radio buttons.
     * @id: The ID of the check-box or radio-button group
     */
    function isCheckedRadio(id){
        var ReturnVal = false;
        jQuery("#" + id).find('input[type="radio"]').each(function(){
            //if (jQuery(this).is(":checked")) {
        	if (jQuery(this).prop("checked")){
                ReturnVal = true;
            }
        });
        return ReturnVal;
    }
    
    function isCheckedCheckbox(id){
        var ReturnVal = false;
        jQuery("#" + id).find('input[type="checkbox"]').each(function(){
            //if (jQuery(this).is(":checked")) {
            if (jQuery(this).prop("checked")){
                ReturnVal = true;
            }
        });
        return ReturnVal;
    }
    
})(jQuery);
